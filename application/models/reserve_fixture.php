<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Reserve Fixture model
 */
class Reserve_Fixture_Model extends ORM {
	
	protected $table_name = 'reserve_fixtures';
	
	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = FALSE)
	{
		// Initialise the validation library and setup so me rules
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('match_date', 'valid::date') // Not required, as fixtures may be TBA
				->add_rules('competition', 'required', 'length[1,64]')
				->add_rules('opposition', 'required', 'length[1,64]') // Not required, as fixtures may be TBA
				->add_rules('is_home', 'required', 'valid::digit')
				->add_rules('kickoff', 'valid::date') // Not required, as fixtures may be TBA
				->post_filter('type_valid::to_date', 'match_date') // Convert to valid, formatted date
				->post_filter('type_valid::to_time', 'kickoff') // Convert to valid, formatted time
				->post_filter('type_valid::empty_or_false_to_null'); // Convert all empty or false fields to null

		return parent::validate($array, $save);
	}

} // End Reserve Fixture Model
