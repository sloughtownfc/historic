<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * News model
 */
class News_Model extends ORM {

	protected $belongs_to = array('season');
	protected $table_name = 'news';

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = FALSE)
	{
		// Initialise the validation library and setup some rules
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('headline', 'required', 'length[1,64]')
				->add_rules('author', 'required', 'length[1,64]')
				->add_rules('publish_date', 'required', 'valid::date')
				->add_rules('excerpt', 'required')
				->add_rules('body', 'required')
				->add_rules('video_url', 'length[1,128]')
				->add_rules('audio_url', 'length[1,150]')
				->add_rules('picture_url', 'required', 'length[1,128]')
				->add_rules('small_picture_url', 'required', 'length[1,128]')
				->add_rules('caption', 'required', 'length[1,128]')
				->add_rules('quote', 'length[1,500]')
				->add_rules('quote_by', 'length[1,64]')
				->post_filter('type_valid::to_date', 'publish_date') // Convert to valid, formatted date
				->post_filter('type_valid::to_time', 'publish_time') // Convert to valid, formatted time
				->post_filter('type_valid::empty_or_false_to_null'); // Convert all empty or false fields to null

		return parent::validate($array, $save);
	}

} // End News Model