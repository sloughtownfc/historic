<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Coach model
 */
class Coach_Model extends ORM {

	protected $table_name = 'fixtures';

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = FALSE)
	{
		// Initialise the validation library and setup some rules.
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('coach_departs', 'required', 'valid::date')
				->add_rules('coach_adult_price', 'required', 'length[1,16]')
				->add_rules('coach_reduced_price', 'required', 'length[1,16]')
				->post_filter('type_valid::to_time', 'coach_departs') // Convert to valid, formatted time
				->post_filter('type_valid::empty_or_false_to_null'); // Convert all empty or false fields to null
		return parent::validate($array, $save);
	}

} // End Coach Model
