<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Video model
 */
class Video_Model extends ORM {

	protected $table_name = 'fixtures';

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = false)
	{
		// Initialise the validation library and setup some rules.
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('video_author', 'required', 'length[1,64]')
				->add_rules('video_picture_url', 'required', 'length[1,128]')
				->add_rules('video_picture_thumbnail_url', 'required', 'length[1,128]')
				->add_rules('video_url', 'length[1,128]')
				->add_rules('video_body', 'required')
				->post_filter('type_valid::empty_or_false_to_null'); // Convert all empty or false fields to null

		return parent::validate($array, $save);
	}

} // End Video Model
