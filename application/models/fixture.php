<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Fixture model
 */
class Fixture_Model extends ORM {

	protected $belongs_to = array('competition', 'opposition');
	protected $has_many = array('stats');

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = FALSE)
	{
		// Initialise the validation library and setup some rules
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('match_date', 'valid::date') // Not required, as fixtures may be TBA
				->add_rules('competition_id', 'required', 'valid::digit', 'type_valid::exists[id, competitions, 0]')
				->add_rules('opposition_id', 'valid::digit', 'type_valid::exists[id, oppositions, 0]') // Not required, as fixtures may be TBA
				->add_rules('is_home', 'required', 'valid::digit')
				->add_rules('away_alternate_kit' , 'required', 'valid::digit')
				->add_rules('kickoff', 'valid::date') // Not required, as fixtures may be TBA
				->add_rules('match_sponsor', 'length[1,128]') // Not required, may be blank
				->add_rules('ball_sponsor', 'length[1,128]') // Not required, may be blank
				->post_filter('type_valid::to_date', 'match_date') // Convert to valid, formatted date
				->post_filter('type_valid::to_time', 'kickoff') // Convert to valid, formatted time
				->post_filter('type_valid::empty_or_false_to_null'); // Convert all empty or false fields to null

		return parent::validate($array, $save);
	}

} // End Fixture Model
