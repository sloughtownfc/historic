<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Academy model
 */
class Academy_Model extends ORM {
	
	protected $table_name = 'academy';

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	 
	public function validate(array &$array, $save = FALSE)
	{		
		// Initialise the validation library and setup some rules
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('name', 'required', 'length[1,64]')
				->add_rules('dob', 'required', 'valid::date')
				->add_rules('return_email', 'required', 'length[1,128]', 'valid::email')
				->add_rules('phone', 'required', 'length[1,12]')
				->add_rules('trial_date', 'required')
				->add_rules('school', 'required')
				->add_rules('position', 'required');

		return parent::validate($array, $save);
	}

} // End Academy Model
