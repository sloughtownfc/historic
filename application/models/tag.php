<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Tag model
 */
class Tag_Model extends ORM {

	protected $has_and_belongs_to_many = array('gallery_images');

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = FALSE)
	{
		// Initialise the validation library and setup some rules
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('name', 'required', 'length[1,32]');

		return parent::validate($array, $save);
	}

} // End Tag model
