<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Academy Report model
 */
class Academy_Report_Model extends ORM {

	protected $table_name = 'academy_fixtures';

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = false)
	{
		// Initialise the validation library and setup some rules.
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('report_home_score', 'required', 'valid::digit')
				->add_rules('report_away_score', 'required', 'valid::digit')
				->add_rules('report_headline', 'required', 'length[1,64]')
				->add_rules('report_home_scorers', 'length[0,1000]')
				->add_rules('report_excerpt', 'required')
				->add_rules('picture_url', 'required', 'length[1,128]')
				->add_rules('report_team', 'length[1,1000]')
				->add_rules('report_motm', 'length[0,64]')
				->add_rules('report_published', 'required', 'valid::digit')
				->post_filter('type_valid::empty_or_false_to_null', 'report_body', 'report_home_scorers', 'report_team', 'report_motm', 'report_published'); // Convert all empty or false fields to null

		return parent::validate($array, $save);
	}

} // End Academy Report Model
