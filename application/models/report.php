<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Report model
 */
class Report_Model extends ORM {

	protected $table_name = 'fixtures';

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = false)
	{
		// Initialise the validation library and setup some rules.
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('report_picture_url', 'required', 'length[1,128]')
				->add_rules('report_home_score', 'required', 'valid::digit')
				->add_rules('report_away_score', 'required', 'valid::digit')
				//->add_rules('club_scorers') // Not required, there may be no scorers.
				//->add_rules('opposition_scorers') // Not required, there may be no scorers.
				->add_rules('report_extra_time', 'valid::digit') // Not required, may not run into extra time.
				->add_rules('report_attendance', 'required', 'valid::digit')
				->add_rules('report_headline', 'required', 'length[1,64]')
				->add_rules('report_excerpt', 'required')
				->add_rules('report_body', 'required')
				->add_rules('report_friendly_team', 'length[1,1000]')
				->add_rules('report_away_team', 'length[1,1000]')
				->add_rules('report_mom', 'required', 'type_valid::exists[id, players, 0]')
				->add_rules('report_flickr_url', 'length[1,128]', 'valid::url')
				->add_rules('report_published', 'required', 'valid::digit')
				->post_filter('type_valid::empty_or_false_to_null', 'report_picture_url', 'report_extra_time', 'report_attendance', 'report_home_scorers', 'report_away_scorers', 'report_headline', 'report_excerpt', 'report_body', 'report_friendly_team', 'report_away_team', 'report_mom', 'report_flickr_url', 'report_published'); // Convert all empty or false fields to null

		return parent::validate($array, $save);
	}

} // End Report Model
