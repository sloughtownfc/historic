<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Paper model
 */
class Paper_Model extends ORM {

	protected $table_name = 'papers';

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = FALSE)
	{
		// Initialise the validation library and setup some rules
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('publish_date', 'required', 'valid::date')
				->add_rules('headline', 'required', 'length[1,64]')
				->add_rules('url', 'length[1,255]', 'valid::url')
				->post_filter('type_valid::to_date', 'publish_date'); // Convert to valid, formatted date

		return parent::validate($array, $save);
	}

} // End Paper Model
