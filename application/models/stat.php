<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Stat model
 */
class Stat_Model extends ORM {

	protected $belongs_to = array('fixture', 'player');

	public static function unique_stat(Validation $array, $field)
	{
		// Fetch fields from validation object
		$fixture_id = $array->fixture_id;
		$player_id = $array->player_id;
		if (isset($array->id))
		{
			$id = $array->id;
		}
		else
		{
			// No id exists, this is a new object
			$id = 0;
		}

		$unique = !(bool)
			Database::instance()
			->where(array('fixture_id' => $fixture_id))
			->where(array('player_id' => $player_id))
			->where(array('id !=' => $id))
			->count_records('stats');

		if (!$unique)
		{
			// Add error because this fixture already has a stat for this player
			$array->add_error($field, 'unique_stat');
		}

		return $unique;
	}

	/**
	 * Tests whether a given booking code is valid.
	 *
	 * @param  integer  booking code to check
	 * @return boolean
	 */
	public static function valid_booking($booking_code)
	{
		return ($booking_code == 0 || $booking_code == 1 || $booking_code == 2) ?
			TRUE :
			FALSE;
	}

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = false)
	{
		// Initialise the validation library and setup some rules.
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('player_id', 'required', 'valid::digit', 'type_valid::exists[id, players, 0]')
				->add_rules('fixture_id', 'required', 'valid::digit', 'type_valid::exists[id, fixtures, 0]')
				->add_callbacks('player_id', 'Stat_Model::unique_stat')
				->add_rules('shirt', 'valid::digit')
				->add_rules('sub_shirt', 'valid::digit')
				->add_rules('appearance', 'valid::digit')
				->add_rules('goals', 'valid::digit')
				->add_rules('bookings', 'valid::digit', 'Stat_Model::valid_booking')
				->add_rules('injured', 'valid::digit')
				->post_filter('type_valid::empty_or_false_to_null'); // Convert all empty or false fields to null

		return parent::validate($array, $save);
	}

} // End Stat Model
