<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Contact model
 */
class Contact_Model extends ORM {

	protected $has_many = array('messages');

} // End Contact Model
