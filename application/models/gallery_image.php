<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Gallery Image model
 */
class Gallery_Image_Model extends ORM {

	protected $has_and_belongs_to_many = array('tags');

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = FALSE)
	{
		// Initialise the validation library and setup some rules
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('filename', 'required', 'length[1,64]')
				->add_rules('preview', 'required', 'length[1,64]')
				->add_rules('thumbnail', 'required', 'length[1,64]')
				->add_rules('type', 'required', 'length[1,16]')
				->add_rules('original_md5', 'required', 'length[1,32]');

		return parent::validate($array, $save);
	}

} // End Gallery Image model
