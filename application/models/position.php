<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Position model
 */
class Position_Model extends ORM {

	protected $has_many = array('players');

} // End Position Model
