<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Manager_Note model
 */
class Manager_Note_Model extends ORM {

	protected $table_name = 'fixtures';

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array	values to check
	 * @param  boolean	save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = false)
	{
		// Initialise the validation library and setup some rules
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('manager_note_author', 'required', 'length[1,64]')
				->add_rules('manager_note_headline', 'required', 'length[1,64]')
				->add_rules('manager_note_excerpt', 'required')
				->add_rules('manager_note_body', 'required')
				->add_rules('manager_note_picture_url', 'required', 'length[1,128]')
				->add_rules('manager_note_picture_thumbnail_url', 'required', 'length[1,128]')
				->add_rules('manager_note_caption', 'required', 'length[1,128]')
				->add_rules('manager_note_quote', 'required')
				->add_rules('manager_note_quote_credit', 'required', 'length[1,64]')
				->add_rules('manager_note_published', 'required', 'valid::digit')
				->post_filter('type_valid::empty_or_false_to_null', 'report_published'); // Convert all empty or false fields to null

		return parent::validate($array, $save);
	}

} // End Manager_Note Model
