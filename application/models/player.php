<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Player model
 */
class Player_Model extends ORM {

	protected $belongs_to = array('position');
	protected $has_many = array('stats');

	/**
	 * Search for player by either first name and last name,
	 * or use name to search for either first or last name.
	 *
	 * @param string $name
	 */
	public function search_fullname($name)
	{
		$name = explode(' ', $name);
		if (count($name) > 1)
		{
			// First and last name passed, search each column respectively
			return $this->like('first_name', $name[0])
				->like('last_name', $name[1]);
		}
		else
		{
			// Search first and last name using the only string passed
			return $this->orlike(array('first_name' => $name[0], 'last_name' => $name[0]));
		}
	}

	/**
	 * Returns a list of players with their full name.
	 *
	 * @param string $key
	 * @return array
	 */
	public function select_list_fullname($key = NULL)
	{
		if ($key === NULL)
		{
			$key = $this->primary_key;
		}

		// Return a full name select list from the results
		return $this->select($key, 'CONCAT(last_name, \', \' ,first_name) full_name')->find_all()->select_list($key, 'full_name');
	}

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = false)
	{
		// Initialise the validation library and setup some rules.
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('position_id', 'required', 'valid::digit', 'type_valid::exists[id, positions, 0]')
				->add_rules('first_name', 'required', 'valid::alpha', 'length[1,32]')
				->add_rules('last_name', 'required', 'valid::alpha', 'length[1,32]')
				->add_rules('dob', 'valid::date') // Not required, as player dob may be unknown.
				->add_rules('description', 'required', 'length[1,2500]') // Capped artificially to limit abuse.
				->add_rules('achievements', 'length[1,800]')
				->add_rules('picture_url', 'required', 'length[1,128]')
				->add_rules('active' , 'required', 'valid::digit')
				->post_filter('type_valid::to_date', 'dob') // Convert to valid, formatted date.
				->post_filter('type_valid::empty_or_false_to_null'); // Convert all empty or false fields to null.

		return parent::validate($array, $save);
	}

} // End Player Model
