<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Message model
 */
class Message_Model extends ORM {

	protected $belongs_to = array('contact');

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = FALSE)
	{
		// Initialise the validation library and setup some rules
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('name', 'required', 'length[1,128]')
				->add_rules('return_email', 'required', 'length[1,128]', 'valid::email')
				->add_rules('contact_id', 'required', 'valid::digit', 'type_valid::exists[id, contacts, 0]')
				->add_rules('subject', 'required', 'length[1,255]')
				->add_rules('body', 'required', 'length[1,1000]');

		return parent::validate($array, $save);
	}

} // End Message Model
