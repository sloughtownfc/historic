<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Opposition model
 */
class Opposition_Model extends ORM {

	protected $belongs_to = array('fixture');

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = false)
	{
		// Initialise the validation library and setup some rules.
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('name', 'required', 'length[1,64]', 'type_valid::not_exists[name, oppositions, ' . (isset($this->id) ? $this->id : 0) . ']')
				->add_rules('colour_away_background', 'required', 'length[7]')
				->add_rules('colour_away_foreground', 'required', 'length[7]')
				->add_rules('colour_home_background', 'required', 'length[7]')
				->add_rules('colour_home_foreground', 'required', 'length[7]')
				->add_rules('logo', 'required', 'length[1,128]')
				->add_rules('website', 'valid::url', 'length[1,128]') // Not required, may not have a website.
				->add_rules('grounds_venue', 'required', 'length[1,128]')
				->add_rules('grounds_address', 'required', 'length[1,128]')
				->add_rules('grounds_telephone', 'valid::phone')
				->add_rules('grounds_directions', 'required')
				->add_rules('grounds_google', 'required', 'length[1,255]')
				->add_rules('notes', 'length[1,1000]')
				->post_filter('type_valid::empty_or_false_to_null'); // Convert all empty or false fields to null

		return parent::validate($array, $save);
	}

} // End Opposition Model
