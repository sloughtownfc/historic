<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Season model
 */
class Season_Model extends ORM {

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = FALSE)
	{
		// Initialise the validation library and setup some rules
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('name', 'required', 'type_valid::not_exists[name, seasons, ' . (isset($this->id) ? $this->id : 0) . ']')
				->add_rules('start_date', 'required', 'valid::date', 'type_valid::valid_date_range[' . (!empty($array['end_date']) ? $array['end_date'] : 0) . ']')
				->add_rules('end_date', 'valid::date')
				->post_filter('type_valid::to_date', 'start_date') // Convert to valid, formatted date
				->post_filter('type_valid::to_date', 'end_date') // Convert to valid, formatted date
				->post_filter('type_valid::empty_or_false_to_null'); // Convert all empty or false fields to null

		return parent::validate($array, $save);
	}

} // End Season Model
