<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Competition model
 */
class Competition_Model extends ORM {

	protected $has_many = array('fixtures');

	/**
	 * Tests whether a given bench count is valid. This can be used
	 * as a Validation rule.
	 *
	 * @param   integer    bench count to check
	 * @return  boolean
	 */
	public static function valid_bench_count($count)
	{
		return ($count == 5 || $count == 7 ) ?
			TRUE :
			FALSE;
	}

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = FALSE)
	{
		// Initialise the validation library and setup some rules.
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('name', 'required', 'length[1,64]', 'type_valid::not_exists[name, competitions, ' . (isset($this->id) ? $this->id : 0) . ']')
				->add_rules('description', 'required', 'length[1,128]')
				->add_rules('bench_count', 'required', 'valid::digit', array($this, 'valid_bench_count'))
				->add_rules('stats_recorded', 'required', 'valid::digit')
				->post_filter('type_valid::empty_or_false_to_null'); // Convert all empty or false fields to null

		return parent::validate($array, $save);
	}

} // End Competition Model
