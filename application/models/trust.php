<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Trust model
 */
class Trust_Model extends ORM {

	protected $belongs_to = array('season');
	protected $table_name = 'trust';

	/**
	 * Validates and optionally saves a new record from an array.
	 *
	 * @param  array    values to check
	 * @param  boolean  save[Optional] the record when validation succeeds
	 * @return boolean
	 */
	public function validate(array &$array, $save = FALSE)
	{
		// Initialise the validation library and setup some rules
		$array = Validation::factory($array)
				->pre_filter('trim')
				->add_rules('publish_date', 'required', 'valid::date')
				->add_rules('author', 'required', 'length[1,64]')
				->add_rules('headline', 'required', 'length[1,64]')
				->add_rules('body', 'required')
				->post_filter('type_valid::to_date', 'publish_date') // Convert to valid, formatted date
				->post_filter('type_valid::empty_or_false_to_null'); // Convert all empty or false fields to null

		return parent::validate($array, $save);
	}

} // End Trust Model
