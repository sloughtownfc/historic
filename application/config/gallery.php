<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Local gallery directory.
 */
$config['root_dir'] = DOCROOT.'gallery';

/**
 * Public gallery URL.
 */
$config['root_url'] = 'gallery';

/**
 * Gallery subdirectories.
 */
$config['badge_dir'] = 'badge';
$config['front_dir'] = 'front';
$config['player_dir'] = 'player';

/**
 * Gallery dimensions.
 */
$config['size_dimensions'] = array
(
	'badge' => array
	(
		'height' 	=> 100,
		'width' 	=> 100
	),
	'front' => array
	(
		'height' 	=> 360,
		'width' 	=> 960
	),
	'player' => array
	(
		'height' 	=> 200,
		'width' 	=> 200
	),
	'preview' => array
	(
		'height' 	=> 300,
		'width' 	=> 629
	),
	'thumbnail' => array
	(
		'height' 	=> 75,
		'width' 	=> 75
	)
);
