<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * IMPORTANT NOTICE
 * ----------------
 * When specifying routes, pointing a URI (e.g., "admin") to a
 * controller will NOT forward methods.
 *
 * In order to forward methods, a rule to account for subsequent
 * URI segments must be configured.
 * e.g., $config['admin(.*)'] = 'controller$1';
 */
/**
 * Sets the default route to "default"
 */
$config['_default'] 					= 'default';
/**
 * Login / Logout
 */
$config['login'] 						= 'auth/login';
$config['logout'] 						= 'auth/logout';
/**
 * Admin
 */
$config['admin'] 						= 'admin/control_panel';
/**
 * Pages
 */
$config['/admin/gallery(.*)'] 			= 'default/page_gallery$1';
$config['500_club'] 					= 'default/page_500_club';
$config['academy'] 						= 'default/page_academy';
$config['academy-fixtures'] 			= 'default/page_academy_fixtures';
$config['academy-report(.*)']			= 'default/page_academy_report$1';
$config['all-time'] 					= 'default/page_all_time';
$config['article(.*)'] 					= 'default/page_article$1';
$config['away-directions'] 				= 'default/page_away_directions';
$config['attendance'] 					= 'default/page_attendance_table';
$config['away-league'] 					= 'default/page_away_league_table';
$config['contacts'] 					= 'default/page_contacts';
$config['contact'] 						= 'default/page_getintouch'; // Duplicate of "getintouch"
$config['current-players'] 				= 'default/page_current_players';
$config['directions(.*)'] 				= 'default/page_directions$1';
$config['fixtures'] 					= 'default/page_fixtures';
$config['galleriffic(.*)'] 				= 'default/page_gallery$1';
$config['getintouch'] 					= 'default/page_getintouch';
$config['groundregs'] 					= 'default/page_groundregs';
$config['goals-scored'] 				= 'default/page_goals_scored_table';
$config['home-league'] 					= 'default/page_home_league_table';
$config['honours'] 						= 'default/page_honours';
$config['how-to-find-us'] 				= 'default/page_how_to_find_us';
$config['league-table'] 				= 'default/page_league_table';
$config['links'] 						= 'default/page_links';
$config['manager-notes'] 				= 'default/page_manager_notes';
//$config['messageboards'] 				= 'default/page_messageboards';
$config['news'] 						= 'default/page_news';
$config['note(.*)'] 					= 'default/page_note$1';
$config['opposition-database(.*)'] 		= 'default/page_opposition_database$1';
$config['opposition(.*)'] 				= 'default/page_opposition$1';
$config['on-this-day'] 					= 'default/page_on_this_day';
$config['partners'] 					= 'default/page_sponsorship_opportunities';
$config['player-statistics']			= 'default/page_player_statistics';
$config['player-database(.*)'] 			= 'default/page_player_database$1';
$config['privacy-policy'] 				= 'default/page_privacy_policy';
$config['profile(.*)'] 					= 'default/page_profile$1';
$config['reports(.*)'] 					= 'default/page_report$1';
$config['reserve-fixtures'] 			= 'default/page_reserve_fixtures';
$config['reserve-report(.*)'] 			= 'default/page_reserve_report$1';
$config['social_guidelines']			= 'default/page_social_guidelines';
$config['sponsorship-opportunities'] 	= 'default/page_sponsorship_opportunities';
$config['stfc-youth'] 					= 'default/page_youth';
$config['supporters-coach'] 			= 'default/page_supporters_coach';
$config['tickets'] 						= 'default/page_tickets';
$config['u8-fixtures'] 					= 'default/page_u8_fixtures';
$config['u9-fixtures'] 					= 'default/page_u9_fixtures';
$config['u10-fixtures'] 				= 'default/page_u10_fixtures';
$config['u11-fixtures'] 				= 'default/page_u11_fixtures';
$config['u12-fixtures'] 				= 'default/page_u12_fixtures';
$config['u13-fixtures'] 				= 'default/page_u13_fixtures';
$config['u14-fixtures'] 				= 'default/page_u14_fixtures';
$config['u15-fixtures'] 				= 'default/page_u15_fixtures';
$config['u16-fixtures'] 				= 'default/page_u16_fixtures';
$config['u18-fixtures'] 				= 'default/page_u18_fixtures';
$config['u18-report(.*)'] 				= 'default/page_u18_report$1';
$config['videos'] 						= 'default/page_videos';
$config['video(.*)'] 					= 'default/page_video$1';