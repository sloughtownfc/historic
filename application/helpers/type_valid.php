<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Type Validation helper
 */
class type_valid {

	/**
	 * Convert all empty or false values to null. This allows fields
	 * that are set to allow null to be set as null instead of the
	 * input (e.g., empty string field, or the 0000-00-00 date).
	 *
	 * @param mixed $value value to check
	 * @return mixed
	 */
	public static function empty_or_false_to_null($value)
	{
		if (empty($value) || $value === FALSE)
		{
			return NULL;
		}
		else
		{
			return $value;
		}
	}

	/**
	 * Decides whether a given column value does exist within a table, and is
	 * not the current object (as determined by id).
	 *
	 * @param 	integer 		$value
	 * @param 	array 			$table_data
	 * @return 	boolean
	 */
	public static function exists($value, array $table_data)
	{
		// Handle incoming data from validation object string callback
		list ($column, $table, $id) = $table_data;
		return (bool)
			Database::instance()
			->where(array($column => $value))
			->where(array('id !=' => $id))
			->count_records($table);
	}

	/**
	 * Decides whether a given column value does not exist within a table, and is
	 * not the current object (as determined by id).
	 *
	 * @param 	integer 		$value
	 * @param 	array 			$table_data
	 * @return 	boolean
	 */
	public static function not_exists($value, array $table_data)
	{
		// Handle incoming data from validation object string callback
		return ! self::exists($value, $table_data);
	}

	/**
	 * Returns a formatted date string from any valid PHP timestamp.
	 * The timestamp can be numeric OR a formatted string OR a PHP
	 * datetime keyword (e.g., "today").
	 *
	 * @param   mixed   timestamp
	 * @param   string  format[Optional] of the returned date string
	 * @return 	mixed   false on empty or date string
	 */
	public static function to_date($time, $format = 'Y-m-d')
	{
		// Only pass a valid timestamp or PHP issues a warning
		// (bool)false is the return of a failed date()
		return (valid::date($time)) ?
			date($format, strtotime($time)) :
			FALSE;
	}

	/**
	 * Returns a formatted time string from any valid PHP timestamp.
	 * The timestamp can be numeric OR a formatted string OR a PHP
	 * datetime keyword (e.g., "now").
	 *
	 * @param   mixed   $time
	 * @param   string  $format[optional] of the returned time string
	 * @return 	mixed   FALSE on empty or time string
	 */
	public static function to_time($time, $format = 'H:i:s')
	{
		// Only pass a valid timestamp or PHP issues a warning
		// (bool)false is the return of a failed date()
		return (valid::date($time)) ?
			date($format, strtotime($time)) :
			FALSE;
	}

	/**
	 * Calculate whether the current date range is valid.
	 * A valid range is only where the start date is before
	 * the end date.
	 *
	 * @param 	string 		$start_date
	 * @param 	mixed 		$end_date - accepts string or array from validation callback
	 * @return 	boolean
	 */
	public static function valid_date_range($start_date = NULL, $end_date = NULL)
	{
		// Handle incoming data from validation object string callback
		if (is_array($end_date))
		{
			$end_date = array_shift($end_date);
		}

		if ($start_date !== NULL && $end_date !== NULL)
		{
			// Check the start date falls before the end date, only when the end date
			// is not false
			if
			(
				(self::to_date($start_date) < self::to_date($end_date)) ||
				self::to_date($end_date) === FALSE
			)
			{
				// All is well, date range is valid
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return TRUE;
		}
	}

} // End Type Validation helper
