<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Sidebar helper.
 */
class sidebar {

	public function squad_form()
	{
		$positions = ORM::factory('position')->select_list('id', 'name');
		$squad = array();
		foreach ($positions as $id => $name)
		{
			$squad[$name] = ORM::factory('player')->where('active', 1)->where('position_id', $id)->orderby('last_name', 'ASC')->find_all();
			if (count($squad[$name]) == 0)
			{
				// Remove squad position where there are no players
				unset($squad[$name]);
			}
		}
		return $squad;
	}

	/**
	 * Used to sort top goal scorers (ugly ugly UGLY!)
	 */
	public function top_goals_cmp($a, $b)
	{
		if ($a['goals'] == $b['goals'])
		{
			return 0;
		}
		return ($a['goals'] < $b['goals']) ? 1 : -1;
	}

	public function tab_goals()
	{
		// Limit number of players to return
		$top_active_goalscorers = ORM::factory('player')
									->where('active', 1)
									->find_all();
		// Fetch the most recent (current) season
		$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();
		$db = new Database;
		$ret = array();
		foreach ($top_active_goalscorers as $key => $goalscorer)
		{
			$ret[$key] = array();
			$ret[$key]['id'] = $goalscorer->id;
			$ret[$key]['name'] = $goalscorer->first_name . ' ' . $goalscorer->last_name;
			$goals = $db->from('fixtures, stats')
						->where('stats.fixture_id = fixtures.id AND stats.player_id = ' . $goalscorer->id)
						->where('fixtures.match_date >=', $season->start_date)
						->where('fixtures.match_date <', $season->end_date)
						->select('SUM(stats.goals) as goals')->get()->result_array(FALSE);
			$ret[$key]['goals'] = $goals[0]['goals'];
		}
		usort($ret, array('sidebar', 'top_goals_cmp'));
		return $ret;
	}

	public function tab_last_fixture()
	{
		// Fetch last report
		$last_report = ORM::factory('report')
						->where('report_published', 1)
						->orderby('match_date', 'DESC')
						->find();
		// Check report loaded
		if ($last_report->loaded)
		{
			$last_fixture = ORM::factory('fixture', $last_report->id);
			// Build limited array
			$last_fixture = array
			(
				'match_date' => date('l, jS F', strtotime($last_fixture->match_date)),
				'home_away' => $last_fixture->is_home ? 'Home' : 'Away',
				'is_home' => $last_fixture->is_home,
				'opposition_badge' => $last_fixture->opposition->logo,
				'opposition_name' => $last_fixture->opposition->name,
				'slough_score' => $last_fixture->is_home ? $last_report->report_home_score : $last_report->report_away_score,
				'opposition_score' => $last_fixture->is_home ? $last_report->report_away_score : $last_report->report_home_score,
				'report_id' => $last_report->id,
				'headline' => $last_report->report_headline
			);
			if ($last_fixture['slough_score'] > $last_fixture['opposition_score'])
			{
				$last_fixture['result'] = 'Won';
			}
			elseif($last_fixture['slough_score'] == $last_fixture['opposition_score'])
			{
				$last_fixture['result'] = 'Draw';
			}
			else
			{
				$last_fixture['result'] = 'Lost';
			}
		}
		else
		{
			// No next fixture
			return FALSE;
		}
		return $last_fixture;
	}

	public function tab_next_fixture()
	{
		// Fetch next fixture
		$next_fixture = ORM::factory('fixture')
		                ->where('match_date >=', date('Y-m-d', time()))
						->where('report_published', NULL)
		                ->orderby('match_date', 'ASC')
		                ->find();
		// Check fixture loaded
		if ($next_fixture->loaded)
		{
			// Build limited array
			$next_fixture = array
			(
				'match_date' => date('l, jS F', strtotime($next_fixture->match_date)),
				'home_away' => $next_fixture->is_home ? 'Home' : 'Away',
				'is_home' => $next_fixture->is_home,
				'away_kit' => $next_fixture->away_alternate_kit,
				'kickoff' => $next_fixture->kickoff ? $next_fixture->kickoff : 'TBA',
				'opposition_badge' => $next_fixture->opposition->logo,
				'opposition_id' => $next_fixture->opposition->id,
				'opposition_name' => $next_fixture->opposition->name,
				'ball_sponsor' => $next_fixture->ball_sponsor ? $next_fixture->ball_sponsor : '<a href="/getintouch">Available</a>',
				'match_sponsor' => $next_fixture->match_sponsor ? $next_fixture->match_sponsor : '<a href="/getintouch">Available</a>'
			);
		}
		else
		{
			// No next fixture
			return FALSE;
		}
		return $next_fixture;
	}

	public function tab_table()
	{

	}

	public function tab_reserves()
	{
		// Fetch next fixture
		$reserve_next_fixture = ORM::factory('reserve_fixture')
		                ->where('match_date >=', date('Y-m-d', time()))
		                ->orderby('match_date', 'ASC')
		                ->find();
		// Check fixture loaded
		if ($reserve_next_fixture->loaded)
		{
			// Build limited array
			$reserve_next_fixture = array
			(
				'match_date' => date('l, jS F', strtotime($reserve_next_fixture->match_date)),
				'home_away' => $reserve_next_fixture->is_home ? 'Home' : 'Away',
				'is_home' => $reserve_next_fixture->is_home,
				'kickoff' => $reserve_next_fixture->kickoff ? $reserve_next_fixture->kickoff : 'TBA',
				'opposition' => $reserve_next_fixture->opposition
			);
		}
		else
		{
			// No next fixture
			return FALSE;
		}
		return $reserve_next_fixture;
	}


	public function tab_u18s()
	{
		// Fetch next fixture
		$u18_next_fixture = ORM::factory('u18s_fixture')
		                ->where('match_date >=', date('Y-m-d', time()))
		                ->orderby('match_date', 'ASC')
		                ->find();
		// Check fixture loaded
		if ($u18_next_fixture->loaded)
		{
			// Build limited array
			$u18_next_fixture = array
			(
				'match_date' => date('l, jS F', strtotime($u18_next_fixture->match_date)),
				'home_away' => $u18_next_fixture->is_home ? 'Home' : 'Away',
				'is_home' => $u18_next_fixture->is_home,
				'kickoff' => $u18_next_fixture->kickoff ? $u18_next_fixture->kickoff : 'TBA',
				'opposition' => $u18_next_fixture->opposition
			);
		}
		else
		{
			// No next fixture
			return FALSE;
		}
		return $u18_next_fixture;
	}

} // End Sidebar helper.