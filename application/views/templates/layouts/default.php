<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://opengraphprotocol.org/schema/" xml:lang="en" lang="en">
<head>
<?php echo $blocks['head']; ?>

</head>
<body onload="initialize()">
	<div id="container">
		<div id="header_container">
	    	<div id="header">
			<div id="menu_container">
		    	<div class="menu">
                    <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/news">News</a>
                        <ul>
                            <li><a href="http://www.sloughtownsupporterstrust.co.uk" target="_blank">Trust News</a></li>
                        </ul>
                    </li>
                    <li><a href="/">First Team</a>
                        <ul>
                            <li><a href="/fixtures">Fixtures &amp; Reports</a></li>
                            <li><a href="/league-table">League Table</a></li>
                            <li><a href="/manager-notes">Manager Notes</a></li>
                            <li><a href="https://www.flickr.com/photos/horshamrebel/collections/72157682912196254/" target="_blank">Match Photos</a></li>
                            <li><a href="/videos">Match Videos</a></li>
                            <li><a href="/current-players">Squad Profiles</a></li>
                            <li><a href="/player-statistics">Player Statistics</a></li>
                            <li><a href="/player-database">Player Database</a></li>
                            <li><a href="/opposition-database">Opposition Database</a></li>
                        </ul>
                    </li>
                    <li><a href="/contacts">The Club</a>
                        <ul>
                            <li><a href="/contacts">Contacts</a></li>
                            <li><a href="/tickets">Tickets &amp; Admission</a></li>
                            <li><a href="/how-to-find-us">How To Find Us?</a></li>
                            <li><a href="/honours">Club Honours</a></li>
                            <li><a href="/all-time">All-time Records</a></li>
                            <li><a href="/on-this-day">On This Day</a></li>
                            <li><a href="/links">External Links</a></li>
                            <li><a href="/away-directions">Away Directions</a></li>
                            <li><a href="/supporters-coach">Supporters Coach</a></li>
                            <li><a href="/social_guidelines">Social Guidelines</a></li>
                        </ul>
                    </li>
                    <li><a href="/shop" target="_blank">Clubshop</a></li>
                    <li><a href="/sponsorship-opportunities">Sponsorship</a>
                        <ul>
                            <li><a href="/500_club">500 Club</a></li>
                        </ul>
                    </li>
                    <!--li><a href="/">Reserves</a>
                        <ul>
                            <li><a href="/reserve-fixtures">Fixtures &amp; Reports</a></li>
                            <li><a href="http://72.3.224.252/LeagueTab.cfm?TblName=Matches&DivisionID=5611&LeagueCode=HLNC2012" onclick="window.open(this.href); return false;">League Table</a></li>
                        </ul>
                    </li-->
                    <!--li><a href="/u18-fixtures">Under 18s</a>
                        <ul>
                            <li><a href="/u18-fixtures">Fixtures &amp; Reports</a></li>
                            <li><a href="http://full-time.thefa.com/ProcessPublicSelect.do;jsessionid=87CCC139720A13EB087E733689FCEBF9?psSelectedSeason=60593651&psSelectedDivision=8208937&psSelectedCompetition=0&psSelectedLeague=3625959" onclick="window.open(this.href); return false;">League Table</a></li>
                        </ul>
                    </li-->
                    <li><a href="/stfc-youth">STFC Juniors</a></li>
                    <!--li><a href="/academy">STFC Academy</a>
                    	<ul>
                        	<li><a href="/academy-fixtures">Fixtures &amp; Reports</a></li>
                            <li><a href="hhttp://full-time.thefa.com/ProcessPublicSelect.do?psSelectedSeason=538888311&psSelectedDivision=908194183&psSelectedLeague=838595926" onclick="window.open(this.href); return false;">League Table</a></li>
                        </ul>
                    </li-->
                    </ul>
				</div>
		    </div>
		</div>
	    </div>
	    <div id="main">
			<div id="content">
                <!--div id="clockdiv">
                    <h2><a href="/shop/index.php?route=product/category&path=65">#RebelsReturn</a> home in <span class="days"></span> days, <span class="hours"></span> hours, <span class="minutes"></span> minutes and <span class="seconds"></span> seconds</h2>
                </div-->
<?php if(isset($blocks['notification'])) echo $blocks['notification']; ?>

<?php
	// Show the body
	echo $blocks['body'];
	// Show the sidebar (if present)
	if (isset($blocks['sidebar'])) echo $blocks['sidebar'];
?>
			</div>
     		<br class="clear" />
     	</div>
	</div>

	<div id="footer_container">
    <div id="footer">
        <div id="sponsors">
            <a href="http://www.sloughtownsupporterstrust.co.uk" target="_blank"><img src="/images/sponsors/trust.png" alt="Slough Town Supporters Trust" id="sponsor_trust" /></a>
            <a href="http://www.morgansindall.com" target="_blank"><img src="/images/sponsors/morgan-sindall.jpg" alt="Morgan Sindall" id="sponsor_morgan" /></a> 
            <a href="http://macronstorecardiff.co.uk" target="_blank"><img src="/images/sponsors/macron.jpg" alt="Macron sponsor Slough Town" id="sponsor_macron" /></a>            
            <a href="http://garyhousephotography.co.uk" target="_blank"><img src="/images/sponsors/gary_house.jpg" alt="Gary House Photography sponsor Slough Town" id="sponsor_myfc" /></a>            
            <a href="http://www.segro.com" target="_blank"><img src="/images/sponsors/segro.jpg" alt="SEGRO" id="segro" /></a>
            <a href="http://www.bigfreebet.com" target="_blank"><img src="/images/sponsors/bigfreebet.jpg" alt="BigFreeBet" id="league_sponsor" /></a>
        </div> 
        <div id="credit">
            <p>Quick Links: <a href="/news">News</a> | <a href="/fixtures">Reports</a> | <a href="/away-directions">Directions</a> | <a href="/partners">Sponsorship</a> | <a href="/shop">Shop</a> | <a href="/contacts">Contact Us</a> | <a href="/privacy-policy">Privacy Policy</a></p>
            <p>Pictures: <a href="http://garyhousephotography.co.uk/" target="_blank">Gary House Photography</a> | &copy; Slough Town FC <?php echo date('Y'); ?></p>
        </div>
        <div id="scarf"></div>
    </div>
    </div>

<script type="text/javascript">

function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime) {
  var clock = document.getElementById(id);
  var daysSpan = clock.querySelector('.days');
  var hoursSpan = clock.querySelector('.hours');
  var minutesSpan = clock.querySelector('.minutes');
  var secondsSpan = clock.querySelector('.seconds');

  function updateClock() {
    var t = getTimeRemaining(endtime);

    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}

var deadline = 'August 29 2016 14:59:59';
initializeClock('clockdiv', deadline);

</script>
</body>
</html>