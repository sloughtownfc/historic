<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	<?php echo $blocks['head']; ?>
	</head>
<body>

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Slough Town FC Dashboard</a>
    </div>
    <div class="navbar-collapse collapse">        
      <ul class="nav navbar-nav navbar-right">
        <li><a href="http://www.sloughtownfc.net">Main Site</a></li>
        <li><a href="/admin">Dashboard</a></li>
        <li><a href="/logout">Log Out</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>


<div class="push container">

<?php if(isset($blocks['notification']))
{
	$blocks['body']->notification = $blocks['notification'];
}
?>

<?php echo $blocks['body']; ?>

</div> <!-- /container -->


 	<div id="footer">
      <div class="container">
        <p class="text-muted">&copy; Slough Town FC</p>
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.validationEngine-en.js"></script>
    <script src="/js/jquery.validationEngine.js"></script>
  	<script src="/js/bootstrap-datepicker.js"></script>
  	<script src="/js/bootstrap-timepicker.js"></script>
  	<script src="/js/tagsinput.js"></script>
  	<script src="/js/colorpicker.js"></script>
    
	<script>
		$(document).ready(function() {
			$("form").validationEngine();

			$('#tags_1').tagsInput({
			   'height':'100px',
			   'width':'300px',
			   'unique':true,
			   'defaultText':'Add tags'
			});

			$('.datepicker').datepicker({ format: 'yyyy-mm-dd' });

			$('.timepicker').timepicker({
          minuteStep: 5,
          showInputs: false,
          disableFocus: true
      });

			$('.colorpicker').minicolors();

			$('.gallery').click( function() {
				galleryEl = $(this).parents('.form-group').find('input');
				pickerWindow = window.open('/admin/gallery/' + galleryEl.attr('name'), 'picker', 'height=600,width=997,menubar=no,location=no,status=no');
				pickerWindow.focus();
				return false;
			});
		});
	</script>

</body>
</html>