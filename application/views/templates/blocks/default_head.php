<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="keywords" content="Slough Town FC, Slough Town Football Club, Slough Town, Rebels, Slough FC, Football, Southern League, Non League football, STFC, Slough, sloughtownfc.net" />
	<meta name="description" content="<?php if(isset($title)) { echo html::specialchars($title); } ?>" />
	<meta name="google-site-verification" content="NQXbM_LJ1twsLLEnAFsx2vPg-zHyNG5Qy_TW1_c9cRo" />
	<link rel="shortcut icon" href="/images/favicon.png" />
	<link rel="apple-touch-icon" href="apple-touch-icon.png" />
	<meta http-equiv="content-canguage" content="EN" />
	<meta name="copyright" content="Slough Town FC" />
	<meta name="revisit-after" content="14 days" />
	<meta name="distribution" content="global" />
	<meta name="resource-type" content="document" />
    
    <meta property="og:title" content="Slough Town FC"/>
    <meta property="og:description"  content="<?php if(isset($title)) { echo html::specialchars($title); } ?>"/> 
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php  echo "http://" . $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>"/>
    <meta property="og:image" content="http://www.sloughtownfc.net/images/facebook_share.jpg"/>
    <meta property="og:site_name" content="www.sloughtownfc.net" />
    <meta property="fb:admins" content="514138161" />

	<title><?php if(isset($title)) { echo html::specialchars($title) . ' | '; } ?>The Official Website of Slough Town FC </title>

	<link rel="stylesheet" type="text/css" href="/css/reset-min.css" />
	<link rel="stylesheet" type="text/css" href="/css/main.css" />
    <link rel="stylesheet" type="text/css" href="/css/admin/validationEngine.jquery.css" />
	<!--<link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 480px)" href="css/mobile.css" />-->
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type='text/javascript' src='/js/jquery.aviaSlider.js'></script>
    <script type='text/javascript' src='/js/jquery.validationEngine-en.js'></script>
    <script type='text/javascript' src='/js/jquery.validationEngine.js'></script>
	<script type="text/javascript" src="/js/ui_main.js"></script>

	<script type="text/javascript" src="/js/scripts.js"></script>
    
    <!-- Analytics -->
    <script type="text/javascript">
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-2349973-1', 'sloughtownfc.net');
	  ga('require', 'displayfeatures');
	  ga('send', 'pageview');
	
	</script>

	<script type="application/ld+json">
		{
		  "@context": "http://schema.org",
		  "@type": "Organization",
		  "url": "http://www.sloughtownfc.net",
		  "name": "Slough Town FC",
		  "logo": "http://www.sloughtownfc.net/images/main/badge_big.jpg",
		  "sameAs": [
			    "https://www.twitter.com/sloughtownfc",
			    "https://www.facebook.com/sloughtownfc",
			    "https://www.youtube.com/user/sloughtownfc",
			    "https://www.instagram.com/sloughtownfc"
			  ]
		}
</script>