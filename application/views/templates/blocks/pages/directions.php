<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<script type="text/javascript">
	var directionDisplay;
	var directionsService = new google.maps.DirectionsService();
	function initialize() {
		var latlng = new google.maps.LatLng(<?php echo $grounds_google; ?>);
		directionsDisplay = new google.maps.DirectionsRenderer();
		var myOptions = {
			zoom: 14,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: true
		};
		var map = new google.maps.Map(document.getElementById("the_map"),myOptions);
		directionsDisplay.setMap(map);
		directionsDisplay.setPanel(document.getElementById("directionsPanel"));
		var marker = new google.maps.Marker({
			position: latlng, 
			map: map
		}); 
	}
	function calcRoute() {
		var start = document.getElementById("routeStart").value;
		var end = "<?php echo $grounds_google; ?>";
		var request = {
			origin:start,
			destination:end,
			travelMode: google.maps.DirectionsTravelMode.DRIVING
		};
		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			} else {
				if (status == 'ZERO_RESULTS') {
					alert('No route could be found between the origin and destination.');
				} else if (status == 'UNKNOWN_ERROR') {
					alert('A directions request could not be processed due to a server error. The request may succeed if you try again.');
				} else if (status == 'REQUEST_DENIED') {
					alert('This webpage is not allowed to use the directions service.');
				} else if (status == 'OVER_QUERY_LIMIT') {
					alert('The webpage has gone over the requests limit in too short a period of time.');
				} else if (status == 'NOT_FOUND') {
					alert('At least one of the origin, destination, or waypoints could not be geocoded.');
				} else if (status == 'INVALID_REQUEST') {
					alert('The DirectionsRequest provided was invalid.');					
				} else {
					alert("There was an unknown error in your request. Requeststatus: \n\n"+status);
				}
			}
		});
	}
</script>

<div id="content_area">
    
    <img src="<?php echo $logo; ?>" alt="Club Badge" class="right" style="border:none;" />
            
    <h1><?php echo $opposition; ?></h1>
    
    <p><?php echo $grounds_address; ?></p>
    
    <p><?php echo $grounds_telephone; ?></p>
    
    <?php
    if($website != NULL)
		{
    		echo '<p><a href="'. $website . '" onclick="window.open(this.href); return false;">' . $website . '</a></p>';
		}
	?>
	
	
    <br class="clear" />
    
    <h2>Directions</h2>
    
    <?php
    	echo '<p>';
		
		echo str_replace(array("\r\n", "\n"), array("<br />", "<br />"), $grounds_directions);
		
		echo '</p>';
	?>
    
    <br class="clear" />
    
    <h2>Create Your Own Directions</h2>
    
    <form onSubmit="calcRoute();return false;" id="routeForm">
		<input type="text" id="routeStart" value="Enter a location and press Create" title="Enter a location and press Create" class="clearMeFocus directions" >
		<input type="submit" value="Calculate route" class="directions">
	</form>
    
    <div class="map_container">
    	<div id="the_map"></div>
    </div>	
    
	<br class="clear" />
    
	<div id="directionsPanel"></div>

</div><!--End Content Area -->