<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

    <h1><?php echo $headline; ?></h1>

    <div class="scorebox">

    <img src="<?php if( $is_home == 1) { echo '/images/main/badge.jpg'; } else { echo $home_badge; } ?>" alt="Home Badge" class="reportbadge" height="75" width="75" />

        <div class="scorebox_middle">

            <div class="matchdetails" style="background: <?php echo $home_background; ?>">
                <p class="team" style="color: <?php echo $home_foreground; ?>"><?php echo $home_team; ?></p>
            </div><!-- End Top Team -->

            <div class="matchdetails">
                <p class="nomargin bold"><?php echo $home_scorers; ?></p>
            </div><!-- End Bottom Scorers -->

        </div><!-- Scorebox_middle -->

        <div class="score">
            <?php echo $home_score; ?>
        </div><!-- End Score -->

    </div><!-- Score box -->

    <div class="scorebox">

    <img src="<?php if( $is_home != 1) { echo '/images/main/badge.jpg'; } else {  echo $away_badge; } ?>" alt="Away Badge" class="reportbadge" height="75" width="75" />

        <div class="scorebox_middle">

            <div class="matchdetails" style="background: <?php echo $away_background; ?>">
                <p class="team" style="color: <?php echo $away_foreground; ?>"><?php echo $away_team; ?></p>
            </div><!-- End Top Team -->

            <div class="matchdetails">
                <p class="nomargin bold"><?php echo $away_scorers; ?></p>
            </div><!-- End Bottom Scorers -->

        </div><!-- Scorebox_middle -->

        <div class="score">
            <?php echo $away_score; ?>
        </div><!-- End Score -->

    </div><!-- Score box -->

    <div class="extrainfo centre">

        <p class="nomargin"><span class="bold">Date:</span> <?php echo date('l, F jS Y', strtotime($match_date)); ?> | <span class="bold">Competition:</span> <?php echo $competition; ?> | <span class="bold">Attendance:</span> <?php echo (($attendance) == NULL ? 'TBC' : $attendance ); ?></p>

        <p class="nomargin"><span class="bold">Man Of Match:</span> <?php echo $mom; ?></p>

        <?php if($is_home)
		{
		?>
			<p class="nomargin">
		<?php
			if (!empty($match_sponsor))
			{
		?>
        		<span class="bold">Match Sponsor:</span> <?php echo $match_sponsor;
			}
			if (!empty($ball_sponsor))
			{
				if (!empty($match_sponsor))
				{
					echo ' |';
				}
			?> <span class="bold">Ball Sponsor:</span> <?php echo $ball_sponsor; ?>
        	<?php
			}
			?>
			</p>
		<?php
        }
		?>

    </div><!-- End Visitors -->

    <div id="lineup">

        <h3 class="squad">Starting Line Up</h3>

        <ul class="lineup">
        	<?php
        		foreach ($lineup as $stat)
        		{
        			// Generate player information variables
        			$player_id = $stat->player->id;
        			$player_name = $stat->player->first_name . ' ' . $stat->player->last_name;
        			$player_shirt = $stat->shirt;
        			// Open list object
        			echo '<li>';
        			// Print the basics
        			echo "<img src=\"/images/icons/report/squads/squad$player_shirt.png\" alt=\"$stat->shirt\" class=\"report\" /><a href=\"/profile/$player_id\">$player_name</a>";
        			// Check goal count
        			if ($stat->goals == 1)
        			{
        				$goal_url = '/images/icons/report/1goal.png';
        				echo "<img src=\"$goal_url\" alt=\"$stat->goals\" class=\"report\" />";
        			}
        			elseif($stat->goals > 1)
        			{
						$goal_url = '/images/icons/report/'.$stat->goals.'goals.png';
						echo "<img src=\"$goal_url\" alt=\"$stat->goals\" class=\"report\" />";
        			}
        			// Check for injury
        			if ($stat->injured)
        			{
						echo '<img src="/images/icons/report/cross.png" alt="Injured" class="report" />';
        			}
					// Check for substitution
					if ($stat->sub_shirt)
					{
						echo "<img src=\"/images/icons/report/squads/sub$stat->sub_shirt.png\" alt=\"Substitute\" class=\"report\" />";
					}
					// Check for bookings
					if ($stat->bookings == 1)
					{
						echo '<img src="/images/icons/report/yellow.png" alt="Yellow Card" class="report" />';
					}
					elseif($stat->bookings == 2)
					{
						echo '<img src="/images/icons/report/red.png" alt="Red Card" class="report" />';
					}
					// Close list object
					echo '</li>';
        		}
        	?>
        </ul>

        <h3 class="squad">Substitutes</h3>

        <ul class="lineup">
            <?php
        		foreach ($subs as $stat)
        		{
        			// Generate player information variables
        			$player_id = $stat->player->id;
        			$player_name = $stat->player->first_name . ' ' . $stat->player->last_name;
        			$player_shirt = $stat->shirt;
        			// Open list object
        			echo '<li>';
        			// Print the basics
        			echo "<img src=\"/images/icons/report/squads/squad$player_shirt.png\" alt=\"$stat->shirt\" class=\"report\" /><a href=\"/profile/$player_id\">$player_name</a>";
        			// Check goal count
        			if ($stat->goals == 1)
        			{
        				$goal_url = '/images/icons/report/1goal.png';
        				echo "<img src=\"$goal_url\" alt=\"$stat->goals\" class=\"report\" />";
        			}
        			elseif($stat->goals > 1)
        			{
						$goal_url = '/images/icons/report/'.$stat->goals.'goals.png';
						echo "<img src=\"$goal_url\" alt=\"$stat->goals\" class=\"report\" />";
        			}
        			// Check for injury
        			if ($stat->injured)
        			{
						echo '<img src="/images/icons/report/cross.png" alt="Injured" class="report" />';
        			}
					// Check for substitution
					if ($stat->sub_shirt)
					{
						echo "<img src=\"/images/icons/report/squads/sub$stat->sub_shirt.png\" alt=\"Substitute\" class=\"report\" />";
					}
					// Check for bookings
					if ($stat->bookings == 1)
					{
						echo '<img src="/images/icons/report/yellow.png" alt="Yellow Card" class="report" />';
					}
					elseif($stat->bookings == 2)
					{
						echo '<img src="/images/icons/report/red.png" alt="Red Card" class="report" />';
					}
					// Close list object
					echo '</li>';
        		}
        	?>
        </ul>

    </div><!-- End Line up -->

    <?php
	$body_count = count($body);
	if ($body_count > 0)
	{
	    echo "<p><b>$body[0]</b></p>";
	    for ($i = 1; $i < $body_count; $i++)
	    {
	    	echo "<p>$body[$i]</p>";
	    }
	}

	if($opposition_team != NULL)
	{
		echo'<div class="extrainfo">';

		echo'<p class="nomargin"><span class="bold">' . $opposition .':</span> ' . $opposition_team . '</p>';

		echo'</div><!-- End Visitors -->';
	}
	else
	{
		echo '<br class="clear"/><hr />';
	}

    ?>

    <br class="clear" />

    <?php

	if($video_url != NULL)
	{

     echo'<h2>Match Highlights</h2>';

     echo'<iframe style="margin-bottom:10px;" id="video-player" type="text/html" width="640" height="340" src="http://www.youtube.com/embed/' .$video_url. '" allowfullscreen frameborder="0"></iframe>';

    echo'<br class="clear" />';
	}

	?>

    <?php

    	if($flickr_url != NULL)
		{
			echo'<p><a class="external_link" href="' .$flickr_url. '">Click to view Gary House Photography of this Match</a></p>';
		}

	?>

    <div id="social">

        <iframe src="http://www.facebook.com/plugins/like.php?app_id=148739918469452&amp;href=<?php  echo "http://" . $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:50px; height:20px;" allowTransparency="true"></iframe>

        <a href="http://twitter.com/share" class="twitter-share-button" data-count="none" data-via="sloughtownfc">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>

        <g:plusone size="medium" count="false"></g:plusone><script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

    </div><!-- End Social -->

</div><!--End Content Area -->