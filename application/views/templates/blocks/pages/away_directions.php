<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<script type="application/javascript">
	$(document).ready(function(){
		$('.the_statistics tbody tr:odd td').css('background', '#f1f1f1');
	});
</script>

<div id="content_area">

	<div class="opener">
            
    	<h1>Away Directions</h1>
        
        <p class="bold">Directions to our forthcoming opponents.</p>
   
   	</div><!-- Close Opener -->
   
   	<table class="the_statistics" summary="Oppostion Directions">
		<thead>
			<tr>
				<th style="padding-left:5px;">Club</th>
                <th class="centre">Venue</th>
				<th class="centre">Directions</th>
			</tr>
		</thead>
		<tbody>
		<?php		
		foreach ($oppo as $direction)
		{
		?>
		<tr>
			<td bgcolor="#ffffff" style="padding-left:5px;"><?php echo $direction->name; ?></td>
            <td bgcolor="#ffffff" class="centre"><?php echo $direction->grounds_venue; ?></td>
			<td bgcolor="#ffffff" class="centre"><a href="/directions/<?php echo $direction->id; ?>">More Information</a></td>
		</tr>
		<?php
		}
		?>
		</tbody>
	</table>
    
</div><!--End Content Area -->