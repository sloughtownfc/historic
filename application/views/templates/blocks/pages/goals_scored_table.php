<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<?php  

$xmlstr = file_get_contents('http://www.footballwebpages.co.uk/league.xml?comp=8&sort=scored'); // read your file  

$xml = new SimpleXMLElement($xmlstr);

?> 

<div id="content_area">
            
    <div class="opener">

        <h1>League Table</h1>
        
        <p class="bold">Slough Town play in the Evo-Stik League Southern Premier Division.</p>
        
        <p>Listed below is the full league table for our division. This league table is updated within an hour or so of results coming through to us from our division.</p>

    </div><!-- End Opener -->
    
    <br class="clear" />
    
    <p class="centre"><a href="/league-table" title="League Table">League Table</a> | <a href="/home-league" title="Home League Form Table">Home League Form</a> | <a href="/away-league" title="Away League Form Table">Away League Form</a> | <a href="/goals-scored" title="League Goals Scored Table">Goals Scored</a> | <a href="/attendance" title="Average Attendances Table">Average Attendances</a></p>
    
    <?php  
        echo "<h2>{$xml->competition} - {$xml->description}</h2>" . "\n"; 

        echo "<table>" . "\n";
        echo "<thead>" . "\n"; 
        echo "<tr>" . "\n"; 
        echo "<th class='centre'>Position</th>" . "\n";
        echo "<th>Name</th>" . "\n";
        echo "<th class='centre'>Played</th>" . "\n";
        echo "<th class='centre'>Won</th>" . "\n";
        echo "<th class='centre'>Drawn</th>" . "\n";
        echo "<th class='centre'>Lost</th>" . "\n";
        echo "<th class='centre'>For</th>" . "\n";
        echo "<th class='centre'>Against</th>" . "\n";
        echo "<th class='centre'>GD</th>" . "\n";
        echo "<th class='centre'>Points</th>" . "\n";
        echo "</tr>" . "\n";
        echo "</thead>" . "\n"; 
         
        foreach ( $xml->team as $team ) { 
        
			// Set the BG Conditions 
			if($team->name=="Slough Town"){ 
			$bg = '#efaf12'; 
			
			}  else if ($team->position=="1") {
			$bg = '#FFFF00'; 
			
			}  else {
			$bg = ($bg=='#f1f1f1' ? '#ffffff' : '#f1f1f1');
			}
        
        
			echo "<tr style=\"background-color:". $bg ."\">\n"; 
			echo "<td class='centre'>{$team->position}</td>" . "\n";
			echo "<td>{$team->name}</td>" . "\n"; 
			echo "<td class='centre'>{$team->played}</td>" . "\n";
			echo "<td class='centre'>{$team->won}</td>" . "\n";
			echo "<td class='centre'>{$team->drawn}</td>" . "\n";
			echo "<td class='centre'>{$team->lost}</td>" . "\n";
			echo "<td class='centre'>{$team->for}</td>" . "\n"; 
			echo "<td class='centre'>{$team->against}</td>" . "\n";
			echo "<td class='centre'>{$team->goalDifference}</td>" . "\n";
			echo "<td class='centre'>{$team->points}</td>" . "\n";
			echo "</tr>" . "\n";
        } 
        
        echo "</table>" . "\n"; 
        
        ?> 
   
</div><!--End Content Area -->
