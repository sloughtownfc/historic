<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">
            	           
    <h1>Supporters Coach: <?php echo $name; ?></h1>
    
    <img src="<?php echo $logo; ?>" alt="Club Badge" class="right" style="border:none;" />
    
    <p><span class="bold">Match Date:</span> <?php $newDate = date("d-m-Y", strtotime($match_date)); echo $newDate; ?></p>
    
    <p><span class="bold">Departs:</span> <?php if($departs != NULL) { echo $departs; } else { echo 'TBC'; } ?></p>
    
    <p><span class="bold">Departing From:</span> St Joseph's School.</p>
    
    <p><span class="bold">Adult Price:</span> <?php if($adult != NULL) { echo $adult; } else { echo 'TBC'; } ?></p>
    
    <p><span class="bold">Seniors &amp; Juniors Price:</span> <?php if($reduced != NULL) { echo $reduced; } else { echo 'TBC'; } ?></p>
    
    <hr />
    
    <p>Book with Alan Harding on 07969 075712, e-mail <a href="mailto:alan.harding9@btinternet.com?subject=Coach Booking">alan.harding9@btinternet.com</a> or speak to a supporters trust member at any of our home games near to the turnstiles on matchdays.</p>
    
    <p>Please note:  Supporters intending to use the coaches this season are advised to join the Supporters Trust. Non-members will be charged an extra &pound;5.</p>
    
</div><!--End Content Area -->
