<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

   <?php 
	
	// Set home/away teams
	if ($is_home == 1)
	{
		echo '<h1 class="centre">Slough Town ' . $home_score . '-' . $away_score . ' ' . $opposition.'</h1>';
	}
	else
	{
		echo '<h1 class="centre">'. $opposition.' ' . $home_score . '-' . $away_score . ' Slough Town</h1>';	
	}
	
	
	?>
    
    <hr />
    
    <?php
	if ($is_home == 1)
	{
		if($home_scorers != NULL)
		{
    
    	 echo '<p class="bold">Slough scorers: ' . $home_scorers .'</p>';
        
        }
	}
	else
	{
		if($away_scorers != NULL)
		{
    
    	 echo '<p class="bold">Slough scorers: ' . $away_scorers .'</p>';
        
        }
	}        
    ?>
    
    <div class="extrainfo centre">   
    
        <p class="nomargin"><span class="bold">Date:</span> <?php $newDate = date("d-m-Y", strtotime($match_date)); echo $newDate; ?> | <span class="bold">Competition:</span> <?php echo $competition; ?></p>
                        
    </div><!-- End Visitors -->
    
    <?php
		if($body != NULL)
		{
			$body_count = count($body);
			if ($body_count > 0)
			{
				echo "<p><b>$body[0]</b></p>";
				for ($i = 1; $i < $body_count; $i++)
				{
					echo "<p>$body[$i]</p>";
				}
			}	
		}
		else
		{
			echo '<p>Awaiting report.</p>';
		}
		
		if($report_team != NULL)
		{
			echo'<div class="extrainfo">';	
			
			echo'<p class="nomargin"><span class="bold">Team:</span> ' . $report_team .'</p>';	
			
			echo'</div><!-- End Team -->';	
		}
		else
		{
			echo '<br class="clear"/><hr />';	
		}
			
	?>
    
     <div id="social">
    
        <iframe src="http://www.facebook.com/plugins/like.php?app_id=148739918469452&amp;href=<?php  echo "http://" . $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:50px; height:20px;" allowTransparency="true"></iframe>
        
        <a href="http://twitter.com/share" class="twitter-share-button" data-count="none" data-via="sloughtown">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
        
        <g:plusone size="medium" count="false"></g:plusone><script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
    
    </div><!-- End Social -->

</div><!--End Content Area -->