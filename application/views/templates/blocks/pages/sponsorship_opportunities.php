<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

	<div class="opener">

		<h1>Sponsorship 2017/18</h1>
    
    	<p class="bold">There are many different ways to support and sponsor us, all of which make a BIG difference. We have tried to make sponsoring Slough Town FC as simple as possible.</p>
        
    </div><!-- End Opener -->
    
    <br class="clear" />

    <h2>Matchday Sponsorship Package - £200</h2>
    
    <!--img src="images/sponsorship/logo_sponsorship.jpg" alt="Slough Town FC Matchday sponsorship"  style="border:none;" class="right" /-->
    <p>A superb opportunity to raise your organisation’s profile whilst enjoying an afternoon or evening out at the state-of-the-art Arbour Park Community Stadium. This price applies to Southern League, Premier Division matches. An additional amount of £50 per person is payable for any additional guests, to a maximum of 8.</p>

    <p>Includes:</p>

    <ul>
        <p class="blue">- Name on Match day Programme</p>
        <p class="blue">- Name on Slough Town FC Website and Social Media</p>
        <p class="blue">- Name during Match day announcements</p>
        <p class="blue">- Entry for 4 people to boardroom</p>
        <p class="blue">- Reserved Arbour Park parking for 2 vehicles</p>
        <p class="blue">- 4 Reserved Seats in West Stand</p>
        <p class="blue">- 4 Match Day Programmes</p>
        <p class="blue">- Tea/Coffee and cakes/biscuits on arrival and at half time</p>
        <p class="blue">- Food and drink(s) after the match</p>
        <p class="blue">- Selection of Man of the Match, including photo with the player </p>
        <p class="blue">- Any additional guests at £50pp to a maximum of 8</p>
    </ul>
    
    <hr />

    <h2>Match ball sponsorship - £100</h2>

    <p>Sponsoring the match ball is another great way to promote your business locally. Your company’s name will feature in match day announcements and the match day programme. This price applies to Southern League, Premier Division matches. An additional amount of £50 per person is payable for any additional guests, to a maximum of 4.</p>
    
    <img src="images/sponsorship/matchball.jpg" alt="Slough Town FC Matchball sponsorship" class="right" />
        
    <p>Includes:</p>

    <p class="blue">- Name on Match day Programme</p>
    <p class="blue">- Name on Slough Town FC Website and Social Media Name during Match day announcements</p>
    <p class="blue">- Entry for 2 people to boardroom</p>
    <p class="blue">- Reserved Arbour Park parking for 1 vehicles</p>
    <p class="blue">- 2 Reserved Seats in West Stand</p>
    <p class="blue">- 2 Match Day Programmes</p>
    <p class="blue">- Tea/Coffee and cakes/biscuits on arrival and at half time Food and drink(s) after the match</p>
    
    <hr />

    <h2>Mascot - £50 (Junior Rebels - £30)</h2>
    
    <p>Give your child a day to remember with the match day mascot experience at Slough Town Football Club.</p>

    <img src="images/sponsorship/mascots.jpg" alt="Slough Town FC Mascots" class="right" />
    
    <p>Includes:</p>

    <p class="blue">- Article in Match day programme</p>
    <p class="blue">- One set of Replica Kit (Junior Rebels receive new Shorts/Socks only)</p>
    <p class="blue">- Name during Match day announcements</p>
    <p class="blue">- Entry for 1 person to the ground</p>
    <p class="blue">- 1 Match day programme</p>
    <p class="blue">- Reserved Arbour Park parking for 1 vehicles</p>
    <p class="blue">- Lead out the team and participate in pre match handshake</p>
    <p class="blue">- Memento Photo of the day</p>
    
    <hr />

    <h2>Perimeter Advertising Boards - £500 per season, per board</h2>

    <p>We expect over 14,000 visitors to Arbour Park this season to watch Slough Town in action – and the stadium also hosts many other sporting activities as well as meetings and conferences, so that number will be even higher. In addition, advertising boards will be shown prominently on video highlights and local print media. Advertising boards therefore offer you an excellent way to promote your company to a captive audience and raise brand awareness.</p>
    
    <img src="images/sponsorship/board.jpg" alt="Slough Town FC Permieter Advertising" class="right" />
    
    <p>Includes:</p>
    <p class="blue">- 8’ x 3’ Colour Advertising Board in your bespoke design</p>
    <p class="blue">- In place at Arbour Park for all events including non-match days</p>
    <p class="blue">- Visible for all of Slough Town’s home fixtures</p>
    <p class="blue">- Feature as a backdrop to action videoing and photography appearing in the media, both locally and through our official website, plus our own match day programme.</p>
    <p class="blue">- Photo and promotion through social media when board installed</p>

    <hr />

    <h2>Video - £3000 per season</h2>
        
    <p>Feel like part of the team helping Slough in their push for Promotion whilst promoting your business or organisation!</p>
    <p>The video sponsor will be visible and appear on match highlights provided by our supplier and published through all social media and through our official website. This is a fantastic opportunity to have high levels of promotion on a repeat basis with minimal effort!</p>

    <hr />

    <h2>Website - £POA</h2>
        
    <p>Have the name of your company appear on the Slough Town official website, with annual visitors of in excess of 81,000. This is a great opportunity to support your local club and raise the presence of your business.</p>

    <hr />

    <h2>Player - £100</h2>
        
    <p>Sponsor a player and their kit for one season. Your name will appear as sponsoring the player in the match day programme and at the end of the season you will receive their signed shirt. Note that player sponsorship is non transferrable.</p>

    <hr />
    
    <h2>Full page programme - £350</h2>

    <p>Gives you a personalised full page advert in the match day programme for all Slough Town matches.</p>

    <hr />

    <h2>Half page programme - £200</h2>

    <p>Gives you a personalised half page advert in the match day programme for all Slough Town matches.</p>

    <hr />

    <h2>Community Programme Page - £50</h2>

    <p>Gives you a cost effective way to get your company logo/image in the match day programme for all Slough Town matches, alongside other valued club supporters. Note: There will be approx. 10 per page.</p>

    <hr />

    <h2>Kit - £POA</h2>

    <p>Have your company name on the first team match day kit.</p>

    <p>Kit sponsorship is the premier offering and generates the highest amount of exposure for any business or organisation.</p>

    <p class="blue">The sponsor will be appear in match official video and photo, published through all social media and through our official website, as well as any external media.</p>
    <p class="blue">The sponsor will also appear on the match day programme and official website.</p>
    <p class="blue">The sponsor will appear on replica jerseys sold via the club shop.</p>
    <p class="blue">Other Kit and Equipment sponsorship opportunities are available on request.</p>

    <h2>More Information?</h2>
    
    <p>Please fill out the form below for further information ahead of the 2017/18 season.</p>
    
     <?php
	// Open form.
	echo form::open();
	?>
	<fieldset>

	<?php if (isset($errors['name'])) echo $errors['name']; ?>
	<input type="text" name="name" title="Name" value="<?php echo (isset($message_data['name'])) ? $message_data['name'] : 'Name'; ?>" class="clearMeFocus" />
	<br />

	<?php if (isset($errors['return_email'])) echo $errors['return_email']; ?>
	<input type="text" name="return_email" title="E-mail" value="<?php echo (isset($message_data['return_email'])) ? $message_data['return_email'] : 'E-mail'; ?>" class="clearMeFocus" />
	<br />

	<?php if (isset($errors['subject'])) echo $errors['subject']; ?>
	<input type="text" name="subject" title="Subject" value="<?php echo (isset($message_data['subject'])) ? $message_data['subject'] : 'Subject'; ?>" class="clearMeFocus" />
	<br />

	<?php if (isset($errors['body'])) echo $errors['body']; ?>
	<textarea name="body" cols="5" rows="5" id="text"><?php echo (isset($message_data['body'])) ? $message_data['body'] : 'Your enquiry'; ?></textarea>

	<br />
	<div style="margin-left: 62px">
	<?php
		// Show the CAPTCHA
		echo $captcha;
		// Show CAPTCHA error, if applicable
		if (isset($errors['captcha'])) echo $errors['captcha'];
	?>
    	<br class="clear"/>
        
       	<p class="nomargin">Confirm the letters and numbers above.</p>
    
	</div>

	<?php
		// Show CAPTCHA input
		echo form::input('captcha_response');
	?>

	<input type="submit" name="submit" value="Send" />

	</fieldset>
	<?php
    // Close form.
	echo form::close();
	?>
    
</div><!--End Content Area -->