<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<script type="text/javascript">
$(document).ready(function(){
	var current_season = <?php echo $season->name; ?>;
	var selected_season = <?php echo $season_name; ?>;
	
	if(current_season != selected_season) {
		$('.TBC').css('display','none');
	}
	
	if((current_season == selected_season) && ($('table.TBC tbody').html().length<1)) {
		$('.TBC').css('display','none');
	}
});
</script>

<div id="content_area">
            
    <div class="opener">

        <h1>Slough Town U18s Fixtures and Reports <?php echo $season_name; ?></h1>
        
        <p class="bold">Please find a list below of the Current fixtures and results for the <?php echo $season_name; ?> season as it happens for our Under 18 side.</p>
        
        <p>Please note however that ALL matches are subject to change, and before leaving for a match check on the Official site that the match has not been cancelled/called off for any particular reason. </p>
        
        <?php echo form::open(); ?>
        <select id="season" name="season">
        <?php
            foreach ($seasons as $id => $name)
            {
                // Check whether this season is selected
                if ($id === $season_selected)
                {
                    echo '<option value="' . $id . '" selected="selected">' . $name . '</option>';
                }
                else
                {
                    echo '<option value="' . $id . '">' . $name . '</option>';
                }
            }
        ?>
        </select>
        <?php
            echo form::submit('submit', 'Change Season');
            echo form::close();
        ?>
        
    </div><!-- End Opener -->
    
    <br class="clear" />
  
    <?php
				foreach ($grouped_fixtures as $group => $fixtures)
				{
					echo "<h2 class='$group'>$group Fixtures/Results</h2>";
			?>
			<table class="the_statistics <?php echo $group; ?>" summary="Fixtures &amp; Reports">
			<thead>
				<tr>
					<th style="width:20%" class="centre">Match Date</th>
					<th style="width:25%" class="centre">Home Team</th>
					<th style="width:5%" class="centre">Score</th>
					<th style="width:25%" class="centre">Away Team</th>
					<th style="width:16%" class="centre">Competition</th>
					<th style="width:14%" class="centre">Report</th>
				</tr>
			</thead>
			<tbody><?php
					foreach ($fixtures as $fixture)
					{
						// Open row
						echo '<tr>';
						
						$newDate = date("d-m-Y", strtotime($fixture->match_date));

						// Match date
						echo '<td bgcolor="#ffffff" class="centre">' . (($fixture->match_date === NULL) ? 'TBC' : $newDate) . '</td>';
						// Set home/away teams
						if ($fixture->is_home)
						{
							$home_team = 'Slough Town U18s';
							$away_team = $fixture->opposition;
						}
						else
						{
							$home_team = $fixture->opposition;
							$away_team = 'Slough Town U18s';
						}
						// Home Team
						echo '<td bgcolor="#ffffff" class="centre">' . $home_team . '</td>';

						// TODO: This is ugly, should have belong_to / has_many in the models
						// and then reference like $fixture->report->report_home_score etc.

						// Score
						echo '<td bgcolor="#ffffff" class="centre">' . $fixture->report_home_score . '-' .  $fixture->report_away_score . '</td>';
						// Away Team
						echo '<td bgcolor="#ffffff" class="centre">' . $away_team . '</td>';
						// Competition
						echo '<td bgcolor="#ffffff" class="centre">' . $fixture->competition . '</td>';

						// TODO: This is ugly, should have belong_to / has_many in the models
						// and then reference like $fixture->report->report_home_score etc.

						// Set report link / if present
						if ($fixture->report_published === 1)
						{
							$report_link = '<a href="/u18-report/' . $fixture->id . '/">Report</a>';
						}
						else
						{
							$report_link = '---';
						}

						// Report
						echo '<td bgcolor="#ffffff" class="centre">' . $report_link . '</td>';

						// Close row
						echo '</tr>';
					}
				?></tbody>
			</table>
		<?php
				}
		?>
</div><!--End Content Area -->