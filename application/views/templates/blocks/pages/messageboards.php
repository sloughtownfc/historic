<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">
            
    <div class="opener messageboard">

        <h1>Messageboards</h1>
        
        <p class="bold">The club and the Official Website cannot be held responsible for any messages which appear on the message boards given below. All views expressed on their message board remain those of the individual user/author and not those of the club and/or it's employees and/or agents.</p>
        
        <p>As a polite request, please do not post messages which could be reasonably construed as one or any of the following, defamatory, abusive, racist or obscene. Any material posted on the message board should also not be deemed undesirable, illegal or in breach of any person's rights.</p>

        <p>This is of course in addition to any terms and conditions set-out on respective forums.</p>

    </div><!-- End Opener -->
    
    <br class="clear" />
    
    <h2>Forums</h2>
    
    <p><a class='external_link' href="http://www.nonleague.co.uk/forum/24-slough-town/" onClick="window.open(this.href); return false;">Unofficial Rebels Forum</a></p>
               
</div><!--End Content Area -->
