<?php defined("SYSPATH") OR die("No direct access allowed."); ?>

<script type="application/javascript">
	$(document).ready(function(){
		$('.the_statistics tbody tr:odd td').css('background', '#f1f1f1');
	});
</script>

<div id="content_area">

	<div class="opener">

		<h1>Player Statistics</h1>

		<p class="bold">Please find a list of player statistics from the current season showing appearances, goals, and cards.</p>

		<p>Statistics cover ALL competitive competitions for the first team squad and do not include friendly matches.</p>

	</div><!-- End Opener -->

	<br class="clear" />

	<h2>Currently playing Slough Town players</h2>

	<table class="the_statistics" summary="Player Statistics">
	<thead>
		<tr>
			<th style="width:50%; padding-left:5px;">Name</th>
			<th class="centre" style="width:12%;">Appearances</th>
			<th class="centre" style="width:12%;">Yellows</th>
			<th class="centre" style="width:12%;">Reds</th>
			<th class="centre" style="width:12%;">Goals</th>
		</tr>
	</thead>
	<tbody>
		<?php
        $yellow_total = 0;
        $red_total = 0;
        $goal_total = 0;
        
        foreach ($player_stats as $player_id => $stat)
        {							
            if ($stat['active'] == 1)
            {
                $yellow_total = $yellow_total + $stat['yellows'];
                $red_total = $red_total + $stat['reds'];
                $goal_total = $goal_total + $stat['goals'];
        ?>
        <tr>
            <td bgcolor="#ffffff" style="padding-left:5px;"><a href="/profile/<?php echo $player_id; ?>/<?php $link = str_replace(" ","-",$stat['name']); echo $link; ?>"><?php echo $stat['name']; ?></a></td>
            <td bgcolor="#ffffff" class="centre"><?php echo $stat['appearances']; ?></td>
            <td bgcolor="#ffffff" class="centre"><?php echo (isset($stat['yellows']) ? $stat['yellows'] : 0); ?></td>
            <td bgcolor="#ffffff" class="centre"><?php echo (isset($stat['reds']) ? $stat['reds'] : 0); ?></td>
            <td bgcolor="#ffffff" class="centre"><?php echo $stat['goals']; ?></td>
        </tr>
        <?php

            }
        }
        ?>
        <tr class="bold">
        	<td bgcolor="#ffffff" style="padding-left:5px;text-align:right;">Total</td>
            <td bgcolor="#ffffff" class="centre"></td>
            <td bgcolor="#ffffff" class="centre"><?php echo $yellow_total; ?></td>
            <td bgcolor="#ffffff" class="centre"><?php echo $red_total; ?></td>
            <td bgcolor="#ffffff" class="centre"><?php echo $goal_total; ?></td>
        </tr>
	</tbody>
	</table>

	<h2>Former Slough Town players who have played this season</h2>

	<table class="the_statistics" summary="Player Statistics">
	<thead>
		<tr>
			<th style="width:50%; padding-left:5px;">Name</th>
			<th class="centre" style="width:12%;">Appearances</th>
			<th class="centre" style="width:12%;">Yellows</th>
			<th class="centre" style="width:12%;">Reds</th>
			<th class="centre" style="width:12%;">Goals</th>
		</tr>
	</thead>
	<tbody>
		<?php
        $yellow_total2 = 0;
        $red_total2 = 0;
        $goal_total2 = 0;
    
        foreach ($player_stats as $player_id => $stat)
        {
            if ($stat['active'] == 0)
            {
                $yellow_total2 = $yellow_total2 + $stat['yellows'];
                $red_total2 = $red_total2 + $stat['reds'];
                $goal_total2 = $goal_total2 + $stat['goals'];
        ?>
        <tr>
            <td bgcolor="#ffffff" style="padding-left:5px;"><a href="/profile/<?php echo $player_id; ?>/<?php $link = str_replace(" ","-",$stat['name']); echo $link; ?>"><?php echo $stat['name']; ?></a></td>
            <td bgcolor="#ffffff" class="centre"><?php echo $stat['appearances']; ?></td>
            <td bgcolor="#ffffff" class="centre"><?php echo (isset($stat['yellows']) ? $stat['yellows'] : 0); ?></td>
            <td bgcolor="#ffffff" class="centre"><?php echo (isset($stat['reds']) ? $stat['reds'] : 0); ?></td>
            <td bgcolor="#ffffff" class="centre"><?php echo $stat['goals']; ?></td>
        </tr>
        <?php

            }
        }
        ?>
        <tr class="bold">
            <td bgcolor="#ffffff" style="padding-left:5px;text-align:right;">Total</td>
            <td bgcolor="#ffffff" class="centre"></td>
            <td bgcolor="#ffffff" class="centre"><?php echo $yellow_total2; ?></td>
            <td bgcolor="#ffffff" class="centre"><?php echo $red_total2; ?></td>
            <td bgcolor="#ffffff" class="centre"><?php echo $goal_total2; ?></td>
        </tr>
	</tbody>
	</table>

</div><!--End Content Area -->