<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">
            
    <div class="opener">

        <h1>Page Not Found</h1>
        
        <p class="bold">You may have clicked on an out of or broken link. </p>
        
        <p>Please <a href="/">click here</a> to return to the homepage</p>

    </div><!-- End Opener -->
    
    <br class="clear" />

</div><!--End Content Area -->