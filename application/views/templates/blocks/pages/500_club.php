<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

    <div class="opener">

        <h1>The 500 Club</h1>

        <p class="bold">The 500 Club is one of, if not the most popular fund raising activities in the UK, probably because it is easy to enter and the rewards can be substantial, both in terms of prize money and funds raised.</p>

        <p>The rules are very simple - the cost of entry to the draw is &pound;5 and all entrants are required to complete an entry form.</p>

        <p>The size of the monthly prizes is dictated by how many people join and Slough Town Football Club are committed to return 50% of the income in prizes. There will be a first, second and third prize and as the scheme grows more prizes will be added.</p>

        <p>The application form below includes a more detailed list of the rules of the 500 Club.</p>

    </div><!-- End Opener -->

    <br class="clear" />

    <h2>How to Join</h2>

    <p>Please print the application form and bank mandate form below. Please complete and return with the appropriate payment or duly completed standing order form to:</p>

    <p><span class="bold">The 500 Club</span><br />c/o Mr A Harding,<br />20 Nursery Road,<br />Taplow,<br />Nr Maidenhead,<br />Bucks,<br />SL6 0JZ</p>

    <p>Or call: 07969 075712</p>

    <p class="pdf"><a href="pdf/500club_application.pdf" target="_blank">Download Application Form</a></p>

</div><!--End Content Area -->