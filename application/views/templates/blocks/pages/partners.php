<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

    <div class="opener">

        <h1>STFC Partners - Making a Difference</h1>
        
        <p class="bold">Thank you for the time you have taken to get to this point, we really do appreciate it!</p>
        
        <p>There are many different ways to support and sponsor us, all of which make a BIG difference. We have tried to make sponsoring Slough Town FC as simple as possible.</p>
        
        <p>Please fill in your details below and hit submit. It could not be simpler! We will arrange for information to be sent to you. Once again thanks very much for you support.</p>
    
    </div><!-- End Opener -->
    
    <br class="clear" />
    
    <h2>Types of Sponsorship or Advertising Available</h2>
    
    <p class="bold">Basic Exposure: &pound;35</p>
    
    <p>Includes name on sponsorship wall, name in programme under sponsorship partner, official sponsorship material -  STFC official sponsor plaque,  web site listing, and official recognition from the club in Slough Observer of your support.</p>
    
    <hr />
    
    <p class="bold">Programme Advertising: From &pound;50.00</p>
    
    <p>Full Page, 1/2 page, 1/4 Page and 1/8 Page for the entire Season</p>
    
    <hr />
    
    <p class="bold">Discount Vouchers: &pound;80.00</p>
    
    <p>These are very popular especially with shops, restaurants etc. Your name printed on the back of our 50/50 draw tickets (approx 150 sold per game) where you offer a discount on presentation of ticket (price is for 2 games)</p>
    
    <hr />
    
    <p class="bold">Product Donation</p>
    
    <p>Please donate one or some of your products to our prize fund which we use throughout the year at our Christmas Draw, matchday competitions and community projects.</p>
    
    <hr />
    
    <p class="bold">Perimeter Advertising: &pound;280</p>
    
    <p>Please donate one or some of your products to our prize fund which we use throughout the year at our Christmas Draw, matchday competitions and community projects.</p>
    
    <hr />
    
    <p class="bold">Matchday Sponsor: &pound;150</p>
    
    <p>Sponsor a match, which includes many different promotions and marketing. Package includes, front cover page acknowledgement, advert in programme, p.a. acknowledgements, reserved parking, 4 tickets, pre and post match hospitality, and much much more.</p>
    
    <hr />
    
    <p class="bold">Gold Advertising: &pound;499</p>
    
    <p>Gold Package - includes perimeter advertising, 1/2 pg programme advert for entire season and 2 matchday sponsorships</p>
    
    <hr />
    
    <p class="bold">Silver Advertising: &pound;299</p>
    
    <p>Silver Package - includes 1/2 page programme advert for entire season and 2 matchday sponsorships</p>
    
    <hr />
    
    <p class="bold">Bronze Advertising: &pound;299</p>
    
    <p>Bronze Package - 1/4 programme advert for entire season and 1 matchday sponsorship.</p>
    
    <hr />
    
    <p class="bold">Shirt Sponsor: P.O.A</p>
    
    <p>Your company name on team shirts and replica shirts sold to supporters. Your name all over Slough!</p>
    
    <hr />
    
    <p class="bold">Web Advert: From &pound;50</p>
    
    <p>During the 2010/11 season our official website attracted 34,000 unique visitors from within the UK and abroad, visiting the site over 135,000 times. Our website is an ideal vehicle to promote your brand and also target the overseas market.</p>
    
    <h2>More Information?</h2>
    
    <p>Please fill out the form below for further information</p>
    
     <?php
	// Open form.
	echo form::open();
	?>
	<fieldset>

	<?php if (isset($errors['name'])) echo $errors['name']; ?>
	<input type="text" name="name" title="Name" value="<?php echo (isset($message_data['name'])) ? $message_data['name'] : 'Name'; ?>" class="clearMeFocus" />
	<br />

	<?php if (isset($errors['return_email'])) echo $errors['return_email']; ?>
	<input type="text" name="return_email" title="E-mail" value="<?php echo (isset($message_data['return_email'])) ? $message_data['return_email'] : 'E-mail'; ?>" class="clearMeFocus" />
	<br />

	<?php if (isset($errors['subject'])) echo $errors['subject']; ?>
	<input type="text" name="subject" title="Subject" value="<?php echo (isset($message_data['subject'])) ? $message_data['subject'] : 'Subject'; ?>" class="clearMeFocus" />
	<br />

	<?php if (isset($errors['body'])) echo $errors['body']; ?>
	<textarea name="body" cols="5" rows="5" id="text"><?php echo (isset($message_data['body'])) ? $message_data['body'] : 'Your enquiry'; ?></textarea>

	<br />
	<div style="margin-left: 62px">
	<?php
		// Show the CAPTCHA
		echo $captcha;
		// Show CAPTCHA error, if applicable
		if (isset($errors['captcha'])) echo $errors['captcha'];
	?>
	</div>

	<?php
		// Show CAPTCHA input
		echo form::input('captcha_response');
	?>

	<input type="submit" name="submit" value="Send" />

	</fieldset>
	<?php
    // Close form.
	echo form::close();
	?>
    
</div><!--End Content Area -->