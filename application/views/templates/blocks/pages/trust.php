<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">
            
    <div class="opener trust">

        <h1>Supporters Trust News</h1>
                    
        <p class="bold">Please find the latest news and events from the Slough Town supporters trust.</p>
        
        <p>Please keep an eye on this page as Trust news will be added to this page on a regular basis.</p>
        
        <p>For more information on the trust and how to join, please visit the supporter hut during a home match.</p>
        
    </div><!-- End Opener -->
    
    <br class="clear" />
    
    <?php
		
		$i = 0;
		
		foreach ($trust as $trusts)
		{
			
			//Display amount of stories == to $i
			if ($i == 4)
			{
				break;
			}
			
			echo '<h2>' . $trusts->headline . '</h2>';
			
    		$newDate = date("d-m-Y", strtotime($trusts->publish_date));
			
			echo '<p class="bold">Published: ' . $newDate . '</p>';
			
			$sanitized = htmlspecialchars($trusts->body, ENT_QUOTES);
			
			echo '<p>';
			
			echo str_replace(array("\r\n", "\n"), array("<br />", "<br />"), $sanitized);
			
			echo '</p>';
		
			$i++;
		
		}
	?>

</div><!--End Content Area -->