<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">
            
    <div class="opener">

        <h1>Tickets &amp; Admission</h1>
        
        <p class="bold"></p>
          
    </div><!-- End Opener -->
    
    <br class="clear" />

    <h2>Matchday admission prices</h2>

    <p class="bold">National League South, FA Cup* and FA Trophy*</p>

    <p>Adult (18-64) - £13<br />Concession (65+) - £9<br />Student (16-17, Student) - £5<br />Child (5-15) - £3 (12 year old and under must be accompanied by an Adult)<br />Infant (0-4) – Free</p>

    <p>*For Play-off matches, FA Competitions and high profile fixtures the club reserves the rights to amend ticket categorisation and pricing.</p>

    <p class="bold">Season Tickets</p>

    <p>Adult (18-64 years) - £150 (£180 if purchased after 30 June 2018)<br />
    Concession (65+) - £110 (£130 if purchased after 30 June 2018)<br />
    Student (16-17, Student) - £80 (£95 if purchased after 30 June 2018)<br /> 
    Junior Rebels (5-15) - £30 (£35 if purchased after 30 June 2018)<br />
    Infant (0-4) - Free admission on matchdays</p>

    <p>Season tickets can be purchased from our online <a href="/shop">Club Shop</a>.</p>

    <p class="bold">Seating</p>

    <p>West Stand Unreserved - No charge. Excludes Testimonial, Cup and Playoff matches, for which a charge may be levied for all seating. Seating is managed on a first come, first served basis - the Club cannot guarantee availability. A number of pre-purchased Reserved seats are in position during League fixtures.</p>

    <p>East Stand Unreserved - No charge. Excludes Testimonial, Cup and Playoff matches, for which a charge may be levied for all seating. Seating is managed on a first come, first served basis - the Club cannot guarantee availability.</p>

    <p class="bold">Parking</p>

    <p>There is no charge for parking in Unreserved bays at Arbour Park. Parking is not allocated and is managed on a first come, first served basis.</p>

    <p>Parking for Blue Badge holders is available on a first come, first served basis.</p>

    <h2>Arbour Park</h2>

    <p>Please note the Arbour Park <a href="groundregs">ground regulations</a>.</p>

    <p class="bold">Smoking</p>

    <p>Smoking or vaping is NOT PERMITTED anywhere within the Arbour Park facility.</p>

    <p class="bold">Dogs</p>

    <p>No dogs, except Guide Dogs, are allowed within the Arbour Park facility.</p>

    <p class="bold">No Standing Zones</p>

    <p>A number of no standing zones are marked with yellow hatching within the Arbour Park facility.</p>

    <p class="bold">Media</p>

    <p>The filming, recording or broadcasting of matches at Arbour Park cannot take place without prior consent from Slough Town FC.</p>

    <h2>Disabilities</h2>

    <p>Slough Town offers Carers of people with disabilities a free Carers' pass. The relevant forms can be downloaded <a href="/pdf/STFC-disability_registration_Form.pdf" target="blank">here</a> with T&amp;Cs viewable <a href="/pdf/STFC-disabled_t&cs.pdf" target="blank">here</a>.</p>

</div><!--End Content Area -->
