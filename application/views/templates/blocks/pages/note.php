<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
        	
<div id="content_area">

    <h2>Published: <?php $newDate = date("d-m-Y", strtotime($match_date)); echo $newDate; ?></h2>
    
    <h1><?php echo $headline; ?></h1>
    
    <p>Written by <?php echo $author; ?></p>
    
    <img src="<?php echo $manager_note_picture_thumbnail_url; ?>" alt="Managers Note image" />
    
    <p class="bold blue centre nomargin"><?php echo $caption; ?></p>
    
    <hr />
    
    <?php
	
		if($quote != NULL)
		{
			echo'<div class="quote">';	
			
			echo'<blockquote><p class="nomargin">' . $quote .'</p></blockquote><p class="nomargin">' . $quote_by . '</p>';	
			
			echo'</div><!-- End quote -->';	
		}
		
		$body_count = count($body);
		if ($body_count > 0)
		{
			echo "<p><b>$body[0]</b></p>";
			for ($i = 1; $i < $body_count; $i++)
			{
				echo "<p>$body[$i]</p>";
			}
		}	
			
	?>
    
    <div id="social">
    
        <iframe src="http://www.facebook.com/plugins/like.php?app_id=148739918469452&amp;href=<?php  echo "http://" . $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:50px; height:20px;" allowTransparency="true"></iframe>
        
        <a href="http://twitter.com/share" class="twitter-share-button" data-count="none" data-via="sloughtownfc">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
        
        <g:plusone size="medium" count="false"></g:plusone><script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
    
    </div><!-- End Social -->

</div><!--End Content Area -->