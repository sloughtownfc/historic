<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<script type="application/javascript">
	$(document).ready(function(){
		$('.the_statistics tbody tr:odd td').css('background', '#f1f1f1');
	});
</script>

<div id="content_area">

    <div class="opener">

        <h1>Slough Town Opposition Database</h1>

        <p class="bold">This page is dedicated to providing information on all the opposition Slough Town FC have played against.</p>

        <?php
			echo form::open();
			echo form::input('search');
			echo form::submit('submit', 'Search');
			echo form::close();
		?>

    </div><!-- End Opener -->

    <br class="clear" />
	
    <table class="the_statistics" summary="Matches Played">
		<thead>
			<tr>
				<th style="padding-left:5px;">Team Name</th>
				<th class="centre">Profile</th>
			</tr>
		</thead>
		<tbody>
		<?php
		foreach ($opposition as $oppo)
		{
		?>
		<tr>
			<td bgcolor="#ffffff" style="padding-left:5px;"><?php echo $oppo->name; ?></td>
			<td bgcolor="#ffffff" class="centre"><a href="/opposition/<?php echo $oppo->id; ?>/<?php $link2 = str_replace(" ","-",$oppo->name); echo $link2; ?>">More Information</a></td>
		</tr>
		<?php
		}
		?>
		</tbody>
	</table>

<?php
// Show pagination.
echo $pagination;
?>
</div><!--End Content Area -->