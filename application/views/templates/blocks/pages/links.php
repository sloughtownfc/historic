<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">
            
    <div class="opener">

        <h1>External Club Links</h1>
        
        <p class="bold">Please find below a list of possible links of interest relating to Slough Town FC.</p>
        
        <p>Please click on a link below to go to a site. Slough Town FC is not responsible for the content of external internet sites.</p>

    </div><!-- End Opener -->
    
    <br class="clear" />
    
    <h2>Links</h2>
    
    <p><a class='external_link' href="http://www.evostikleaguesouthern.co.uk/" target="_blank">The Evo-Stik League Southern</a></p>

    <p><a class='external_link' href="http://www.thefa.com" target="_blank">The FA</a></p>

    <p><a class='external_link' href="http://www.berks-bucksfa.com/" target="_blank">The Berks &amp; Bucks FA</a></p>
    
    <p><a class='external_link' href="http://garyhousephotography.co.uk/" target="_blank">Gary House Photography</a></p>
    
    <p><a class='external_link' href="http://sloughtownsoapbox.blogspot.com/" target="_blank">Brighton Rebel Blog</a></p>
    
    <p><a class='external_link' href="http://www.sloughobserver.co.uk/sport/sloughtown/" target="_blank">Slough Observer Sport</a></p>
    
    <p><a class='external_link' href="https://www.sloughexpress.co.uk/section/72/slough-town-fc" target="_blank">Slough Express Sport</a></p>
                
</div><!--End Content Area -->
