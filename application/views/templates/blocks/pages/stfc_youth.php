<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<script type="text/javascript">

function initialize() {
	var latlng = new google.maps.LatLng(51.512332, -0.651204);
	var myOptions = {
		zoom: 14,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControl: true
	};
	var map = new google.maps.Map(document.getElementById("the_map"),myOptions);
	var marker = new google.maps.Marker({
		position: latlng, 
		map: map
	}); 
}

</script>

<div id="content_area">
            
    <div class="opener youth">

        <h1>Slough Town Juniors FC</h1>
        
        <p class="bold">We currently have three junior teams who will be U12, U13 and U15 for the 2017/18 season.</p>

        <p>We are an inclusive club who also provide training only packages for those young people who do not want to commit to regular match play. Our aim is to provide football for boys and girls of all abilities and through the guidance of our dedicated coaches we strive to develop and improve the football skills of all of our young players.</p>

        <p>The Under 12 and 13 teams play in the Thames valley Development League whilst the Under 15's travel to the Wycombe League. All teams train weekly and matches are played on a Sunday.</p>

        <p>We are always keen to welcome new players or volunteers to the club.</p>

        <p>If you require any general information about Slough Town Junior FC please complete the contact form below or contact the relevant Manager directly.</p>


    </div><!-- End Opener -->
    
    <br class="clear" />
    
    <!--h2>News</h2>
    
    <p><a href="other-report-template.php">26/2/10 - Under 9 report</a></p>
        
    <p><a href="other-report-template.php">26/2/10 - Under 12 report</a></p>
    
    <p><a href="other-report-template.php">26/2/10 - Under 16 report</a></p>
    
    <p><a href="other-report-template.php">26/2/10 - Under 9 report</a></p>
        
    <p><a href="other-report-template.php">26/2/10 - Under 12 report</a></p>
    
    <p><a href="other-report-template.php">26/2/10 - Under 16 report</a></p -->
    
    <h2>Club Information</h2>
    
    <p class="nomargin"><span class="bold">Chair:</span> Kay Lathey</p>
    <p class="nomargin"><span class="bold">Secretary:</span> Kelly Turner</p>
    <p class="nomargin"><span class="bold">Treasurer:</span> Mike Lightfoot</p>
    <p><span class="bold">Welfare Officer:</span> Mark Betts</p>

   	<h2> Team Contacts </h2>
    
    <p class="nomargin"><span class="bold">U12 Manager:</span> Adam Foulser - 07792 126124</p>
    <p class="nomargin"><span class="bold">U13 Manager:</span> Dave Tormey - 07748 014193</p>
    <p><span class="bold">U15 Manager:</span> Dave King	- 07749 245950</p>
    
    <h2>Home Games</h2>
    
    <p>All home games are played at Mercian Way Recreation Ground, Slough.</p>
        
    <p><span class="bold">Address:</span> Mercian Way Recreation Ground, Mercia Way Cippenham, SL1 5LX</p>
            
    <div class="map_container">
    	<div id="the_map"></div>
    </div>

    <br class="clear" />
    
    <h2>Contact Us</h2>
    
    <p>If you have a question or query please contact us using the form below.</p>
    
    <p>Where your enquiry relates to joining one of our teams, please contact the relevant manager which can be found on the right hand menu.</p>
   
     <?php
    // Open form.
	echo form::open(NULL, array('class'=>'validation'));
	?>
	    <fieldset>

		<?php if (isset($errors['name'])) echo $errors['name']; ?>
		<input type="text" name="name" title="Name" value="<?php echo (isset($message_data['name'])) ? $message_data['name'] : 'Name'; ?>" class="validate[required] clearMeFocus" />
		<br />

		<?php if (isset($errors['return_email'])) echo $errors['return_email']; ?>
		<input type="text" name="return_email" title="E-mail" value="<?php echo (isset($message_data['return_email'])) ? $message_data['return_email'] : 'E-mail'; ?>" class="validate[required,custom[email]] clearMeFocus" />
		<br />

		<?php if (isset($errors['subject'])) echo $errors['subject']; ?>
		<input type="text" name="subject" title="Subject" value="<?php echo (isset($message_data['subject'])) ? $message_data['subject'] : 'Subject'; ?>" class="validate[required] clearMeFocus" />
		<br />

		<?php if (isset($errors['body'])) echo $errors['body']; ?>
		<textarea name="body" cols="5" rows="5" id="text" class="validate[required]"><?php echo (isset($message_data['body'])) ? $message_data['body'] : 'Your enquiry'; ?></textarea>

		<br />
		<div style="margin-left: 62px">
		<?php
			// Show the CAPTCHA
			echo $captcha;
			// Show CAPTCHA error, if applicable
			if (isset($errors['captcha'])) echo $errors['captcha'];
		?>
        	<br class="clear"/>
        
        	<p class="nomargin">Confirm the letters and numbers above.</p>
        
		</div>

		<?php
			// Show CAPTCHA input
			echo form::input('captcha_response');
		?>

		<input type="submit" name="submit" value="Send" />

	    </fieldset>
	 <?php
    // Close form.
	echo form::close();
	?>

               
</div><!--End Content Area -->