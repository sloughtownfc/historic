<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

    <div class="opener">

        <h1>Arbour Park Ground Regulations</h1>

        <p>Entry to the Ground is expressly subject to acceptance by the visitor of these Ground Regulations and the rules and regulations of The Football Association. The Ground Regulations incorporate the Club's Code of Conduct. Entry to the Ground shall constitute acceptance of the Ground Regulations. </p>

    </div><!-- End Opener -->

    <br class="clear" />

    <p>"Ground" means Arbour Park, Stoke Road, Slough, Berks, SL2 5AY</br>"Club" means Slough Town Football Club.</br>"Match" means any association football match taking place at the Ground.</p>

    <p>1.  Permission to enter or to remain within the Ground (notwithstanding possession of any ticket) is at the absolute discretion of the Club, any police officer or authorised steward. On no account will admission be granted to a person who is the subject of a current Banning Order.</p>

    <p>2.  No child under the age of 12 will be permitted admission unless they are accompanied by an adult.</p>

    <p>3.  The Club excludes to the maximum extent permitted by law any liability for loss, injury or damage to persons/property in or around the Ground.</p>

    <p>4.  No guarantees can be given by the Club that a Match will take place at a particular time or on a particular date and the Club reserves the right to reschedule the Match without notice and without any liability whatsoever.</p>

    <p>5.  In the event of the postponement or abandonment of the Match and at the exclusive discretion of the Club, tickets issued at the point of entry to the Ground will be valid for any date the postponed or abandoned Match is replayed. The Club will have no liability whatsoever, including (but not limited to) any indirect or consequential loss or damage, such as (but not limited to) loss of enjoyment or travel costs.</p>

    <p>6.  All persons seeking entrance to the Ground acknowledge the Club's right to search any person entering the Ground and to refuse entry to or eject from the Ground any person refusing to submit to such a search.</p>

    <p>7.  The following articles must not be brought within the Ground - knives, fireworks, smoke canisters, air-horns, flares, weapons, dangerous or hazardous items, laser devices, bottles, glass vessels, cans, poles and any article that might be used as a weapon and/or compromise public safety. Any person in possession of such items will be refused entry to the Ground. The decision on what items are subject to this policy rests with the Club.</p>

    <p>8.  Further, you may not bring into the Ground any sponsorship, promotional or marketing materials save in respect of official club merchandise and/or other football related clothing worn in good faith nor may you offer (either free or for sale by any person) any goods (including literature) of any nature without the express written approval of the Management.</p>

    <p>9.  The use of threatening behaviour, foul or abusive language is strictly forbidden and may result in arrest and/or ejection from the Ground. The Club may impose a ban for one or more Matches.</p>

    <p>10. Racial, homophobic or discriminatory abuse, chanting or harassment is strictly forbidden and may result in arrest and/or ejection from the Ground. The Club may impose a ban for one or more Matches.</p>

    <p>11. The following acts are offences under the Football (Offences) Act 1991, as amended:<br/>
    11.1    The throwing of any object within the Ground without lawful authority or excuse. <br/>
    11.2 The chanting of anything of an indecent or racialist nature.<br/>
    11.3 The entry onto the playing area or any adjacent area to which spectators are not generally admitted without lawful authority or excuse. Conviction may result in a Banning Order being made.</p> 
    <p>12. Where segregation exists and/or is applicable all persons entering the Ground may only occupy the part of the ground and/or seat allocated to them by their ticket, or instructions given by a Club Official, and must not move from any one part of the Ground to another without the express permission or instruction of any steward, officer of the Club and/or any police officer.</p> 

    <p>13. Nobody may stand in any seating area or other designated no-standing area whilst play is in progress. Persistent standing in seated areas or designated no-standing areas whilst play is in progress is strictly forbidden and may result in ejection from the Ground.</p> 

    <p>14. The obstruction of gangways, access ways, exits and entrances, stairways and like places is strictly forbidden. Nobody entering the Ground shall be permitted to climb any structures within the Ground.</p> 

    <p>15. The ground is designated a no-smoking ground and consequently smoking is not permitted in any closed building, room or space within the Ground.</p> 

    <p>16. Attempting to enter the Ground or being inside the Ground whilst drunk will result in refusal of admission and ejection from the ground respectively.</p> 

    <p>17. No alcohol may be brought into the Ground and the purchase and consumption of alcohol is restricted to designated areas only.</p> 

    <p>18. Being in possession of bottle, glass or other portable container which could cause damage or personal injury in a non-designated area of the Ground from which the event can be viewed may be confiscated and returned at the end of the match.</p> 

    <p>19. Any individual who has entered any part of the Ground designated for the use of any group of supporters to which he does not belong may be ejected from the Ground either for the purposes of his own safety or for any other reason.</p> 

    <p>20. No person may bring into the Ground or use within the Ground any equipment, which is capable of recording or transmitting (by digital or other means) any audio, visual or audio-visual material or any information or data in relation to the Match or the Ground without the express written permission of the Club. Copyright in any unauthorised recording or transmission is assigned (by way of present assignment of future copyright pursuant to section 91 of the Copyright, Designs and Patents Act 1988) to the Club.</p> 

    <p>21. No goods (including literature) of any nature may be offered either free or for sale by any person within the Ground without the express written permission of the Club.</p> 

    <p>22. No animals, other than guide dogs, shall be brought into the ground.</p> 

    <p>23. Tickets are not transferable and may not be offered for sale without the prior written permission of the Club. Any tickets offered for sale may be confiscated by any steward, officer of the Club or any police officer.</p> 
        
    <p>24. For those with pitch side access - seats with vertical legs or those with large footprints (cushions, boxes, etc) are NOT allowed. Seats with horizontal bars as their base are allowed. Camera cases are allowed on the pitch, but should not be used as seats. The Club will have no liability whatsoever, including (but not limited to) any indirect or consequential loss or damage to items brought onto the pitch.</p>

    <p>25. Those with pitch side access must adhire to the appropriate football regulations or will not be allowed on the pitch. Astro trainers, AG boots and normal boots with moulds are allowed whilst normal shoes, trainers, boots with metal studs and boots with blades are not.</p>
</div><!--End Content Area -->