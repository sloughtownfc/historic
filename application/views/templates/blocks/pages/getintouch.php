<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

	<div class="opener">

		<h1>Contact Us</h1>

		<p class="bold">Thank you for getting in touch with us at Slough Town Football Club.</p>

		<p>Please fill in the form below to get in touch with a club official. Please note that some of the details listed below are required fields and you will not be able to preceed without filling them in.</p>
        
        <p>For information related to Slough Town Juniors teams please <a href="stfc-youth">click here</a>. This includes players in categories aged Under 8 to Under 16.</p>

	</div><!-- End Opener -->

    <br class="clear" />

	<h2>Contact Slough Town FC</h2>

    <?php
    // Open form.
	echo form::open(NULL, array('class'=>'validation'));
	?>
	    <fieldset>

		<?php if (isset($errors['name'])) echo $errors['name']; ?>
		<input type="text" name="name" title="Name" value="<?php echo (isset($message_data['name'])) ? $message_data['name'] : 'Name'; ?>" class="validate[required] clearMeFocus" />
		<br />

		<?php if (isset($errors['return_email'])) echo $errors['return_email']; ?>
		<input type="text" name="return_email" title="E-mail" value="<?php echo (isset($message_data['return_email'])) ? $message_data['return_email'] : 'E-mail'; ?>" class="validate[required,custom[email]] clearMeFocus" />
		<br />

		<?php if (isset($errors['contact_id'])) echo $errors['contact_id']; ?>
		<select name="contact_id" id="contact_id" style="margin-left:62px;" class="validate[required]" >
		    <option value="" <?php if (!isset($message_data['contact_id'])) echo 'selected="selected"'; ?>>Message For</option>
		    <?php
			    foreach ($contacts as $id => $name)
			    {
			    	// Print all contacts
			    	if ($message_data['contact_id'] == $id)
			    	{
			    		echo '<option value="' . $id . '" selected="selected">' . $name . '</option>';
			    	}
			    	else
			    	{
			    		echo '<option value="' . $id . '">' . $name . '</option>';
			    	}

			    }
		    ?>
		</select>

		<br />

		<?php if (isset($errors['subject'])) echo $errors['subject']; ?>
		<input type="text" name="subject" title="Subject" value="<?php echo (isset($message_data['subject'])) ? $message_data['subject'] : 'Subject'; ?>"  class="validate[required] clearMeFocus" />
		<br />

		<?php if (isset($errors['body'])) echo $errors['body']; ?>
		<textarea name="body" cols="5" rows="5" id="text"><?php echo (isset($message_data['body'])) ? $message_data['body'] : 'Your enquiry'; ?></textarea>

		<br />
		<div style="margin-left: 62px">
		<?php
			// Show the CAPTCHA
			echo $captcha;
			// Show CAPTCHA error, if applicable
			if (isset($errors['captcha'])) echo $errors['captcha'];
		?>
		</div>

		<?php
			// Show CAPTCHA input
			echo form::input('captcha_response');
		?>

		<input type="submit" name="submit" value="Send" />

	    </fieldset>
	 <?php
    // Close form.
	echo form::close();
	?>

</div><!--End Content Area -->