<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

    <div class="opener academy">

        <h1>STFC Academy 2017/18</h1>

        <p class="bold">This page is currently being updated ahead of the 2017/18 season. Return soon for more information.</p>

        <!--p class="bold">Slough Town FC in partnership with SCL are trialing for places for the 2016/17 season.</p>

		<p>The Football Academy is a 2-year, full-time Education and Football Development programme delivered in partnership with Professional and Non-League Football Clubs.</p>
        
        <p>Open to talented footballers aged between 16–18 years, the Academy Programme is a must for any football enthusiast. A playing trial will be required as places will be offered based on playing ability. Places at the Academy will be offered after each trial until the availability within the current Academy squad is filled.</p>
        
        <a href="http://wearescl.co.uk/slough-town-fc" target="_blank" class="academy_register_now"></a-->

    </div><!-- End Opener -->

    <br class="clear" />
    
    <!--img src="images/academy/academy1.jpg" width="629" height="175" alt="STFC Academy" />

    <br class="clear" />
    
    <h2>The STFC Academy</h2>

    <p>Here at the academy we focus on football player development by integrating our players into Academy and Allied Counties leagues with further opportunities to join the first team. In addition we are linked to a number of local teams to give playing opportunities to all levels of ability.</p>

    <p>We also place great emphasis on the education of the young players, alongside the qualifications provided by SCL; opportunities are available to gain coaching qualifications, first aid and functional skills. Importantly we provide links to coaching work experience within schools, camps and mentoring young people in the community.</p>

    <p>Preparing our students to take a step into the working world is very important to us, from gaining these experiences 3 of our current students have applied to higher education, 4 have been offered scholarships to study and practice coaching in the USA and 12 of the students have gained meaningful employment within the industry. We also provide a coaching apprentice scheme where upon completion of the programme students are invited to apply for positions within the club to work as coaches. This year we have 16 applications for the role.</p>

    <br class="clear" />

    <h2>Location</h2>
    
    <p>The football and educational element of the academy will be delivered from the clubs excellent facilities at Farnham Park Playing Fields, Beaconsfield Road, Farnham Royal, Slough SL2 3BP. This is also home to the clubs youth set up, <a href="http://www.sloughtownfc.net/stfc-youth">Slough Town Youth FC</a>.</p>
    
    <br class="clear" /> 

    <h2>Academy Education</h2>
    
    <p>Over the 2-year programme Academy players will study a variety of National Recognised qualifications within the subject of Sport, Coaching &amp; Fitness. *The educational programme will include qualifications such as BTEC, NVQ and Diploma’s, delivered at Level 2 and 3 (A-Level equivalence). Successful completion of the educational programme will enable players to follow pathways into higher education at selected Universities and employment opportunities within the Sport and leisure industry, including coaching placements and playing scholarships in America.</p>
    
    <p>*Qualifications and achievement levels offered may vary depending on each Academy preference.</p>

    <img src="images/academy/academy2.jpg" width="629" height="175" alt="STFC Academy" />
        
    <br class="clear" /> 

    <h2>A messsage from the Management</h2>

    <p>At the academy we have an ethos  ‘work hard, play football’  This is what we try to instil into all our players of all abilities. Our emphasis is on progressing players ability, helping them to achieve thier goals wether in a football or educational environment.</p> 

    <p>Both the academy manager and joint first team managers work very closely together to integrate youth team players into the frist team squad. This has been shown this season by 3 of our youth players making appearences for the first team, Seb Costello, Matty  Bonnet and Arron Collymore have all gone on to train and play some part with the frist team this year. This is a great achievemnt for the academy and just shows the wealth of talent available in the local area.</p>  

    <p>Further to this we want to create opportunities for those that love the game as much as we do. We provide work experience roles thorugh our coaching programme which benefits the next generation of young players in and around Slough, further to this we have opportunities in other aspects of football club management, including match analysis, photography, media and communications and kit management/sponsorship. </p>

    <p>Our coaches have gone on to work at local primary schools this provides them with real life coaching experince and gives the young children the chance to learn from the best. </p>

    <p>We also have great links with other clubs including professional clubs, Watford, Reading, Crystal Palace, Leeds United, QPR.</p>

    <br class="clear" />

    <h2>A day at the Academy</h2>

    <p>09:30: Arrive onsite with a brief meeting about the day’s activities, we then get ready to train – Tom J Year 2</p>

    <p>10:00: After a 10 minute warm up we start training, today we are working on attacking movement, we’re all assigned roles and each one is described to us, we then run through possession scenarios and are taught about roles either directly or indirectly involved with play and how each can be used to our advantage. – Harry L Year 1</p>

    <p>The first team goal keeper <a href="http://www.sloughtownfc.net/profile/1984/Jake-Somerville">Jake Somerville</a> takes the 5 academy GK’s for training today, we worked on positioning and reactions - Nathan M Year 1</p>

    <p>11:30: I checked in with the Sports Therapist today, I’ve struggling with a calf injury. After some testing we did some rehabilitation exercises and I was shown the things I should do at home, I was able to join in with the others who were also doing rehab where our progress is guided throughout - Dan B Year 1</p>

    <p>12:00: Lunchtime. We’re allowed to relax and enjoy lunchtime, we can go to the local shops, listen to music, watch sky sports, play table tennis, pool and football – Yasin Year 2</p>

    <p>1:00: Back out for training, we doing an hour of strength & conditioning with the sports scientist, it’s really intense but it’s benefited a lot of us especially at the end of games - Rory Year 2</p>

    <p>2:00: Mark, the academy manager took this afternoons session, we worked on shooting and set pieces, which added to our attacking training this morning - Warren B Year 2</p>

    <p>3:00: Training was progressed into a match this afternoon, where the players are able to be more expressive and it’s great to see some of the drills from this morning being utilised in game situations - Mark Academy manager</p>

    <p>3:30: we have a 10-15 minute warm down after every session, it’s important for our recovery and to prevent injuries - Ricky Year 1</p>

    <p>A brief meeting takes place, where tomorrow’s activities are set out. We’re training and in the classroom tomorrow, so we’ve got to remember our folders. Those that are coaching in schools are then taken to their respective schools after changing into their STFCA tracksuits.</p>

    <br class="clear" />

    <h2>Player Progression</h2>

    <p>This year 3 of our players have been invited to train with the first team squad:</p>

    <p><a href="http://www.sloughtownfc.net/profile/2009/Matty-Bonnett">Matty Bonnet</a> 16 – made his debut for the first team at the age of 15, during these games Matty put out some fearless displays against players often twice his age. Over the season he has established himself as reserve keeper to Jake Somerville and is a much liked member of the squad. Matty now trains with the first team on a regular basis and has made appearances on loan at Bagshot Lea to gain men’s football experience. Both Jon &amp; Neil (first team managers) are very excited about Matty’s future in the Slough team.</p>

    <p><a href="http://www.sloughtownfc.net/profile/2004/Sebastien-Castello">Seb Castello</a>, 18 – was invited by the first team management to train with the first team after watching him play for the Allied Counties team in September 2013, since then Seb has gone on to make regular appearances for the first team, he has also continued to captain the Allied Counties team, captained the county team and made appearances for Bagshot Lea to gain men’s football experience.</p>

    <p>Arron Collymore, 18 – has trained with the first team over the course of the season and has featured on the bench with the first team. Arron has also played on loan whilst continually influencing games at Allied County level.</p>

    <p>This year has also seen a number of academy recruits make the step up to Allied county football level: Tom Johnson – 18, Central defence, Joseph Moore – 16, Centre midfield, Jason Lewis – 17 Attacking midfield, Ricky Pritchard - 16, Wide midfield, Daniel Buggy – 16 Left back, Oscar Betts – 15, Attacking midfield </p>
    
    <br class="clear" />

    <h2>Student Testimonials</h2>

    <p style="font-style:italic;">"I’ve been playing and studying here at STFCA for 2 years now, the skills I have learnt are vast. Above all I am most proud of how my football ability has developed; I went from being an average player to part of the Allied Counties team. Training everyday was hard work but thoroughly enjoyable and I’m now seeing the rewards of being taught how to understand as well as play the game. Over the two years, I’ve made many lifelong friends and learnt many life skills, I now feel more prepared to go out and work. I hope to work in football, primarily coaching, whilst I see how my non league carer develops. This has been a great programme and the management and staff are brilliant. ‘You will come in with a smile on your face and leave with a bigger one!'" <span class="bold" style="font-style:normal;">Tom Johnson, 18 – Year 2, Allied Counties, Academy League Player &amp; Captain, Central Defender. </span></p>

    <p style="font-style:italic;">"As I look back on my first year on the Academy programme, I realise how enriching and entertaining this year has been. STFCA and specifically the coaches have helped me develop both as a person and player, giving me experiences and opportunities at every corner. In my first year I have made many new friends and became part of a great team. The things I enjoy most about the Academy are travelling to new places, seeing England and playing in Semi professional stadiums. I also enjoyed breaking in to Slough u18’s Allied counties squad and getting a taste the high level of football that is on offer, it has given me something to strive for going into my 2nd year. Overall the Academy has made positive changes to my life, while introducing me to the world of football and working life, I feel I am part of something at STFCA. Thank you." <span class="bold" style="font-style:normal;">Joseph Moore, 16 – Year 1, Academy league, Allied Counties player, Centre Midfielder.</span> </p>

    <br class="clear" />
    
    <h2 id="register">Register for Trials</h2>
    
    <p>To register for trials please click <a href="http://wearescl.co.uk/slough-town-fc" target="_blank">here</a>.</p-->

    
</div><!--End Content Area -->

<!--div id="right_nav">
	
    <h3>Academy Football</h3>
    
    <ul class="academy_list">
    	<li><span>Daily professional coaching by UEFA 'A' and 'B' Licence Coaches</span></li>
        <li><span>Guidance on football fitness, nutritional advice and regular appraisals to monitor each player's development both on and off the pitch</span></li>
    	<li><span>Emphasis on progressing Academy players into the First Team squad</span></li>
    </ul>
    
    <h3>Academy Education</h3> 
    
    <ul class="academy_list">
    	<li><span>2-year programme</span></li>
        <li><span>NVQ &amp; Diploma's in Sport</span></li>
        <li><span>Coaching Qualifications</span></li>
    </ul>      

    <h3>Other Activities</h3>
    
    <ul class="academy_list">
        <li><span>Coaching opportunities U7-U16s</span></li>
        <li><span>Leisure activities</span></li>
        <li><span>Coaching in after school clubs</span></li>
        <li><span>Summer camp coaches</span></li>
        <li><span>Days out and team building</span></li>
        <li><span>Working in the community</span></li>
        <li><span>Mentoring youngsters in local schools</span></li>
    </ul>   
    
    <h3>Costs</h3>
    
    <ul class="academy_list">
    	<li><span>Football and Education elements are fully funded</span></li>
        <li><span>Subsidised training kit</span></li>
    </ul>  

    <br class="clear" />

</div--><!-- End Right Nav -->