<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

    <div class="opener">

        <h1>All-time Records</h1>

        <p class="bold">Listed below are the club's top 20 all-time appearance-makers and goalscorers.</p>


    </div><!-- End Opener -->

    <br class="clear" />

    <h2>Top 20 all-time appearances</h2>
    
    <table class="the_statistics" summary="Player Statistics">
	<thead>
		<tr>
			<th class="centre" style="width:20%;">Position</th>
            <th style="width:40%;">Name</th>
			<th class="centre" style="width:40%;">Appearances (Goals)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<?php			
			
			$i = 1;
			
			foreach ($apps as $player_id)
			{
				if($i =="1") { $bg = '#efaf12'; }  else { $bg = ($bg=='#f1f1f1' ? '#ffffff' : '#f1f1f1'); }
			?>
            <tr>
            	<td bgcolor="<?php echo $bg; ?>" class="centre"><?php echo $i; ?></td>
				<td bgcolor="<?php echo $bg; ?>"><a href="/profile/<?php echo $player_id->player_id; ?>/<?php echo $player_id->first_name; ?>-<?php echo $player_id->last_name; ?>"><?php echo $player_id->first_name; ?> <?php echo $player_id->last_name; ?></a></td>
				<td bgcolor="<?php echo $bg; ?>" class="centre"><?php echo $player_id->appearances; ?>&nbsp;(<?php echo $player_id->goals; ?>)</td>
            </tr>
			<?php
			$i++;
			}
			?>
		</tr>
	</tbody>
	</table>
    
    
    <h2>Top 20 all-time goalscorers</h2>
	
    <table class="the_statistics" summary="Player Statistics">
	<thead>
		<tr>
			<th class="centre" style="width:20%;">Position</th>
            <th style="width:40%;">Name</th>
			<th class="centre" style="width:40%;">Goals (Appearances)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<?php
			$j = 1;
			foreach ($goals as $player_id)
			{
					if($j =="1") { $bg = '#efaf12'; }  else { $bg = ($bg=='#f1f1f1' ? '#ffffff' : '#f1f1f1'); }
			?>
			<tr>
            	<td bgcolor="<?php echo $bg; ?>" class="centre"><?php echo $j; ?></td>
				<td bgcolor="<?php echo $bg; ?>" ><a href="/profile/<?php echo $player_id->player_id; ?>/<?php echo $player_id->first_name; ?>-<?php echo $player_id->last_name; ?>"><?php echo $player_id->first_name; ?> <?php echo $player_id->last_name; ?></a></td>
				<td bgcolor="<?php echo $bg; ?>" class="centre"><?php echo $player_id->goals; ?>&nbsp;(<?php echo $player_id->appearances; ?>)</td>
            </tr>
			<?php
			$j++;
			}
			?>
		</tr>
	</tbody>
	</table>

</div><!--End Content Area -->