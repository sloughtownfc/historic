<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">
	
    <?php
    $newDate = date("l, F jS Y", strtotime($match_date));
	?>
            
    <h2>Match date: <?php echo $newDate; ?></h2>
    
    <?php
		// Set home/away teams
		if ($is_home == 1)
		{
			echo '<h1 class="centre">Slough Town ' . $home_score . '-' . $away_score . ' ' . $opposition.'</h1>';
		}
		else
		{
			echo '<h1 class="centre">'. $opposition .' ' . $home_score . '-' . $away_score . ' Slough Town</h1>';	
		}
    ?>
    <p>Filmed by <?php echo $author; ?></p>
    
 	<iframe style="margin-bottom:10px;" id="video-player" type="text/html" width="640" height="340" src="http://www.youtube.com/embed/<?php echo $video_url; ?>" allowfullscreen frameborder="0"></iframe>
    
    <?php 
		$body_count = count($video_body);
	
		if ($body_count > 0)
		{
			echo "<p><b>$video_body[0]</b></p>";
			for ($i = 1; $i < $body_count; $i++)
			{
				echo "<p>$video_body[$i]</p>";
			}
		}
	?>
    
    <p><a href="/reports/<?php echo $id; ?>">Read Match Report</a></p>
    
    <?php
    	if($report_flickr_url != NULL)
		{
			echo'<p><a class="external_link" href="' . $report_flickr_url . ' " onclick="window.open(this.href); return false;">View Gary House Photography</a></p>';	
		}
    ?>
    
    <div id="social">
    
        <iframe src="http://www.facebook.com/plugins/like.php?app_id=148739918469452&amp;href=<?php  echo "http://" . $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>&amp;send=false&amp;layout=button_count&amp;width=90&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:50px; height:20px;" allowTransparency="true"></iframe>
        
        <a href="http://twitter.com/share" class="twitter-share-button" data-count="none" data-via="sloughtownfc">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
        
        <g:plusone size="medium" count="false"></g:plusone><script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
    
    </div><!-- End Social -->

</div><!--End Content Area -->