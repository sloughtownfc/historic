<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

    <div class="opener">

        <h1>Slough Town FC and Social Media Guidelines</h1>

        <p class="bold">Social media offers the opportunity for people with common interests to gather online enabling views and information to be shared and discussed. These views and thoughts are now accessible and can be read by anyone. Slough Town FC (STFC) would like to emphasise that it is important that the reputation of the club, its affiliated associations and sport in general are not tarnished by anyone using social media tools inappropriately; particularly in relation to any content that may reference STFC.</p>

    </div><!-- End Opener -->

    <br class="clear" />
    
    <p>When someone clearly identifies their association with STFC they are expected to behave and express themselves appropriately in the ways that are consistent with the clubs values.</p>

    <p>Below is an extract from the FA Guidance for Participants document 2012-13.  Whilst this lays out the FA position on Social Network sites, Slough Town FC would further remind all players and officials connected to the Club that it is their responsibility to prevent inappropriate posts which others may find abusive, insulting, libellous, threatening, defamatory or harassing in any way. If in doubt don’t post or check with the club.</p>
    
    <p>Whilst we cannot prevent individuals’ use of these sites, the Club advises that wherever possible disciplinary action will be taken against individuals who disregard these guidelines.</p>
    
    <h2>Social Networking Sites (FA Guidance document)</h2>
    
    <p>Social networking sites include, but are not limited to, Twitter, Facebook and online blogs.<p>
    
    <p>Participants are reminded that comments made on such internet sites may be considered public comment and that further to FA Rule E3 any comments which are improper, bring the game into disrepute or are threatening, abusive, indecent or insulting may lead to disciplinary action. In addition, if those comments include a reference to any one or more of a person’s ethnic origin, colour, race, nationality, faith, gender, sexual orientation or disability, a Regulatory Commission shall consider the imposition of an increased sanction in accordance with FA Rule E3(2).</p>

    <p>Comments made which are personal in nature or could be construed as offensive, use foul language or contain direct or indirect threats may be considered improper.</p>
    <p>Participants are advised that postings on social networking sites, which they believe are only visible to a limited number Social networking sites include, but are not limited to, Twitter, Facebook and online of selected people, may still end up in the public domain and consequently care should be taken with the content of such postings. Participants are required to act in the best interest of the game at all times and should be particularly aware of this when using social networking websites.</p>
    <p>In addition, Participants are reminded that content posted on social networking websites could also lead to civil proceedings being brought by affected parties.</p>
	
</div><!--End Content Area -->