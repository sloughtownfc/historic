<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<script type="text/javascript">
	var directionDisplay;
	var directionsService = new google.maps.DirectionsService();
	function initialize() {
		var latlng = new google.maps.LatLng(51.519649, -0.589738);
		directionsDisplay = new google.maps.DirectionsRenderer();
		var myOptions = {
			zoom: 14,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: true
		};
		var map = new google.maps.Map(document.getElementById("the_map"),myOptions);
		directionsDisplay.setMap(map);
		directionsDisplay.setPanel(document.getElementById("directionsPanel"));
		var marker = new google.maps.Marker({
			position: latlng, 
			map: map
		}); 
	}
	function calcRoute() {
		var start = document.getElementById("routeStart").value;
		var end = "51.519649, -0.589738";
		var request = {
			origin:start,
			destination:end,
			travelMode: google.maps.DirectionsTravelMode.DRIVING
		};
		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			} else {
				if (status == 'ZERO_RESULTS') {
					alert('No route could be found between the origin and destination.');
				} else if (status == 'UNKNOWN_ERROR') {
					alert('A directions request could not be processed due to a server error. The request may succeed if you try again.');
				} else if (status == 'REQUEST_DENIED') {
					alert('This webpage is not allowed to use the directions service.');
				} else if (status == 'OVER_QUERY_LIMIT') {
					alert('The webpage has gone over the requests limit in too short a period of time.');
				} else if (status == 'NOT_FOUND') {
					alert('At least one of the origin, destination, or waypoints could not be geocoded.');
				} else if (status == 'INVALID_REQUEST') {
					alert('The DirectionsRequest provided was invalid.');					
				} else {
					alert("There was an unknown error in your request. Requeststatus: \n\n"+status);
				}
			}
		});
	}
</script>
<div id="content_area">
            
    <h1>Slough Town FC</h1>
    
    <img src="/images/main/badge.jpg" alt="Club Badge" class="right" style="border:none;" />
    
    <p>Arbour Park,<br />Stoke Road,<br />Slough,<br />Berkshire,<br />SL2 5AY</p>
    
    <p>07792 126124</p>

    <p>Please note the Arbour Park <a href="groundregs">ground regulations</a>.</p>
	
    <br class="clear" />
    
    <h2>Directions</h2>
    
	<p class="bold">By Road</p> 
	<p>From the M4: Exit at Junction 5 (signposted Colnbrook/Datchet/Langley) and head west on London Road (A4). Pass the Sainsbury’s Superstore on your right hand side and drive straight over the roundabout, continuing on London Road. Pass the Tesco Slough Extra store and then the turning to Slough Rail station on your right and at the next junction, turn right onto Stoke Road (B416). Continue along Stoke Road for 0.7 miles, going straight ahead at the crossroads with Shaggy Calf Lane and Elliman Avenue (following the signpost to Slough Cemetery & Crematorium, Wexham Park Hospital and Stoke Poges). The entrance to Arbour Park is a few hundred yards further on the right hand side.</p>
	<p>From the M25 (north): Exit at Junction 16 and join the M40 to Uxbridge/London (West). At Junction 1, exit and follow signs to Slough/A412/Uxbridge/A4020. At the roundabout, exit onto Denham Road (A412). Continue over two roundabouts and at the ‘Five Points’ roundabout (with the Crooked Billet pub/restaurant), take the third exit and continue on Uxbridge Road (A412). Continue at the first set of lights, and at the second turn right onto Church Lane. Continue over two roundabouts, passing The South Buckinghamshire Golf Club and then Slough Cemetery & Crematorium on your right hand side. Continue over one more mini-roundabout and the entrance to Arbour Park will be on your left hand side.</p>
	<p>FFrom the M25 (south): Exit at Junction 15 (signposted Reading/Slough) onto the M4. Exit the M4 at Junction 5 (signposted Colnbrook/Datchet/Langley). Head west on London Road (A4). Pass the Sainsbury’s Superstore on your right hand side and drive straight over the roundabout, continuing on London Road. Pass the Tesco Slough Extra store and then the turning to Slough Rail station on your right and at the next junction, turn right onto Stoke Road (B416). Continue along Stoke Road for 0.7 miles, going straight ahead at the crossroads with Shaggy Calf Lane and Elliman Avenue (following the signpost to Slough Cemetery & Crematorium, Wexham Park Hospital and Stoke Poges). The entrance to Arbour Park is a few hundred yards further on the right hand side.</p>
	<p>From the M3: Exit at Junction 2 onto the M25 (signposted M4/M1/Heathrow Airport). Exit at Junction 15 (signposted Reading/Slough) onto the M4. Exit the M4 at Junction 5 (signposted Colnbrook/Datchet/Langley). Head west on London Road (A4). Pass the Sainsbury’s Superstore on your right hand side and drive straight over the roundabout, continuing on London Road. Pass the Tesco Slough Extra store and then the turning to Slough Rail station on your right and at the next junction, turn right onto Stoke Road (B416). Continue along Stoke Road for 0.7 miles, going straight ahead at the crossroads with Shaggy Calf Lane and Elliman Avenue (following the signpost to Slough Cemetery & Crematorium, Wexham Park Hospital and Stoke Poges). The entrance to Arbour Park is a few hundred yards further on the right hand side.</p>
	<p>From the M40: Exit at Junction 2 (Beaconsfield) and travel on the A355 towards Slough. Take a left turn onto Parish Lane and follow it to the end, where you will turn right onto Windsor Road (B416). At the next roundabout, take the second exit. You will pass The South Buckinghamshire Golf Club and then Slough Cemetery & Crematorium on your right hand side. Continue over one more mini-roundabout and the entrance to Arbour Park will be on your left hand side.</p>
	<p>The postcode to use for sat navs is SL2 5AY.</p>

	<h2>Parking</h2>

	<p>There are a number of options for supporters travelling to Arbour Park by car.</p>

	<p>This season we are delighted to be able to offer our season ticket holders the chance to reserve parking at the newly completed Arbour Park Community Facility, meaning you can get to the ground at your leisure and be guaranteed a parking space.  We are also pleased to confirm that the partnership with neighbouring St Joseph’s Catholic High School will continue, and that parking spaces will be available there at reasonable rates.</p>

	<p class="bold">Arbour Park Reserved</p>

	<p>Your chance to reserve a parking space at Arbour Park for the season, for just £50. Includes home pre-season friendlies, and excludes Testimonial, Cup and Playoff matches. Cost is applicable for Blue Badge holders and non-Blue Badge holders. Parking is not pre-allocated, but there is a designated reserved parking area close to the ground with a parking space guaranteed for Reserved Parking permit holders. <b>Parking permits are limited in number and will only be sold together with season tickets online, via the <a href="/shop" target="_blank">online Club Shop</a></b>. Parking permits are non-transferrable.</p>

	<p class="bold">Arbour Park Unreserved</p>

	<p>There will be no charge for parking in Unreserved bays at Arbour Park. Unreserved parking is a limited number of spaces. Parking is not allocated and is managed on a first come, first served basis, including the limited number of Blue Badge spaces. This excludes Testimonial, Cup and Playoff matches, for which a charge may be levied for all parking spaces.</p>

	<p class="bold">St Joseph’s School</p>

	<p>Secure a parking space for the season at St Joseph’s school - a short walk to Arbour Park - for just £20. This includes home pre-season friendlies, except for the Wycombe Wanderers match (due to the St Joseph’s Summer Fayre), and also excludes Testimonial, Cup and Playoff matches. Cost is applicable for Blue Badge holders and non-Blue Badge holders. Parking is not allocated and parking is managed on a first come, first served basis. Parking permits are on sale to season ticket holders and non-season ticket holders alike, but <b>will only be sold online, via the <a href="/shop" target="_blank">online Club Shop</a></b>. Parking permits are non-transferrable.Please note that parking is at your own risk. The Club assumes no responsibility for loss or damage to parked vehicles.</p>

	<p class="bold">Other options</p>

	<p>Off street parking around Arbour Park is extremely limited - however there are a number of car parks within walking distance. The ground is conveniently situated approximately half a mile from the train and bus stations where plenty of <a href="https://drive.google.com/open?id=0B9nC0MruyclrWlA1WWNmbWt4SXM" target="_blank">car parking</a> can also be found. It takes approximately 15-20 minutes to walk from the station and the car parks. <b>If you do park in residential streets, please be respectful to our neighbours and do not block entranceways and driveways, and do not park dangerously, including on blind corners. There are a number of car parks within walking distance of Arbour Park. </b>We would encourage supporters to travel by public transport where possible.</p>

	<h2>Bus and taxis</h2>

	<p>1 - Operated by First Bus (To Britwell). Every 15mins Mon-Sat daytime, every 60mins evenings and Sunday.</p>
	<p>WF1 - Operated by Redline bus (To Wexham Park Hospital). Every 20mins Mon-Fri daytime, every 30mins evening, Saturday and Sunday. Please note that some Mon-Friy evening routes serve the Park &amp; Ride (for Wexham Park staff) which stops at Wexham Park stadium, these operate via Wexham Road and Shaggy Calf lane rather than the main service via Stoke Road.</p>
	<p>12 - Operated by First Bus every 30mins Mon-Sat daytime. Note there is no evening service running on this route for evening games.</p>

	<h2>Cycle</h2>

	<p>For those of you who wish to cycle, a cycle map of the borough is available from <a href="http://slough.gov.uk/downloads/cycling-map.pdf" target="_blank">Slough Borough Council</a> and the ground does have cycle racks. Alternatively at Slough Station (Brunel Way) secure indoor bike storage is available – the first month is free and then 50p per day thereafter (only charged on days used) – to find out more visit <a href="http://www.cycleslough.com/hub" target="_blank">Cycle Hub Slough</a>. If you don’t want to use your own bike, Cycle Hire Slough is available at nine sites across the borough and the nearest docking station is at Slough Station (Brunel Way).</p>

	<h2>Rail and bus</h2>

	<p>For those of you who travel by train, PlusBus is available as an add-on rail ticket and for £3 per day offers unlimited bus travel on that day. These are available from ticket offices, machines and online.</p>

	<h2>Disabled parking</h2>

	<p>There are designated parking spaces for Blue Badge holders at Arbour Park. There is a limited number of allocated spaces, so please contact the club in advance if you require a space by emailing <a href="mailto:gensec@sloughtownfc.net">gensec@sloughtownfc.net</a>.</p>

	<h2>Parking at St Joseph's Catholic School</h2>

	<p>Parking is strictly limited to those supporters who have purchased a parking permit. Parking at the school will be by permit only. Permits will NOT be on sale at the gates of the school. If you have ordered a permit but it has not yet arrived, the stewards will have a registration list to allow entry where appropriate. Supporters are reminded that permits are non-transferable.</p>
    
    <h2>Create Your Own Directions</h2> 
    
    <div class="map_container" >
    	<div id="the_map"></div>
    </div>	
    
	<br class="clear" />
    
    <form onSubmit="calcRoute();return false;" id="routeForm">
		<input type="text" id="routeStart" value="Enter a location and press Send" title="Enter a location and press Send" class="clearMeFocus" >
		<input type="submit" value="Calculate route">
	</form>
    
	<div id="directionsPanel"></div>

</div><!--End Content Area -->