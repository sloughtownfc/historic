<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">
    
    <img src="<?php echo $logo; ?>" alt="Club Badge" class="right" style="border:none;" />
            
    <h1><?php echo $opposition; ?></h1>
    
    <br class="clear" />
	
    <h2>Statistics</h2>

    <table class="the_statistics" summary="Profiles">
        <tbody>
        <tr>
            <th class="profiles">Games Played:</th>
            <td class="profiles"><?php echo $total_games; ?></td>
        </tr>
        <tr>
            <th class="profiles">Games Won:</th>
            <td class="profiles"><?php echo $win; ?></td>
        </tr>
        <tr>
            <th class="profiles">Games Drawn:</th>
            <td class="profiles"><?php echo $draw; ?></td>
        </tr>
        <tr>
            <th class="profiles">Games Lost:</th>
            <td class="profiles"><?php echo $lose; ?></td>
        </tr>
        <tr>
            <th class="profiles">Goals For/Against:</th>
            <td class="profiles"><?php echo $goals_for; ?>/<?php echo $goals_against; ?></td>
        </tr>
        </tbody>
    </table>    
    
    <br class="clear" />
    
    <h2>Matches Played</h2>
    
    <table class="the_statistics" summary="Fixtures &amp; Reports">
	<thead>
		<tr>
			<th style="width:20%" class='centre'>Match Date</th>
			<th style="width:25%" class='centre'>Home Team</th>
			<th style="width:5%" class='centre'>Score</th>
			<th style="width:25%" class='centre'>Away Team</th>
			<th style="width:16%" class='centre'>Competition</th>
			<th style="width:14%" class='centre'>Report</th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach ($fixtures as $fixture)
			{
				// Open row
				echo '<tr>';
				
				$newDate = date("d-m-Y", strtotime($fixture->match_date));
				
				// Match date
				echo '<td bgcolor="#ffffff" class="centre">' . (($fixture->match_date === NULL) ? 'TBC' : $newDate) . '</td>';
				// Set home/away teams
				if ($fixture->is_home)
				{
					$home_team = 'Slough Town';
					$away_team = $fixture->opposition->name;
				}
				else
				{
					$home_team = $fixture->opposition->name;
					$away_team = 'Slough Town';
				}
				// Home Team
				echo '<td bgcolor="#ffffff" class="centre">' . $home_team . '</td>';

				// TODO: This is ugly, should have belong_to / has_many in the models
				// and then reference like $fixture->report->report_home_score etc.

				// Score
				echo '<td bgcolor="#ffffff" class="centre">' . $fixture->report_home_score . '-' .  $fixture->report_away_score . '</td>';
				// Away Team
				echo '<td bgcolor="#ffffff" class="centre">' . $away_team . '</td>';
				// Competition
				echo '<td bgcolor="#ffffff" class="centre">' . $fixture->competition->name . '</td>';

				// TODO: This is ugly, should have belong_to / has_many in the models
				// and then reference like $fixture->report->report_home_score etc.

				// Set report link / if present
				if ($fixture->report_published === 1)
				{
					$report_link = '<a href="/reports/' . $fixture->id . '/">Report</a>';
				}
				else
				{
					$report_link = 'To Play';
				}

				// Report
				echo '<td bgcolor="#ffffff" class="centre">' . $report_link . '</td>';

				// Close row
				echo '</tr>';
			}
		?>
	</tbody>
	</table>
</div><!--End Content Area -->