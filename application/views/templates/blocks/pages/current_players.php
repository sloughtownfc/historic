<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

    <div class="opener">

        <h1>Slough Town Squad </h1>

        <p class="bold">Please find a list below of the Current Slough Town squad for the current season.</p>

        <p>Click on each player for a biog on each player as well as statistical data for this and past seasons.</p>

        <p>If you are looking to find information on a former player please view our former <a href="player-database">player database</a>.</p>

    </div><!-- End Opener -->

    <br class="clear" />

	<?php
	foreach ($positions as $position)
	{
		$position_id = $position->id;
		if (isset($grouped_players[$position_id]))
		{
			echo "<h2>$position->name</h2>";
	?>
		<table class="the_statistics" summary="Current Players">
		<thead>
			<tr>
				<th style="width:50%; padding-left:5px;">Name</th>
				<th class="centre" style="width:50%;">Profile</th>
			</tr>
		</thead>
		<tbody>
	<?php
			$players = $grouped_players[$position_id];
			foreach ($players as $player)
			{
	?>
		<tr>
			<td bgcolor="#ffffff"  style="padding-left:5px;" ><?php echo $player->first_name . ' ' . $player->last_name; ?></td>
			<td bgcolor="#ffffff" class="centre"><a href="/profile/<?php echo $player->id; ?>/<?php echo $player->first_name . '-' . $player->last_name; ?>">More Information</a></td>
		</tr>
	<?php
			}
	?>
		</tbody>
		</table>
	<?php
		}
	}
	?>

</div><!--End Content Area -->