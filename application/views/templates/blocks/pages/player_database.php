<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<script type="application/javascript">
	$(document).ready(function(){
		$('.the_statistics tbody tr:odd td').css('background', '#f1f1f1');
	});
</script>

<div id="content_area">

    <div class="opener">

        <h1>Slough Town Player Database</h1>

        <p class="bold">This page is dedicated to providing information on all the players who have played for Slough Town FC throughout the seasons, we aim to keep track of those who move onto pastures new as well as those who are currently with the Rebels.</p>

        <p>Players are listed alphabetically by surname.</p>
        
        <?php
			echo form::open();
			echo form::input('search');
			echo form::submit('submit', 'Search');
			echo form::close();
		?>

    </div><!-- End Opener -->

    <br class="clear" />
	
    <table class="the_statistics" summary="Matches Played">
		<thead>
			<tr>
				<th style="padding-left:5px;">Name</th>
				<th class="centre">Profile</th>
			</tr>
		</thead>
		<tbody>
		<?php
		foreach ($players as $player)
		{
		?>
		<tr>
			<td bgcolor="#ffffff" style="padding-left:5px;"><?php echo $player->first_name . ' ' . $player->last_name; ?></td>
			<td bgcolor="#ffffff" class="centre"><a href="/profile/<?php echo $player->id; ?>/<?php echo $player->first_name . '-' . $player->last_name; ?>">More Information</a></td>
		</tr>
		<?php
		}
		?>
		</tbody>
	</table>

<?php
// Show pagination.
echo $pagination;
?>
</div><!--End Content Area -->