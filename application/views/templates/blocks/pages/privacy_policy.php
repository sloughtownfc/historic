<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">
            
    <div class="opener">

        <h1>Privacy Policy</h1>
        
        <p class="bold">We are committed to safeguarding the privacy of our website visitors; this policy sets out how we will treat your personal information.</p>

    </div><!-- End Opener -->
    
    <br class="clear" />

    <h2>(1) What information do we collect?</h2>
    
    <p>We may collect, store and use the following kinds of personal information:</p>
    
    <ul class="privacy">
        <li>(a) information about your computer and about your visits to and use of this website (including your IP address, geographical location, browser type and version operating system, referral source, length of visit, page views, website navigation);</li>
        <li>(b) information relating to any transactions carried out between you and us on or in relation to this website, including information relating to any purchases you make of our goods or services;</li>
        <li>(c) information that you provide to us for the purpose of registering with us;</li>
        <li>(d) information that you provide to us for the purpose of subscribing to our website services, email notifications and/or newsletters;</li>
        <li>(e) any other information that you choose to send to us;</li>
    </ul>
    
    <br class="clear" />
    
    <h2>(2) Cookies</h2>
    
    <p>A cookie consists of infomration sent by a web server to a web browser, and stored by the browser. The information is then sent back to the server each time the browser requests a page from the server. This enables the web server to identify and track the web browser. We use session cookies on the website.  We will use the session cookies to: keep track of you whilst you navigate the website. Session cookies will be deleted from your computer when you close your browser.</p>
    
    <p>We use Google Analytics to analyse the use of this website. Google Analytics generates statistical and other information about website use by means of cookies, which are stored on users' computers. The information generated relating to our website is used to create reports about the use of the website. Google will store this information. Google's privacy policy is available at: <a href="http://www.google.com/privacypolicy.html">http://www.google.com/privacypolicy.html</a>. To opt out of Google Analytics please visit <a href="http://tools.google.com/dlpage/gaoptout?hl=en">http://tools.google.com/dlpage/gaoptout?hl=en</a>.</p>
    
    <p>Most browsers allow you to reject all cookies, whilst some browsers allow you to reject just third party cookies.  For example, in Internet Explorer you can refuse all cookies by clicking “Tools”, “Internet Options”, “Privacy”, and selecting “Block all cookies” using the sliding selector.  Blocking all cookies will, however, have a negative impact upon the usability of many websites including this one.</p>
        
    <h2>(3) Using your personal information</h2>
    
    <p>Personal information submitted to us via this website will be used for the purposes specified in this privacy policy or in the relevant parts of the website.</p>
    
    <p>We may use your personal information to:</p>
    
    <ul class="privacy">
        <li>(a) administer the website;</li>
        <li>(b) improve your browsing experience by personalising the website;</li>
        <li>(c) enable your use of the services available on the website;</li>
        <li>(d) send to you goods purchased via the website, and supply to you services purchased via the website;</li>
        <li>(e) send statements and invoices to you, and collect payments from you;</li>
        <li>(f) send you general (non-marketing) commercial communications;</li>
        <li>(g) send you email notifications which you have specifically requested;</li>
        <li>(h) send to you our newsletter and other marketing communications relating to our business which we think may be of interest to you by post or, where you have specifically agreed to this, by email or similar technology (you can inform us at any time if you no longer require marketing communications);</li>
        <li>(i) deal with enquiries and complaints made by or about you relating to the website; and all our website financial transactions are handled through our payment services provider, PayPal.  You can review the PayPal privacy policy at www.paypal.com.  We will share information with  PayPal only to the extent necessary for the purposes of processing payments you make via our website and dealing with complaints and queries relating to such payments.</li>
    </ul>
    
    <br class="clear" />
    
    <h2>(4) Security of your personal information</h2>
    
    <p>We will take reasonable technical and organisational precautions to prevent the loss, misuse or alteration of your personal information.</p>
    
    <p>We will store all the personal information you provide on our secure servers. All electronic transactions you make to or receive from us will be encrypted using SSL technology. Of course, data transmission over the internet is inherently insecure, and we cannot guarantee the security of data sent over the internet. You are responsible for keeping your password and user details confidential. We will not ask you for your password (except when you log in to the website).</p>
    
    
    <h2>(5) Policy amendments</h2>
    
    <p>We may update this privacy policy from time-to-time by posting a new version on our website.  You should check this page occasionally to ensure you are happy with any changes.</p>
        
    <h2>(6) Your rights</h2>
    
    <p>You may instruct us to provide you with any personal information we hold about you.  Provision of such information will be subject to:</p>
    
    <ul class="privacy">
        <li>(a) the payment of a fee currently fixed at &pound;10.00; and</li>
        <li>(b) the supply of appropriate evidence of your identity (for this purpose, we will usually accept a photocopy of your passport certified by a solicitor or bank plus an original copy of a utility bill showing your current address).</li>
    </ul>
    
    <p>We may withhold such personal information to the extent permitted by law.</p>
    
    <p>You may instruct us not to process your personal information for marketing purposes by email at any time.  In practice, you will usually either expressly agree in advance to our use of your personal information for marketing purposes, or we will provide you with an opportunity to opt-out of the use of your personal information for marketing purposes.</p>
        
    <h2>(7) Third party websites</h2>
    
    <p>The website contains links to other websites. We are not responsible for the privacy policies or practices of third party websites.</p>
        
    <h2>(8) Updating information</h2>
    
    <p>Please let us know if the personal information which we hold about you needs to be corrected or updated.</p>
        
    <h2>(9) Contact</h2>
    
    <p>If you have any questions about this privacy policy or our treatment of your personal information, please write to us by email via the contact us form.</p>

</div><!--End Content Area -->