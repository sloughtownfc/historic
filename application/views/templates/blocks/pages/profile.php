<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

    <h2>Role: <?php echo (($player->active) === 1 ? 'Current' : 'Former'); ?></h2>

	<img src="<?php echo $player->picture_url; ?>" alt="Player profile image"  class="right"/>

    <h1><?php echo $player_name; ?></h1>
	
    <?php
	
		$body = explode("\n\n", $player->description);
		
		$body_count = count($body);
		if ($body_count > 0)
		{
			echo "<p><b>$body[0]</b></p>";
			for ($i = 1; $i < $body_count; $i++)
			{
				echo "<p>$body[$i]</p>";
			}
		}	
		
	?>

    <br class="clear" />

    <h2>Statistics</h2>

    <table class="profiles" summary="Profiles">
        <tbody>
        <tr>
            <th class="profiles">First Appearance:</th>
            <td class="profiles"><?php echo $first_appearance; ?></td>
        </tr>
        <tr>
            <th class="profiles">First Goal:</th>
            <td class="profiles"><?php echo $first_goal; ?></td>
        </tr>
        <tr>
            <th class="profiles">Position:</th>
            <td class="profiles"><?php echo $position; ?></td>
        </tr>
        <tr>
            <th class="profiles">Total STFC Appearances:</th>
            <td class="profiles"><?php echo $total_appearances; ?></td>
        </tr>
        <tr>
            <th class="profiles">Total STFC Goals:</th>
            <td class="profiles"><?php echo $total_goals; ?></td>
        </tr>
        </tbody>
    </table>
    
    <?php
	if($player->achievements != NULL)
	{
    
    	echo'<h2>Achievements</h2>';
    
		$achieve = explode("\n", $player->achievements);
		
		$achieve_count = count($achieve);
		if ($achieve_count > 0)
		{
			for ($i = 0; $i < $achieve_count - 1; $i++)
			{
				echo "<p class='nomargin'>$achieve[$i]</p>";
			}
			echo "<p>" . $achieve[count($achieve)-1] . "</p>";
		}	
	}
	?>

    <h2>Matches Played</h2>

    <table class="the_statistics" summary="Matches Played">
      <thead>
        <tr>
          <th class='centre'>Match Date</th>
          <th class='centre'>Home Team</th>
          <th class='centre'>Score</th>
          <th class='centre'>Away Team</th>
          <th class='centre'>Competition</th>
          <th class='centre'>Goals</th>
          <th class='centre'>Cards</th>
          <th class='centre'>Report</th>
        </tr>
      </thead>
      <tbody>
        <?php
			foreach ($matches_played as $match)
			{
				// Open row
				echo '<tr>';
				
				$newDate = date("d-m-Y", strtotime($match->match_date));
				
				// Match date
				echo '<td bgcolor="#ffffff" class="centre">' . (($match->match_date === NULL) ? 'TBC' : $newDate) . '</td>';
				// Set home/away teams
				if ($match->is_home)
				{
					$home_team = 'Slough Town';
					$away_team = $match->opposition_name;
				}
				else
				{
					$home_team = $match->opposition_name;
					$away_team = 'Slough Town';
				}
				// Home Team
				echo '<td bgcolor="#ffffff" class="centre">' . $home_team . '</td>';

				// TODO: This is ugly, should have belong_to / has_many in the models
				// and then reference like $fixture->report->report_home_score etc.

				// Score
				echo '<td bgcolor="#ffffff" class="centre">' . $match->report_home_score . '-' .  $match->report_away_score . '</td>';
				// Away Team
				echo '<td bgcolor="#ffffff" class="centre">' . $away_team . '</td>';
				// Competition
				echo '<td bgcolor="#ffffff" class="centre">' . $match->competition_name . '</td>';
				// Goals
				echo '<td bgcolor="#ffffff" class="centre">';
				if ($match->goals == 1)
				{
					echo '<img class="stats" alt="1" src="/images/icons/report/1goal.png" />';
				}
				elseif ($match->goals > 1)
				{
					echo '<img class="stats" alt="' . $match->goals . '" src="/images/icons/report/' . $match->goals . 'goals.png" />';
				}
				else
				{
					echo '-';
				}
				echo '</td>';
				// Bookings
				echo '<td bgcolor="#ffffff" class="centre">';
				if ($match->bookings == 1)
				{
					echo '<img class="stats" alt="yellow" src="/images/icons/report/yellow.png" />';
				}
				elseif ($match->bookings == 2)
				{
					echo '<img class="stats" alt="yellow" src="/images/icons/report/red.png" />';
				}
				else
				{
					echo '-';
				}
				echo '</td>';

				// TODO: This is ugly, should have belong_to / has_many in the models
				// and then reference like $fixture->report->report_home_score etc.

				// Set report link / if present
				if ($match->report_published == 1)
				{
					$report_link = '<a href="/reports/' . $match->id . '/">Report</a>';
				}
				else
				{
					$report_link = '---';
				}

				// Report
				echo '<td bgcolor="#ffffff" class="centre">' . $report_link . '</td>';

				// Close row
				echo '</tr>';
			}
		?>
        </tbody>
    </table>

</div><!--End Content Area -->