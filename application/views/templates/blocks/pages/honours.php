<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">

    <div class="opener">

        <h1>Slough Town FC Honours</h1>
        
        <p class="bold">Please find Slough Town's honours list below as a football club, covering Slough Town and all previous namesakes.</p>

    </div><!-- End Opener -->
    
    <br class="clear" />
    
    <h2>First Team Honours</h2>

    <p class="bold blue centre honours">Ansell Trophy Winners</p>

    <p class="centre">1973/74 (Shared)</p>

    <p class="bold blue centre honours">Athenian League Premier Champions</p>
    
    <p class="centre">1967/68, 1971/72, 1972/73</p>

    <p class="bold blue centre honours">Athenian League Premier Division Runners Up</p>
    
    <p class="centre">1968/69</p>

    <p class="bold blue centre honours">Athenian League Cup Winners</p>
    
    <p class="centre">1971/72, 1972/73</p>

    <p class="bold blue centre honours">Athenian League Div 1 Champions</p>
    
    <p class="centre">1964/65</p>
    
    <p class="bold blue centre honours">Athenian League Memorial Shield Winners</p>
    
    <p class="centre">1971/72, 1972/73</p>

    <p class="bold blue centre honours">Berks &amp; Bucks Benevolent Cup Winners</p>
    
    <p class="centre">1938/39, 1945/46 (Shared), 1948/49, 1950/51, 1955/56, 1956/57, 1957/58, 1961/62</p>
    
    <p class="bold blue centre honours">Berks &amp; Bucks Benevolent Cup Runners Up</p>
    
    <p class="centre">1935/36, 1952/53, 1959/60</p>

    <p class="bold blue centre honours">Berks &amp; Bucks Junior Cup Winners</p>
    
    <p class="centre">1891/92</p>

    <p class="bold blue centre honours">Berks &amp; Bucks Red Cross Cup Winners</p>
    
    <p class="centre">1943/44, 1944/45 (Shared)</p>

    <p class="bold blue centre honours">Berks &amp; Bucks Senior Cup Winners</p>
    
    <p class="centre">1902/03, 1919/20, 1923/24, 1926/27, 1935/36, 1954/55, 1970/71, 1971/72, 1976/77, 1980/81</p>
    
    <p class="bold blue centre honours">Berks &amp; Bucks Senior Cup Runners Up</p>

    <p class="centre">1901/02, 1920/21, 1929/30, 1946/47, 1948/49, 1949/50, 1950/51, 1952/53, 1964/65, 1967/68, 1968/69, 1972/73, 1973/74, 1989/90, 1994/95</p>

    <p class="bold blue centre honours">Bert Hall Cup Winners</p>
    
    <p class="centre">1933/34, 1936/37, 1937/38, 1939/40, 1943/44, 1945/46, 1947/48, 1967/68, 1969/70</p>

    <p class="bold blue centre honours">Bert Hall Cup Runners Up</p>
    
    <p class="centre">1934/35, 1935/36</p>

    <p class="bold blue centre honours">Bressington Memorial Shield Winners</p>
    
    <p class="centre">1982/83</p>

    <p class="bold blue centre honours">Bucks Charity Cup Winners</p>
    
    <p class="centre">1920/21, 1923/24 (Played 1924/25)</p>
    
    <p class="bold blue centre honours">Bucks Charity Cup Runners Up</p>
    
    <p class="centre">1922/23</p>

    <p class="bold blue centre honours">Bucks Hospital Cup Winners</p>
    
    <p class="centre">1920/21</p>

    <p class="bold blue centre honours">Chippenham Hospital Cup Winners</p>
    
    <p class="centre">1945/46</p>

    <p class="bold blue centre honours">Corinthian League Champions</p>
    
    <p class="centre">1950/51</p>
    
    <p class="bold blue centre honours">Corinthian League Runners Up</p>
    
    <p class="centre">1945/46, 1946/47, 1957/58</p>
    
    <p class="bold blue centre honours">Corinthian League Memorial Shield Winners</p>
    
    <p class="centre">1964/65</p>
    
    <p class="bold blue centre honours">Corinthian League Memorial Shield Runners Up</p>
    
    <p class="centre">1957/58</p>
    
    <p class="bold blue centre honours">Diadora League Cup Runners Up</p>
    
    <p class="centre">1994/95</p>

    <p class="bold blue centre honours">Dylon Charity Shield Runners Up</p>
    
    <p class="centre">1981/82</p>

    <p class="bold blue centre honours">FA Amateur Cup Runners Up</p>
    
    <p class="centre">1972/73</p>
    
    <p class="bold blue centre honours">FA Amateur Cup Semi-Finalists</p>
    
    <p class="centre">1970/71</p>
    
    <p class="bold blue centre honours">FA Amateur Cup Quarter-Finalists</p>
    
    <p class="centre">1952/53, 1966/67, 1969/70, 1971/72</p>
    
    <p class="bold blue centre honours">FA Cup 1st Round</p>
    
    <p class="centre">1945/46, 1973/74, 1974/75, 1998/99, 1991/92, 1992/93, 1993/94, 1994/95, 1995/96, 1996/97, 1997/98, 1998/99, 2002/03, 2012/13, 2017/18.</p>
    
    <p class="bold blue centre honours">FA Cup 2nd Round</p>
    
    <p class="centre">1970/71, 1979/80, 1982/83, 1985/86, 1986/87, 2004/05</p>
    
    <p class="bold blue centre honours">FA Trophy Semi Finalists</p>
    
    <p class="centre">1976/77, 1997/98</p>

    <p class="bold blue centre honours">Great Western Suburban League Runners Up</p>
    
    <p class="centre">1919/20</p>

    <p class="bold blue centre honours">Herts &amp; Middlesex League Runners Up</p>
    
    <p class="centre">1943/44</p>

    <p class="bold blue centre honours">Isthmian Div 2 Runners Up</p>
    
    <p class="centre">1973/74</p>
    
    <p class="bold blue centre honours">Isthmian Premier Div Champions</p>
    
    <p class="centre">1980/81, 1989/90</p>
    
    <p class="bold blue centre honours">Isthmian Premier Runners Up</p>
    
    <p class="centre">1994/95</p>
    
    <p class="bold blue centre honours">Isthmian League Cup</p>
    
    <p class="centre">2004/05</p>

    <p class="bold blue centre honours">Isthmian Subsidary Cup Winners</p>
    
    <p class="centre">1975/76, 1980/81</p>

    <p class="bold blue centre honours">Lloyd Memorial Winners</p>
    
    <p class="centre">1953/54</p>

    <p class="bold blue centre honours">Marlow Inviatation Cup Winners</p>
    
    <p class="centre">1969/70, 1978/79, 1980/81, 1982/83 (Shared)</p>
    
    <p class="bold blue centre honours">Maidenhead Charity Cup Runners Up</p>
    
    <p class="centre">1953/54</p>

    <p class="bold blue centre honours">Maidenhead Norfolkian Cup Runners Up</p>
    
    <p class="centre">1891/92</p>
    
    <p class="bold blue centre honours">Mittas Cup Runners Up</p>
    
    <p class="centre">1967/68 (Played 1968/69)</p>

    <p class="bold blue centre honours">Oxford Hospital Cup Winners</p>
    
    <p class="centre">1920/21 (Shared)</p>
    
    <p class="bold blue centre honours">Oxford Hospital Cup Runners Up</p>
    
    <p class="centre">1905/06, 1911/12, 1912/13, 1921/22</p>

    <p class="bold blue centre honours">Premier Midweek Floodlight League Champions</p>
    
    <p class="centre">1971/72</p>
    
    <p class="bold blue centre honours">Premier Midweek Floodlight League Section Winners</p>
    
    <p class="centre">1973/74</p>
    
    <p class="bold blue centre honours">Printed &amp; Allied Trades Cup Winners</p>
    
    <p class="centre">1953/54</p>
    
    <p class="bold blue centre honours">Slough Centenary Cup Winners</p>
    
    <p class="centre">1989/90</p>

    <p class="bold blue centre honours">Slough Charity Cup Winners</p>
    
    <p class="centre">1930/31, 1931/32, 1932/33, 1933/34</p>

    <p class="bold blue centre honours">Slough Hospital Cup Runners Up</p>
    
    <p class="centre">1945/46, 1947/48, 1948/49, 1946/47, 1968/69</p>

    <p class="bold blue centre honours">Southern Combination Cup Winners</p>
    
    <p class="centre">1958/59, 1962/63, 1963/64</p>
    
    <p class="bold blue centre honours">Southern Combination Cup Runners Up</p>
    
    <p class="centre">1959/60 (Played 1960/61)</p>

    <p class="bold blue centre honours">Spartan League Runners Up</p>
    
    <p class="centre">1920/21, 1921/22, 1931/32, 1932/33, 1938/39</p>
    
    <p class="bold blue centre honours">Thame Tournament Runners Up</p>
    
    <p class="centre">1994/95</p>
    
    <p class="bold blue centre honours">Tom Spicer Cup Winners</p>
    
    <p class="centre">1988/89</p>
    
    <p class="bold blue centre honours">Vauxhall League Charity Shield Winners</p>
    
    <p class="centre">1990/91</p>

    <p class="bold blue centre honours">Westminster Hospital Cup Winners</p>
    
    <p class="centre">1966/67, 1967/68</p>

    <p class="bold blue centre honours">Westminster Hospital Cup Runners Up</p>
    
    <p class="centre">1968/69</p>

    <p class="bold blue centre honours">Windsor Hospital Cup Winners</p>
    
    <p class="centre">1965/66, 1966/67 (Shared), 1968/69 (Shared), 1969/70</p>
    
    <p class="bold blue centre honours">Windsor Hospital Cup Runners Up</p>
    
    <p class="centre">1921/22, 1930/31, 1934/35, 1935/36 (Played 1936/37), 1967/68</p>

    <p class="bold blue centre honours">Wycombe Floodlight League Champions</p>
    
    <p class="centre">1925/26, 1926/27, 1965/66, 1967/68</p>

    <h2>Reserve Team Honours</h2>
    
    <h2>1901/02 - Present Day</h2>
    
    <p>A list of the honours won by Slough Town FC's Reserve teams or their past namesakes between 1901/02 and the present day listed in alphabetical order.</p>
    
    <p class="bold blue centre honours">Athenian League Premier Champions</p>

    <p class="centre">1966/67</p>
    
    <p class="bold blue centre honours">Athenian League Premier Runners Up</p>
    
    <p class="centre">1969/70, 1970/71</p>
    
    <p class="bold blue centre honours">Athenian League Div 1 Champions</p>
    
    <p class="centre">1964/65</p>
    
    <p class="bold blue centre honours">Berks &amp; Bucks Junior Cup Runners Up</p>
    
    <p class="centre">1903/04, 1905/06</p>
    
    <p class="bold blue centre honours">Berks &amp; Bucks Intermediate Cup Runners Up</p>
    
    <p class="centre">1959/60</p>
    
    <p class="bold blue centre honours">Bucks Charity Cup Winners</p>
    
    <p class="centre">1923/24</p>
    
    <p class="bold blue centre honours">Corinthian League Champions</p>
    
    <p class="centre">1948/49, 1951/52</p>
    
    <p class="bold blue centre honours">Maidenhead Norfolkian Cup Winners</p>
    
    <p class="centre">1922/23, 1988/89</p>
    
    <p class="bold blue centre honours">Maidenhead Norfolkian Cup Runners Up</p>
    
    <p class="centre">1908/09, 1913/14</p>
    
    <p class="bold blue centre honours">Neale Cup Winners</p>
    
    <p class="centre">1949/50</p>
    
    <p class="bold blue centre honours">Neale Cup Runners Up</p>
    
    <p class="centre">1951/52, 1955/56, 1959/60</p>
    
    <p class="bold blue centre honours">Presidents Cup Winners</p>
    
    <p class="centre">2003/04</p>
    
    <p class="bold blue centre honours">Reading &amp; District League Champions</p>
    
    <p class="centre">1938/39</p>
    
    <p class="bold blue centre honours">Reading Senior Cup Winners</p>
    
    <p class="centre">1950/51, 1951/52, 1965/66</p>
    
    <p class="bold blue centre honours">Reading Senior Cup Runners Up</p>
    
    <p class="centre">1956/57</p>
    
    <p class="bold blue centre honours">Slough Town Cup Winners</p>
    
    <p class="centre">1946/47, 1953/54</p>
    
    <p class="bold blue centre honours">Slough Town Cup Runners Up</p>
    
    <p class="centre">1926/27</p>
    
    <p class="bold blue centre honours">Suburban League Cup Runners Up</p>
    
    <p class="centre">1981/82</p>
    
    <p class="bold blue centre honours">Suburban League Champions Cup Winners</p>
    
    <p class="centre">1995/96, 1996/97</p>
    
    <p class="bold blue centre honours">Suburban League Challenge Shield Runners Up</p>
    
    <p class="centre">1995/96</p>
    
    <p class="bold blue centre honours">Suburban League North Champions</p>
    
    <p class="centre">1994/95</p>
    
    <p class="bold blue centre honours">Suburban League Premier Champions</p>
    
    <p class="centre">1997/98</p>
    
    <p class="bold blue centre honours">Suburban League West Champions</p>
    
    <p class="centre">1988/89</p>
    
    <p class="bold blue centre honours">South Bucks &amp; East Berkshire League Runners Up</p>
    
    <p class="centre">1901/08</p>
    
    <p class="bold blue centre honours">Spartan League Runners Up</p>
    
    <p class="centre">1926/27</p>
    
    <p class="bold blue centre honours">Westminster Hospital Cup Winners</p>
    
    <p class="centre">1955/56, 1956/57, 1957/58, 1958/59, 1960/61</p>
    
    <p class="bold blue centre honours">Windsor, Slough &amp; District League Champions</p>
    
    <p class="centre">1913/14</p>

</div><!--End Content Area -->
