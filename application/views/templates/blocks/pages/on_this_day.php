<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<script type="application/javascript">
	$(document).ready(function(){
		$('.the_statistics tbody tr:odd td').css('background', '#f1f1f1');
	});
</script>

<div id="content_area">

    <div class="opener">

        <h1>On This Day</h1>
		
        <?php
		// get current day
		$currentday = date('dS');

		// get current month
		$currentmonth = date('F');
		
        ?>
        
        <p class="bold">Listed below are matches played on todays date, the <?php echo $currentday; ?> <?php echo $currentmonth; ?>.</p>

    </div><!-- End Opener -->

    <br class="clear" />
    
    <?php
    	if ($today == NULL)
		{
			echo '<p>There is no historical data for today.</p>';
		}
		else
		{
	?>
    
    	<table class="the_statistics" summary="Fixtures &amp; Reports">
			<thead>
				<tr>
					<th style="width:20%" class='centre'>Match Date</th>
					<th style="width:25%" class='centre'>Home Team</th>
					<th style="width:5%" class='centre'>Score</th>
					<th style="width:25%" class='centre'>Away Team</th>
					<th style="width:16%" class='centre'>Competition</th>
					<th style="width:14%" class='centre'>Report</th>
				</tr>
			</thead>
			<tbody><?php
					foreach ($today as $day)
					{			
						// Open row
						echo '<tr>';
				
						$newDate = date("d-m-Y", strtotime($day->match_date));
						
						// Match date
						echo '<td bgcolor="#ffffff" class="centre">' . $newDate . '</td>';								
		
						if ($day->is_home == 1)
						{
							// Home Team
							$home_team = "Slough Town";
							
							//Away Team
							$away_team = $day->name;
						}
						else
						{
							//Home Team
							$home_team = $day->name;
							
							// Away Team
							$away_team = "Slough Town";
						}
						
						
						echo '<td bgcolor="#ffffff" class="centre">' . $home_team . '</td>';
						
						// Score
						echo '<td bgcolor="#ffffff" class="centre">' . $day->report_home_score . '-' .  $day->report_away_score . '</td>';
						// Away Team
						echo '<td bgcolor="#ffffff" class="centre">' . $away_team . '</td>';
						
						// Competition
						echo '<td bgcolor="#ffffff" class="centre">' . $day->league . '</td>';
						
						$report_link = '<a href="/reports/' . $day->id . '/">Report</a>';
						
						// Report
						echo '<td bgcolor="#ffffff" class="centre">' . $report_link . '</td>';

						// Close row
						echo '</tr>';	
					}
				?></tbody>
			</table>
    	<?	
		}
		?>     

</div><!--End Content Area -->