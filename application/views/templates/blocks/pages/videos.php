<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">
            
    <div class="opener">

        <h1>Club Videos - STFC TV</h1>
        
        <p class="bold">The Official Slough Town Website will provide you with videos throughout the campaign, as well as other videos from previous seasons.</p>
        
        <p>All videos are available on our <a href="http://www.youtube.com/user/sloughtownfc" onclick="window.open(this.href); return false;">YouTube</a> channel.</p>
        
        <p><a href="http://www.youtube.com/user/sloughtownfc" onclick="window.open(this.href); return false;">Visit Slough Town FC's Offcial YouTube Channel</a></p>
        
        <p>View videos from a particular season:  
        
        <?php echo form::open(); ?>
        <select id="season" name="season">
        <?php
            foreach ($seasons as $id => $name)
            {
                // Check whether this season is selected
                if ($id === $season_selected)
                {
                    echo '<option value="' . $id . '" selected="selected">' . $name . '</option>';
                }
                else
                {
                    echo '<option value="' . $id . '">' . $name . '</option>';
                }
            }
        ?>
        </select>
        <?php
            echo form::submit('submit', 'Change Season');
            echo form::close();
        ?></p>
        
    </div><!-- End Opener -->
    
    <br class="clear" />
    
    <?php

		$i = 0;
		
		foreach ($fixtures as $fixture)
		{
			

			if ($fixture->video_url != NULL)
			{
				
				
    			$newDate = date("d-m-Y", strtotime($fixture->match_date));
			
				//Display amount of stories == to $i
				if ($i == 0)
				{
					echo '<img src="' . $fixture->video_picture_thumbnail_url . '" alt="News Item image" />';
					
				}
				
				if ($i <= 5)
				{
					echo '<p><a href="video/' . $fixture->id . '">' . $newDate . ' : Video footage against ' . $fixture->opposition->name. '</a></p>';
									
				}
				
				if ($i == 5)
				{
					echo '<hr />';
					
				}
				
				if ($i > 5)
				{
					echo '<p><a href="video/' . $fixture->id . '">' . $newDate . ' : Video footage against ' . $fixture->opposition->name. '</a></p>';
					
				}
				
				
				$i++;
					
				}
			
		}
	?>

</div><!--End Content Area -->