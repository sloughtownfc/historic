<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">
            
    <div class="opener">

        <h1>Contact Us</h1>
        
        <p class="bold">There are a variety of ways that you can get in touch with various people at the football club and website. Please look below to find the contact you are looking for:</p>
        
        <p><span class="bold">Slough Town FC</span><br />Arbour Park,<br />Stoke Road,<br />Slough,<br />Berkshire,<br />SL2 5AY<br /><span class="bold">(Absolutely no correspondence to this address)</span></p>
        <p><strong>Tel:</strong> 07792 126124<br /><strong>Email:</strong> <a href="mailto:gensec@sloughtownfc.net">gensec@sloughtownfc.net</a></p>

        <p><span class="bold">All Correspondence to:</span> - Slough Town FC, Stowe Road, Slough, Berks, SL1 5QF<br/><span class="bold">All Invoices to:</span> - Slough Town FC, Tanglewood, Gough Road, Fleet, Hants, GU51 4LT</p>

        <p>Slough Town FC is owned by Thames Valley Sports &amp; Leisure Limited (Company Number 5223594) which is incorporated in the United Kingdom, a company in which Mr S. Easterbook has a significant interest.</p>
        
        <p>If you are looking to find directions to the club, please <a href="how-to-find-us">click here</a>. Ticketing &amp; Admission information can be found <a href="tickets">here</a>. Please note the Arbour Park <a href="groundregs">ground regulations</a>.</p>
       
    </div><!-- End Opener -->

    <br class="clear" />
      
    <h2>Club Contacts</h2>
        
    <p>General Club Enquiries<br /><a href="getintouch">Click to Contact</a></p>
    
    <p>Club Trials/Player Enquiries<br /><a href="getintouch">Click to Contact</a></p>
                    
    <p>Website Comments/Problems<br /><a href="getintouch">Click to Contact</a></p>
    
    <p><strong>Club Chairman</strong><br />Steve Easterbrook<br /><a href="getintouch">Click to Contact</a></p>
        
    <p><strong>General Manager</strong><br />John Porter<br /></p>

    <p><strong>General Secretary</strong><br />Kay Lathey<br /><a href="getintouch">Click to Contact</a></p>

    <p><strong>Steering Group</strong><br />Steve Easterbrook<br />John Porter<br />Kay Lathey<br />Mike Lightfoot</p>

    <p><strong>Operational Management Committee</strong><br />Kay Lathey<br />Adrian Gomm<br />Sam Newman<br />Mike Lightfoot<br />John Porter<br />Tom Gathern<br />Matt Webb</p>
    
    <p><strong>Matchday Secretary</strong><br />Mark Hunter<br /><a href="getintouch">Click to Contact</a></p>
    
    <p><strong>Match &amp; Player Sponsorship</strong><br />Alan Harding<br /><a href="getintouch">Click to Contact</a></p>
    
    <p><strong>Safety Officer</strong><br />Sam Newman</p>
    
    <p><strong>Commercial Manager</strong><br />Paul Lillywhite<br /><a href="getintouch">Click to Contact</a></p>
        
    <p><strong>First Team Managers</strong><br />Neil Baker and Jon Underwood</p>

    <p><strong>Physiotherapist</strong>:<br />Kevin McGoldrick</p>
                
    <p><strong>Boardroom Hospitality Manager</strong><br />Debbie Lillywhite</p>
    
    <p><strong>Programme Editor</strong><br />Steve Chapman<br /><a href="getintouch">Click to Contact</a></p>

    <p><strong>Life Member</strong><br />Roy Merryweather</p>
    
</div><!--End Content Area -->
