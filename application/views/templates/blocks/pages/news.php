<?php defined('SYSPATH') OR die('No direct access allowed.'); $title="News"; ?>

<div id="content_area">
            
    <div class="opener">

        <h1>Rebels News</h1>
        
        <p class="bold">The Slough Town FC Website provide you with all the latest and breaking news throughout the season as it happens.</p>
        
        <p>View news from a previous season:  
        
        <?php echo form::open(); ?>
        <select id="season" name="season">
        <?php
            foreach ($seasons as $id => $name)
            {
                // Check whether this season is selected
                if ($id === $season_selected)
                {
                    echo '<option value="' . $id . '" selected="selected">' . $name . '</option>';
                }
                else
                {
                    echo '<option value="' . $id . '">' . $name . '</option>';
                }
            }
        ?>
        </select>
        <?php
            echo form::submit('submit', 'Change Season');
            echo form::close();
        ?></p>
        
    </div><!-- End Opener -->
    
    <br class="clear" />
    
    <?php
		
		if ($news->count() == 0)
		{
			echo '<p>There is currently no news for this season.</p>';	
		}
	
		$i = 0;
		
		foreach ($news as $item)
		{
	
			//Display amount of stories == to $i
			if ($i == 0)
			{
				echo '<img src="' . $item->small_picture_url . '" alt="News Item image" />';
				
			}
			
			$newDate = date("d-m-Y", strtotime($item->publish_date ));
			$link = str_replace(" ","-",$item->headline);
			
			if ($i <= 5)
			{
				echo '<p><a href="article/' . $item->id . '/' . $link . '">' . $newDate . ' : ' . $item->headline . '</a><br />' . $item->excerpt . '</p>';
				
			}
			
			if ($i == 5)
			{
				echo '<hr />';
				
			}
			
			if ($i > 5)
			{
				echo '<p><a href="article/' . $item->id . '/' . $link . '">' . $newDate . ' : ' . $item->headline . '</a></p>';
				
			}
			
			
			$i++;
		}
	?>
               
</div><!--End Content Area -->
