<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div id="content_area">
            
    <div class="opener">

        <h1>Managers Notes</h1>
        
        <p class="bold">Joint Managers Neil Baker and Jon Underwood's Managers Notes for the season will appear in this section throughout the season.</p>
        
        <p>View notes from a previous season:  
                    
        <?php echo form::open(); ?>
        <select id="season" name="season">
        <?php
            foreach ($seasons as $id => $name)
            {
                // Check whether this season is selected
                if ($id === $season_selected)
                {
                    echo '<option value="' . $id . '" selected="selected">' . $name . '</option>';
                }
                else
                {
                    echo '<option value="' . $id . '">' . $name . '</option>';
                }
            }
        ?>
        </select>
        <?php
            echo form::submit('submit', 'Change Season');
            echo form::close();
        ?></p>
        
    </div><!-- End Opener -->
    
    <br class="clear" />
    
    <?php
	
		if ($fixtures->count() == 0)
		{
			echo '<p>There is currently no manager notes for this season.</p>';	
		}
		
		$i = 0;
		
		foreach ($fixtures as $fixture)
		{
			
			//Display amount of stories == to $i
			if ($i == 0)
			{
				echo '<img src="' . $fixture->manager_note_picture_thumbnail_url . '" alt="Managers Note image" />';
				
			}
			
			$newDate = date("d-m-Y", strtotime($fixture->match_date));
			$link = str_replace(" ","-",$fixture->manager_note_headline);
			
			if ($i <= 5)
			{
				echo '<p><a href="note/' . $fixture->id . '/' . $link .'">' . $newDate . ' : ' . $fixture->manager_note_headline . '</a><br />' . $fixture->manager_note_excerpt . '</p>';
								
			}
			
			if ($i == 5)
			{
				echo '<hr />';
				
			}
			
			if ($i > 5)
			{
				echo '<p><a href="note/' . $fixture->id . '/' . $link .'">' . $newDate . ' : ' . $fixture->manager_note_headline . '</a></p>';
				
			}
			
			
			$i++;
		}
	?>
    
</div><!--End Content Area -->
