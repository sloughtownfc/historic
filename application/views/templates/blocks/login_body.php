<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
	<form id="login-form" method="POST" action="<?php echo url::base(FALSE) . $login_uri; ?>">
       <img src="/images/admin/badge.png" alt="Slough Town Badge" style="float:left; margin:0 20px;" />

       	<fieldset>

       		<h1>Slough Town Admin</h1>

	    <p><?php 	if (isset($message)) echo $message; ?></p>

	    <label for="username">Username:</label>
	    <input type="text" name="username" size="20" />
	    <br />
	    <label for="password">Password:</label>
	    <input type="password" name="password" size="20" />
	    <br />
	    <input type="submit" value="Login" />

	</fieldset>
	</form>