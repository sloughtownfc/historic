<?php defined('SYSPATH') OR die('No direct access allowed.');

	// Fetch information via the sidebar helper
	$sidebar = new sidebar;
	$next_fixture = $sidebar->tab_next_fixture();
	$last_fixture = $sidebar->tab_last_fixture();
	$top_active_goalscorers = $sidebar->tab_goals();
	$reserve_next_fixture = $sidebar->tab_reserves();
	$u18_next_fixture = $sidebar->tab_u18s();
?>

<div id="right_nav">

	<a href="http://www.bostik.co.uk" target="_blank"><img src="/images/main/southernleague_small.png" alt="The Evo-Stik League South" style="margin-bottom:10px;"/></a>

    <div id="tabs" class="widget">

        <ul class="tabnav">
            <li><a href="#next">Next</a></li>
            <li><a href="#last">Prev</a></li>
            <li><a href="#goals">Goals</a></li>
            <li><a href="#table">Table</a></li>
            <!--li><a href="#res">Res</a></li-->
            <!--li><a href="#youth">U18s</a></li-->
        </ul>

        <div id="next" class="tabdiv">

			<?php
			if ($next_fixture !== FALSE)
			{
				$newkickoffmain = substr($next_fixture['kickoff'], 0, -3);
				
			?>
        	<h2><?php echo $next_fixture['match_date']; ?></h2>

            <div class="fixture_details">

                <h2 class="opponents">vs <?php echo $next_fixture['opposition_name']; ?></h2>

            	<p class="bold <?php if($next_fixture['away_kit'] == 1) { echo "away"; } else { echo "home"; } ?>"><?php echo $next_fixture['home_away'] . ', KO ' . $newkickoffmain; ?></p>

				<?php
				if ($next_fixture['is_home'])
				{
				?>
					<p class="nomargin"><span>+</span> Match Sponsor: <?php echo $next_fixture['match_sponsor']; ?></p>
					<p class="nomargin"><span>+</span> Ball Sponsor: <?php echo $next_fixture['ball_sponsor']; ?></p>
				<?php
				}
				else
				{
				?>
					<p class="nomargin"><span>+</span> <a href="/directions/<?php echo $next_fixture['opposition_id']; ?>">Directions</a></p>
					<p class="nomargin"><span>+</span> <a href="/supporters-coach">Supporters Coach Details</a></p>
				<?php
				}
				?>
            </div>
            <img src="<?php echo $next_fixture['opposition_badge']; ?>" alt="Club Badge"  style="float:left; border: 1px solid #bbbbbb;" />
			<?php
			}
			else
			{
			?>
			<h2>No planned next match.</h2>
            <p>Pre season fixtures will be announced soon.</p>
			<?php
			}
			?>

        </div><!--Next-->

        <div id="last" class="tabdiv">

       	 <?php
			if ($last_fixture !== FALSE)
			{
				if ($last_fixture['slough_score'] > $last_fixture['opposition_score'])
				{
					$big_score = $last_fixture['slough_score'];
					$small_score = $last_fixture['opposition_score'];
				}
				else
				{
					$big_score = $last_fixture['opposition_score'];
					$small_score = $last_fixture['slough_score'];
				}
			?>
        	<h2><?php echo $last_fixture['match_date']; ?></h2>

            <div class="fixture_details">

                <h2 class="opponents">vs <?php echo $last_fixture['opposition_name']; ?></h2>

            	<p class="bold"><?php echo $last_fixture['result'] . ' ' . $big_score . '-' . $small_score . ' (' . $last_fixture['home_away'] . ')'; ?></p>

                <p class="nomargin"><span>+</span> <a href="/reports/<?php echo $last_fixture['report_id']; ?>/<?php $link = str_replace(" ","-",$last_fixture['headline']); echo $link; ?>">View Report</a></p>

            	<p class="nomargin"><span>+</span> <a href="/league-table">League Table</a></p>

            </div>

            <img src="<?php echo $last_fixture['opposition_badge']; ?>" alt="Club Badge"  style="float:left; border: 1px solid #bbbbbb;" />
			<?php
			}
			else
			{
			?>
			<h2>No last match.</h2>
			<?php
			}
			?>
        </div><!--Last-->

        <div id="table" class="tabdiv">

        	<h2>League Table <a href="/league-table">[Full Stats]</a></h2>

            <?php  
				//Load file and create SimpleXMLElement
				$load = 'https://www.footballwebpages.co.uk/league.xml?comp=8'; // read your file  

				$ch = curl_init();
				$timeout = 5;
				curl_setopt($ch, CURLOPT_URL, $load);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
				$result = curl_exec($ch);
				curl_close($ch);

				if ($xml = simplexml_load_string($result))
				{
				//Find Slough in the XML file and assign the position we are in on the table
				$curr_position = $xml->xpath('/leagueTable/team[name="Slough Town"]/position');
				
				//We Always want FIVE teams from the table
				//So we find our current position and as a default show one team above us
				//If were in 1st, 22nd, 21st, 20th then we can't show one team above us
				//As if were 1st (Champions!) one team above does not exist
				//If were 22nd, 21st or 20th showing one team above us won't show enough teams
				//So we use a switch for this...
				if (isset($curr_position[0])) {
				    switch ($curr_position[0]) {
				        case 1 : $value = 1;
				            break;
				        case 2 : $value = $curr_position[0]-1;
				            break;
				        case 23 : $value = $curr_position[0]-3;
				            break;
				        case 24 : $value = $curr_position[0]-4;
				            break;
				        default : $value = $curr_position[0]-2; 
				    }
				} else {
				    $value = 0;
				}
				
				echo '
				<table class="the_statistics nomargin" summary="League Table">
				<thead>
				<tr> 
				<th class="centre">Pos</th>
				<th class="centre">Team</th>
				<th class="centre">Pld</th>
				<th class="centre">Pts</th>
				</tr>
				</thead>
				<tbody>';
				
				$i=1;
				$bg=0;
				foreach($xml->team as $team) { 
				
				//If it's us then show our orangey colour
				if($team->name=="Slough Town") { $bg = '#efaf12'; }  else { $bg = ($bg=='#f1f1f1' ? '#ffffff' : '#f1f1f1'); }
				
				if ($i >= $value) {
				echo '
				<tr> 
				<td bgcolor="'.$bg.'" class="centre">'.$team->position.'</td>
				<td bgcolor="'.$bg.'" class="centre">'.$team->name.'</td>
				<td bgcolor="'.$bg.'" class="centre">'.$team->played.'</td>
				<td bgcolor="'.$bg.'" class="centre">'.$team->points.'</td>
				</tr>';
				}
   				 if ($i <= $value+3) { $i++; } else { break; } //Show five teams only then break
				}
				echo'
				</tbody>
				</table>';
				$i++;
				}
				else
				{
					echo '<p>Unable to fetch League Table at this time.</p>';
				}
			?>

        </div><!--Table-->

        <div id="goals" class="tabdiv">

        	<h2>Top Goalscorers <a href="/player-statistics">[Full Stats]</a></h2>

             <table class="the_statistics nomargin" summary="Top Goalscorers">
              <thead>
                <tr>
                  <th class='centre'>Pos</th>
                  <th class='centre'>Name</th>
                  <th class='centre'>Goals</th>
                </tr>
              </thead>
              <tbody>
              	<?php
				$i = 0;
              	foreach ($top_active_goalscorers as $key => $goalscorer)
              	{
					if ($i == 5)
					{
						break;
						
					}
					
              		if ($key == 0)
              		{
              			$bg = '#efaf12';
              		}
              		elseif (($key % 2) === 0)
              		{
              			$bg = '#f1f1f1';
              		}
              		else
              		{
              			$bg = '#ffffff';
              		}
              	?>
                <tr>
                  <td bgcolor="<?php echo $bg; ?>" class='centre'><?php echo $key + 1; ?></td>
                  <td bgcolor="<?php echo $bg; ?>" class='centre'><a href="/profile/<?php echo $goalscorer['id']; ?>/<?php $link = str_replace(" ","-",$goalscorer['name']); echo $link; ?>"><?php echo $goalscorer['name']; ?></a></td>
                  <td bgcolor="<?php echo $bg; ?>" class='centre'><?php echo $goalscorer['goals']; ?></td>
                </tr>
                <?php
              	$i++;
				}
              	?>
                </tbody>
            </table>

        </div><!--Goals-->
        
        <!--div id="res" class="tabdiv">
        
        	<?php
			if ($reserve_next_fixture !== FALSE)
			{
				$reserve_newkickoff = substr($reserve_next_fixture['kickoff'], 0, -3);				
			?>
        	<h2><?php echo $reserve_next_fixture['match_date']; ?></h2>

            <div class="fixture_details">

                <h2 class="opponents">vs <?php echo $reserve_next_fixture['opposition']; ?></h2>

            	<p class="bold"><?php echo $reserve_next_fixture['home_away'] . ', KO ' . $reserve_newkickoff; ?></p>
				
                <p class="nomargin">+ <a href="reserve-fixtures">Fixtures</a></p>

            	<p class="nomargin">+ <a href="http://72.3.224.252/LeagueTab.cfm?TblName=Matches&DivisionID=5611&LeagueCode=HLNC2012" onclick="window.open(this.href); return false;">League Table</a></p>
				
            </div>
            <img src="/images/main/badge.jpg" alt="Club Badge"  style="float:left; border: 1px solid #bbbbbb;" />
			<?php
			}
			else
			{
			?>
			<h2>No planned next match.</h2>
			<?php
			}
			?>
            
        </div--><!--Res-->

        <!--div id="youth" class="tabdiv">
        
        	<?php
			if ($u18_next_fixture !== FALSE)
			{
				$u18_newkickoff = substr($u18_next_fixture['kickoff'], 0, -3);				
			?>
        	<h2><?php echo $u18_next_fixture['match_date']; ?></h2>

            <div class="fixture_details">

                <h2 class="opponents">vs <?php echo $u18_next_fixture['opposition']; ?></h2>

            	<p class="bold"><?php echo $u18_next_fixture['home_away'] . ', KO ' . $u18_newkickoff; ?></p>
				
                <p class="nomargin">+ <a href="u18-fixtures">Fixtures</a></p>

            	<p class="nomargin">+ <a href="http://www.goalrun.com/clubs?club_fixtures&tid=21740" onclick="window.open(this.href); return false;">League Table</a></p>
				
            </div>
            <img src="/images/main/badge.jpg" alt="Club Badge"  style="float:left; border: 1px solid #bbbbbb;" />
			<?php
			}
			else
			{
			?>
			<h2>No planned next match.</h2>
			<?php
			}
			?>
            
        </div--><!--U18s-->

    </div><!-- End Tabs -->

	<a href="/pdf/STFC-Sponsorship.pdf" target="_blank"><img src="/images/right_nav/sponsorship-new.jpg" alt="Slough Town FC Online sponsorship opportunities" style="margin-bottom:10px;" /></a>

	<?php
	    //$arr = array('<a href="/shop/index.php?route=product/product&path=61&product_id=67" target="_blank"><img src="/images/right_nav/homekit.jpg" alt="Slough Town FC Season Tickets" style="margin-bottom:10px;" /></a>','<a href="http://eepurl.com/NJyNX" target="_blank"><img src="/images/right_nav/newsletter.jpg" alt="Slough Town Newsletter" style="margin-bottom:10px;" /></a>');
    
    	//echo $arr[rand(0,1)];
	?>


    <br class="clear"/>

    <h3>Slough Town FC Squad</h3> 
    
    <div id="squad_container">
    	<div id="squad_container_inner">
        <?php
			$squad = $sidebar->squad_form();
		
			foreach ($squad as $position => $players)
			{
				echo '<h4>' . $position . '</h4>';
				foreach ($players as $player)
				{
					echo '<a class="side_squad" href="/profile/' . $player->id . '/' . $player->first_name . '-' . $player->last_name . '">' . $player->first_name . ' ' . $player->last_name . '</a>';
				}
			}
        ?>
        </div>
    </div>
    
    <br class="clear"/>

    <a href="http://www.gamblingkingz.co.uk/the-southern-football-league/" target="_blank"><img src="/images/sponsors/gambling-kings.jpg" alt="GamblingKingz logo" style="margin-bottom:10px;" /></a>

	<h3>Follow Slough Town FC</h3>
    
    <div id="social_container">
    	<link itemprop="url" href="http://www.sloughtownfc.net"> 
        <a class="social twitter" href="http://www.twitter.com/sloughtownfc" target="_blank"></a>
    
        <a class="social facebook" href="http://www.facebook.com/sloughtownfc" target="_blank"></a>
    
        <a class="social youtube" href="http://www.youtube.com/user/sloughtownfc" target="_blank"></a>
    
        <a class="social flickr" href="https://www.flickr.com/photos/horshamrebel/collections/72157682912196254/" target="_blank"></a>
	</div>

</div><!-- End Right Nav -->
