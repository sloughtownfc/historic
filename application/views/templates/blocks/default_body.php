<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<link rel="stylesheet" href="/js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/js/fancybox/jquery.fancybox.pack.js"></script>

<?php

if (Kohana::Config('promotions.enabled'))
{

?>
<script type="text/javascript">
	$(document).ready(function() {
		$("#hidden_link").fancybox().trigger('click');
	});
</script>

<?php
}
?>

<a id="hidden_link" rel="group" href="#poster" style="display:none;"></a>


<div id="poster" style="display:none; float:left; width:960px; height:360px;">

<?php
		$arrmain = array(
			'<a href="/shop/index.php?route=product/category&path=59" target="_blank" onclick="_gaq.push([\'_trackEvent\', \'Season tickets\', \'Season tickets\', \'2018/19\'])" style="float:left; width:960px; height:360px; background: url(\'/images/poster/seasontickets18-19v2.jpg\') no-repeat;" ></a>',
			'<a href="/shop/index.php?route=product/product&path=59&product_id=64" target="_blank" onclick="_gaq.push([\'_trackEvent\', \'Club Shop\', \'Club Shop\', \'Junior Membership\'])" style="float:left; width:960px; height:360px; background: url(\'/images/poster/junior18-19.jpg\') no-repeat;" ></a>');
		 //'<a href="/shop/index.php?route=product/category&path=65" target="_blank" onclick="_gaq.push([\'_trackEvent\', \'Club Shop\', \'Club Shop\', \'WBA\'])" style="float:left; width:960px; height:360px; background: url(\'/images/poster/wba.jpg\') no-repeat;" ></a>',
		 //'<a href="/shop/index.php?route=product/product&path=61&product_id=67" target="_blank" onclick="_gaq.push([\'_trackEvent\', \'Club Shop\', \'Club Shop\', \'Home 15/16\'])" style="float:left; width:960px; height:360px; background: url(\'/images/poster/homekit.jpg\') no-repeat;" ></a>'
		//'<a href="/fixtures" target="_blank" onclick="_gaq.push([\'_trackEvent\', \'Fixtures\', \'2016/17\', \'Frome Town\'])" style="float:left; width:960px; height:360px; background: url(\'/images/poster/homecoming_fro.jpg\') no-repeat;" ></a>');
		echo "{$arrmain[array_rand($arrmain)]}";
	?>
</div>

<ul class='aviaslider' id="frontpage-slider">

	<?php
	$i = 0;

	foreach ($slider as $slide)
	{
		$newDate = date("d-m-Y", strtotime($slide->match_date ));
		$link2 = str_replace(" ","-",$slide->headline);

		if($slide->tbl == 'news')
		{
			$link="article/";
		} elseif($slide->tbl == 'fixture')
		{
			$link="reports/";
		} elseif($slide->tbl == 'u18')
		{
			$link="u18-report/";
		} elseif($slide->tbl == 'reserve')
		{
			$link="reserve-report/";
		} elseif($slide->tbl == 'academy')
		{
			$link="academy-report/";
		} elseif($slide->tbl == "video")
		{
			$link="video/";
		}

		echo '<li><a href="'. $link . $slide->fix_id .'/' . $link2 . '"><img src="' . $slide->picture_url . '" alt="'. $newDate .' : '. $slide->headline .' :: '. $slide->excerpt .'" /></a></li>';

	}

	?>
	</ul>

<div id="content_area">

    <div class="news">

        <h2>Recent News</h2>
		<?php

		foreach ($recent_news as $recent_news_item)
		{
			$link2 = str_replace(" ","-",$recent_news_item->headline);

			if($recent_news_item->tbl == 'news')
			{
				$link="article/";
			} elseif($recent_news_item->tbl == 'fixture')
			{
				$link="reports/";
			} elseif($recent_news_item->tbl == 'u18')
			{
				$link="u18-report/";
			} elseif($recent_news_item->tbl == 'reserve')
			{
				$link="reserve-report/";
			} elseif($recent_news_item->tbl == 'academy')
			{
				$link="academy-report/";
			} elseif($recent_news_item->tbl == 'video')
			{
				$link="video/";
			}

			echo '<p><a href="'. $link . $recent_news_item->fix_id .'/' . $link2 . '">' . $recent_news_item->headline . '</a><br />'. $recent_news_item->excerpt .'</p>';
		}

		?>

    </div>

    <a href="/shop/index.php?route=product/category&path=59" class="season_tickets" onclick="window.open(this.href); return false;"></a>


    <?php
	    $arrmain = array('<a href="/shop" class="shop" onclick="window.open(this.href); return false;"></a>', '<a href="/500_club" class="fivehundredclub"></a>');
    	echo $arrmain[rand(0,1)];
	?>

    <a href="http://www.youtube.com/channel/UC4J-s4b4Cv0XAXQyfiTL0Eg?feature=watch" class="main_small small_add" onclick="window.open(this.href); return false;"></a>

    <a href="http://www.garyhousephotography.co.uk" class="main_small horsham" onclick="window.open(this.href); return false;"></a>

    <div class="split split_margin">

        <h2>Slough Town Soapbox</h2>

        <?php
        // Get cache instance
        $cache = Cache::instance();

        $feed = $cache->get('feed');

        if ( ! $feed)
        {
	        $request_url = "http://sloughtownsoapbox.blogspot.com/feeds/posts/default?alt=rss";

	        $ch = curl_init();
	        $timeout = 5;
	        curl_setopt($ch, CURLOPT_URL, $request_url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	        $result = curl_exec($ch);
	        curl_close($ch);

	        if ($xml = simplexml_load_string($result))
	        {
				$i = 0;

		        foreach($xml->channel->item as $item)
		        {

					// Display $i number of stories
					if ($i === 4)
					{
						break;
					}

					$upper = ucwords(strtolower($item->title));

					$feed .= "<p><a class='external_link' href='".$item->link."' onclick='window.open(this.href); return false;'>".$upper."</a></p>";

					$i++;
					kohana::log('debug', $i);
		        }

		        $cache->set('feed', $feed, NULL, 3600);
	        }
	        else
	        {
	        	$feed = '<p>Unable to fetch feed at this time.</p>';
	        }
        }

        echo $feed;

        ?>

    </div>

    <div class="split">

        <h2>On This Day <a class="more" href="/on-this-day">[Full List]</a></h2>

        <?php
		if ($today == NULL)
		{
			echo '<p>There is no historical data for today.</p>';
		}
		else
		{
			$i = 0;

			foreach ($today as $day)
			{

				// Display $i number of stories
				if ($i === 4)
				{
					break;
				}

				$newDate = date("d-m-Y", strtotime($day->match_date));

				if ($day->is_home == 1)
				{
					echo '<p><a href="reports/' . $day->id . '">' . $newDate . ' : Slough Town ' . $day->report_home_score . '-' . $day->report_away_score . ' ' . $day->name.'</a></p>';
				}
				else
				{
					echo '<p><a href="reports/' . $day->id . '">' . $newDate . ' : '. $day->name.' ' . $day->report_home_score . '-' . $day->report_away_score . ' Slough Town</a></p>';
				}

			$i++;

			}
		}
		?>

    </div>
    
    <!--div class="press">
        
        <h2>STFC Newsletter</h2>
        <p>Subscribe to Slough Town FC's email newsletter keeping you up to date with all the latest match reports, squad movements, club news and also receive our FREE match-day video highlights before anyone else!</p>
        <div id="mc_embed_signup">
			<form action="http://sloughtownfc.us3.list-manage.com/subscribe/post?u=a784e0e3204a3805e03b86b6a&amp;id=17cc03f018" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate validation" target="_blank" novalidate>
                <div class="mc-field-group">
                    <input type="email" value="Email address" name="EMAIL" class="validate[required,custom[email]] clearMeFocus" id="mce-EMAIL">
                </div>
                <div class="mc-field-group">
                    <input type="text" value="First name" name="FNAME" class="validate clearMeFocus" id="mce-FNAME">
                </div>
                <div class="mc-field-group">
                    <input type="text" value="Last name" name="LNAME" class="required" id="mce-LNAME">
                </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none"></div>
                    </div>	
                <div class="clear">
                    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                </div>
            </form>
        </div>

    </div--><!-- End Mailchimp -->

    <div class="press">

        <h2>In The Media</h2>

        <?php

		foreach ($papers as $paper)
		{
			$newDate = date("d-m-Y", strtotime($paper->publish_date ));

			echo '<p><a class="external_link" href="'. $paper->url .'" onclick="window.open(this.href); return false;">'. $newDate .' : '. $paper->headline .'</a></p>';
		}

		?>
    </div>

</div><!--End Content Area -->