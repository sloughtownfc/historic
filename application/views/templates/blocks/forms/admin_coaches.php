<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Coaches Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit Coach Details</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));
// Coach departs.
echo '<div class="form-group">
  		<label for="coach_departs" id="coach_departs" class="col-sm-2 control-label">Depart time</label>
  		<div class="col-sm-10">';
	    	echo form::input('coach_departs', $form['coach_departs'], 'class="form-control timepicker validate[required]"');
	  		echo (empty($errors['coach_departs'])) ? '' : $errors['coach_departs'];
	echo'</div>
</div>';

// Coach adult price.
echo '<div class="form-group">
  		<label for="coach_adult_price" id="coach_adult_price" class="col-sm-2 control-label">Adult Price</label>
  		<div class="col-sm-10">';
    		echo form::input('coach_adult_price', $form['coach_adult_price'], 'class="form-control validate[required]"');
			echo (empty($errors['coach_adult_price'])) ? '' : $errors['coach_adult_price'];
  	echo'</div>
</div>';

// Coach seniors price
echo '<div class="form-group">
  		<label for="coach_reduced_price" id="coach_reduced_price" class="col-sm-2 control-label">Senior &amp; Junior Price</label>
  		<div class="col-sm-10">';
    		echo form::input('coach_reduced_price', $form['coach_reduced_price'], 'class="form-control validate[required]"');
			echo (empty($errors['coach_reduced_price'])) ? '' : $errors['coach_reduced_price'];
  	echo'</div>
</div>';

// Submit.
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();