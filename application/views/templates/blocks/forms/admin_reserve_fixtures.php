<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Reserve Fixtures Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit Fixture</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));
// Checkbox field fixes (checkbox $_POST hack).
// Since an unchecked box does not return anything to $_POST
// (i.e., gets set as null) it will not pass validation.
echo form::hidden('is_home', 0);
// Fixture match date.
echo '<div class="form-group">
  		<label for="match_date" id="match_date" class="col-sm-2 control-label">Match Date <small>(Leave blank if TBC)</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('match_date', $form['match_date'], 'class="form-control datepicker"');
			echo (empty($errors['match_date'])) ? '' : $errors['match_date'];
	echo'</div>
</div>';
// Kickoff time.
echo '<div class="form-group">
  		<label for="kickoff" id="kickoff" class="col-sm-2 control-label">Kick Off <small>(Leave blank if TBC)</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('kickoff', $form['kickoff'], 'class="form-control timepicker"');
			echo (empty($errors['kickoff'])) ? '' : $errors['kickoff'];
	echo'</div>
</div>';
// Fixture competition.
echo '<div class="form-group">
  		<label for="competition" id="competition" class="col-sm-2 control-label">Competition</label>
  		<div class="col-sm-10">';
	    	echo form::input('competition', $form['competition'], 'class="form-control validate[required]"');
			echo (empty($errors['competition'])) ? '' : $errors['competition'];
	echo'</div>
</div>';

// Fixture opposition.
echo '<div class="form-group">
  		<label for="opposition" id="opposition" class="col-sm-2 control-label">Opposition</label>
  		<div class="col-sm-10">';
	    	echo form::input('opposition', $form['opposition'], 'class="form-control validate[required]"');
			echo (empty($errors['opposition'])) ? '' : $errors['opposition'];
	echo'</div>
</div>';

// Is game being played at home?
echo '<div class="form-group">
  		<label for="is_home" id="is_home" class="col-sm-2 control-label">Is a home game?</label>
  		<div class="col-sm-10">';
	    	echo form::checkbox('is_home', 1, (empty($selected['is_home'])) ? '' : $selected['is_home']);
			echo (empty($errors['is_home'])) ? '' : $errors['is_home'];
	echo'</div>
</div>';
// Submit.
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();
