<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin News Form view
 */
echo '<h2>Add/Edit A News Story</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));
// News headline.
echo '<div class="form-group">
  		<label for="headline" class="col-sm-2 control-label">Headline</label>
  		<div class="col-sm-10">';
	    	echo form::input('headline', $form['headline'], 'class="form-control validate[required]"');
			echo (empty($errors['headline'])) ? '' : $errors['headline'];
	echo'</div>
</div>';

// News story author.
echo '<div class="form-group">
  		<label for="author" class="col-sm-2 control-label">Author</label>
  		<div class="col-sm-10">';
	    	echo form::input('author', $form['author'], 'class="form-control validate[required]"');
			echo (empty($errors['author'])) ? '' : $errors['author'];
	echo'</div>
</div>';

// News Publish Date.
echo '<div class="form-group">
  		<label for="publish_date" class="col-sm-2 control-label">Publish Date</label>
  		<div class="col-sm-10">';
	    	echo form::input('publish_date', $form['publish_date'], 'class="form-control datepicker validate[required,custom[date]]"');
			echo (empty($errors['publish_date'])) ? '' : $errors['publish_date'];
	echo'</div>
</div>';

// News Publish Time.
echo '<div class="form-group">
  		<label for="publish_time"  class="col-sm-2 control-label">Publish Time</label>
  		<div class="col-sm-10">';
	    	echo form::input('publish_time', $form['publish_time'], 'class="form-control timepicker"');
			echo (empty($errors['publish_time'])) ? '' : $errors['publish_time'];
	echo'</div>
</div>';

// Body.
echo '<div class="form-group">
  		<label for="excerpt" class="col-sm-2 control-label">Excerpt</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('excerpt', $form['excerpt'], 'class="form-control validate[required]"');
			echo (empty($errors['excerpt'])) ? '' : $errors['excerpt'];
	echo'</div>
</div>';

// Body.
echo '<div class="form-group">
  		<label for="body" class="col-sm-2 control-label">Body</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('body', $form['body'], 'class="form-control big validate[required]"');
			echo (empty($errors['body'])) ? '' : $errors['body'];
	echo'</div>
</div>';

// Video URL.
echo '<div class="form-group">
  		<label for="video_url" class="col-sm-2 control-label">YouTube ID</label>
  		<div class="col-sm-10">';
	    	echo form::input('video_url', $form['video_url'], 'class="form-control"');
			echo (empty($errors['video_url'])) ? '' : $errors['video_url'];
	echo'</div>
</div>';

// Audio URL.
echo '<div class="form-group">
  		<label for="audio_url" class="col-sm-2 control-label">Audioboom ID</label>
  		<div class="col-sm-10">';
	    	echo form::input('audio_url', $form['audio_url'], 'class="form-control"');
			echo (empty($errors['audio_url'])) ? '' : $errors['audio_url'];
	echo'</div>
</div>';

// News picture URL.
echo '<div class="form-group">
  		<label for="picture_url" class="col-sm-2 control-label">Picture URL <small class="gallery">Click to select image</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('picture_url', $form['picture_url'], 'class="form-control validate[required]"');
			echo (empty($errors['picture_url'])) ? '' : $errors['picture_url'];
	echo'</div>
</div>';

// News picture URL.
echo '<div class="form-group">
  		<label for="small_picture_url" class="col-sm-2 control-label">Small Picture URL <small class="gallery">Click to select image</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('small_picture_url', $form['small_picture_url'], 'class="form-control validate[required]"');
			echo (empty($errors['small_picture_url'])) ? '' : $errors['small_picture_url'];
	echo'</div>
</div>';

// News caption.
echo '<div class="form-group">
  		<label for="caption" class="col-sm-2 control-label">Caption</label>
  		<div class="col-sm-10">';
	    	echo form::input('caption', $form['caption'], 'class="form-control validate[required]"');
			echo (empty($errors['caption'])) ? '' : $errors['caption'];
	echo'</div>
</div>';

// News quote.
echo '<div class="form-group">
  		<label for="quote" class="col-sm-2 control-label">Quote</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('quote', $form['quote'], 'class="form-control"');
			echo (empty($errors['quote'])) ? '' : $errors['quote'];
	echo'</div>
</div>';

// News quote by.
echo '<div class="form-group">
  		<label for="quote_by" class="col-sm-2 control-label">Quote By</label>
  		<div class="col-sm-10">';
	    	echo form::input('quote_by', $form['quote_by'], 'class="form-control"');
			echo (empty($errors['quote_by'])) ? '' : $errors['quote_by'];
	echo'</div>
</div>';

// Submit
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();