<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Videos Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit Video</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));
// Video author.
echo '<div class="form-group">
  		<label for="video_author" id="video_author" class="col-sm-2 control-label">Video Director</label>
  		<div class="col-sm-10">';
	    	echo form::input('video_author', $form['video_author'], 'class="form-control validate[required]"');
			echo (empty($errors['video_author'])) ? '' : $errors['video_author'];
	echo'</div>
</div>';

// Video picture URL.
echo '<div class="form-group">
  		<label for="video_picture_url" id="video_picture_url" class="col-sm-2 control-label">External Picture URL <small class="gallery">Click to select image</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('video_picture_url', $form['video_picture_url'], 'class="form-control validate[required]"');
			echo (empty($errors['video_picture_url'])) ? '' : $errors['video_picture_url'];
	echo'</div>
</div>';

// Video picture URL.
echo '<div class="form-group">
  		<label for="video_picture_thumbnail_url" id="video_picture_thumbnail_url" class="col-sm-2 control-label">Internal Picture URL <small class="gallery">Click to select image</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('video_picture_thumbnail_url', $form['video_picture_thumbnail_url'], 'class="form-control validate[required]"');
			echo (empty($errors['video_picture_thumbnail_url'])) ? '' : $errors['video_picture_thumbnail_url'];
	echo'</div>
</div>';

// Video URL.
echo '<div class="form-group">
  		<label for="video_url" id="video_url" class="col-sm-2 control-label">YouTube ID</label>
  		<div class="col-sm-10">';
	    	echo form::input('video_url', $form['video_url'], 'class="form-control validate[required]"');
			echo (empty($errors['video_url'])) ? '' : $errors['video_url'];
	echo'</div>
</div>';

// Body.
echo '<div class="form-group">
  		<label for="video_body" id="video_body" class="col-sm-2 control-label">Body</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('video_body', $form['video_body'], 'class="form-control big validate[required]"');
			echo (empty($errors['video_body'])) ? '' : $errors['video_body'];
	echo'</div>
</div>';

// Submit.
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();
