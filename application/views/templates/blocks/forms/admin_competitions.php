<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Competitions Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit Competition</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));
// Checkbox field fixes (checkbox $_POST hack).
// Since an unchecked box does not return anything to $_POST
// (i.e., gets set as null) it will not pass validation.
echo form::hidden('stats_recorded', 0);

// Competition name.
echo '<div class="form-group">
  		<label for="name" id="name" class="col-sm-2 control-label">Name</label>
  		<div class="col-sm-10">';
	    	echo form::input('name', $form['name'], 'class="form-control  validate[required]"');
			echo (empty($errors['name'])) ? '' : $errors['name'];
	echo'</div>
</div>';

// Competition description.
echo '<div class="form-group">
  		<label for="description" id="description" class="col-sm-2 control-label">Description</label>
  		<div class="col-sm-10">';
	    	echo form::input('description', $form['description'], 'class="form-control validate[required]"');
			echo (empty($errors['description'])) ? '' : $errors['description'];
	echo'</div>
</div>';

// Bench count.
echo '<div class="form-group">
  		<label for="bench_count" id="bench_count" class="col-sm-2 control-label">Bench Count</label>
  		<div class="col-sm-10">';
	    	echo form::input('bench_count', $form['bench_count'], 'class="form-control"');
			echo (empty($errors['bench_count'])) ? '' : $errors['bench_count'];
	echo'</div>
</div>';

// Are stats for this competition recorded?
echo '<div class="form-group">
  		<label for="stats_recorded" id="stats_recorded" class="col-sm-2 control-label">Record Statistics?</label>
  		<div class="col-sm-10">';
	    	echo form::checkbox('stats_recorded', 1, (empty($selected['stats_recorded'])) ? '' : $selected['stats_recorded']);
			echo (empty($errors['stats_recorded'])) ? '' : $errors['stats_recorded'];
	echo'</div>
</div>';

// Submit.
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();
