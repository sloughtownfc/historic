<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Players Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit Player</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));
// Checkbox field fixes (checkbox $_POST hack).
// Since an unchecked box does not return anything to $_POST
// (i.e., gets set as null) it will not pass validation.
echo form::hidden('active', 0);
// Player first name.
echo '<div class="form-group">
  		<label for="first_name" id="first_name" class="col-sm-2 control-label">First Name</label>
  		<div class="col-sm-10">';
	    	echo form::input('first_name', $form['first_name'], 'class="form-control validate[required]"');
			echo (empty($errors['first_name'])) ? '' : $errors['first_name'];
	echo'</div>
</div>';

// Player last name.
echo '<div class="form-group">
  		<label for="last_name" id="last_name" class="col-sm-2 control-label">Last Name</label>
  		<div class="col-sm-10">';
	    	echo form::input('last_name', $form['last_name'], 'class="form-control validate[required]"');
			echo (empty($errors['last_name'])) ? '' : $errors['last_name'];
	echo'</div>
</div>';

// Player date of birth.
echo '<div class="form-group">
  		<label for="dob" id="dob" class="col-sm-2 control-label">Date of Birth <small>(leave blank if unknown)</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('dob', $form['dob'], 'class="form-control datepicker"');
			echo (empty($errors['dob'])) ? '' : $errors['dob'];
	echo'</div>
</div>';

// Player possition.
echo '<div class="form-group">
  		<label for="position_id" id="position_id" class="col-sm-2 control-label">Position</label>
  		<div class="col-sm-10">';
	    	echo form::dropdown('position_id', $form['position_id'], (empty($selected['position_id'])) ? '' : $selected['position_id'], 'class="form-control"');
			echo (empty($errors['position_id'])) ? '' : $errors['position_id'];
	echo'</div>
</div>';

// Player description.
echo '<div class="form-group">
  		<label for="description" id="description" class="col-sm-2 control-label">Description</label>
  		<div class="col-sm-10">';
		echo form::textarea('description', $form['description'], 'class="form-control big validate[required]"');
		echo (empty($errors['description'])) ? '' : $errors['description'];
	echo'</div>
</div>';

// Player description.
echo '<div class="form-group">
  		<label for="achievements" id="achievements" class="col-sm-2 control-label">Achievements</label>
  		<div class="col-sm-10">';
		echo form::textarea('achievements', $form['achievements'], 'class="form-control"');
		echo (empty($errors['achievements'])) ? '' : $errors['achievements'];
	echo'</div>
</div>';


// Player picture URL.
echo '<div class="form-group">
  		<label for="picture_url" id="picture_url" class="col-sm-2 control-label">Picture URL <small class="gallery">Click to select image</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('picture_url', $form['picture_url'], 'class="form-control"');
			echo (empty($errors['picture_url'])) ? '' : $errors['picture_url'];
	echo'</div>
</div>';

// Is this player active? (on the current season's squadlist)
echo '<div class="form-group">
  		<label for="active" id="active" class="col-sm-2 control-label">Player active?</label>
  		<div class="col-sm-10">';
	    	echo form::checkbox('active', 1, (empty($selected['active'])) ? '' : $selected['active']);
			echo (empty($errors['active'])) ? '' : $errors['active'];
	echo'</div>
</div>';

// Submit.
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();
