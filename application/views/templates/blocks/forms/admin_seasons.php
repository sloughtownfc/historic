<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Seasons Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit Season</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));
// Season name.
echo '<div class="form-group">
  		<label for="name" id="name" class="col-sm-2 control-label">Season Name</label>
  		<div class="col-sm-10">';
	    	echo form::input('name', $form['name'], 'class="form-control validate[required]"');
			echo (empty($errors['name'])) ? '' : $errors['name'];
	echo'</div>
</div>';

// Season start date.
echo '<div class="form-group">
  		<label for="start_date" id="start_date" class="col-sm-2 control-label">Season Start Date</label>
  		<div class="col-sm-10">';
	    	echo form::input('start_date', $form['start_date'], 'class="form-control datepicker validate[required,custom[date]]"');
			echo (empty($errors['start_date'])) ? '' : $errors['start_date'];
	echo'</div>
</div>';

// Season end date.
echo '<div class="form-group">
  		<label for="end_date" id="end_date" class="col-sm-2 control-label">Season End Date <small>(Optional)</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('end_date', $form['end_date'], 'class="form-control datepicker"');
			echo (empty($errors['end_date'])) ? '' : $errors['end_date'];
	echo'</div>
</div>';

// Submit.
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';

// Close form.
echo form::close();
