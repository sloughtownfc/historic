<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin News Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit A Trust News Story</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));
// News story author.
echo '<div class="form-group">
  		<label for="author" id="author" class="col-sm-2 control-label">Author</label>
  		<div class="col-sm-10">';
	    	echo form::input('author', $form['author'], 'class="form-control validate[required]"');
			echo (empty($errors['author'])) ? '' : $errors['author'];
	echo'</div>
</div>';

// News headline.
echo '<div class="form-group">
  		<label for="headline" id="headline" class="col-sm-2 control-label">Headline</label>
  		<div class="col-sm-10">';
	    	echo form::input('headline', $form['headline'], 'class="form-control validate[required]"');
			echo (empty($errors['headline'])) ? '' : $errors['headline'];
	echo'</div>
</div>';

// News Publish Date.
echo '<div class="form-group">
  		<label for="publish_date" id="publish_date" class="col-sm-2 control-label">Publish Date</label>
  		<div class="col-sm-10">';
	    	echo form::input('publish_date', $form['publish_date'], 'class="form-control datepicker validate[required]"');
			echo (empty($errors['publish_date'])) ? '' : $errors['publish_date'];
	echo'</div>
</div>';

// Body.
echo '<div class="form-group">
  		<label for="body" id="body" class="col-sm-2 control-label">Body</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('body', $form['body'], 'class="form-control big validate[required]"');
			echo (empty($errors['body'])) ? '' : $errors['body'];
	echo'</div>
</div>';

// Submit
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();
