<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Opposition Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit Opposition</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));
// Opposition team name.
echo '<div class="form-group">
  		<label for="name" id="name" class="col-sm-2 control-label">Team Name</label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'name', 'maxlength' => '64'), $form['name'], 'class="form-control validate[required]" value="#5367ce"');
			echo (empty($errors['name'])) ? '' : $errors['name'];
	echo'</div>
</div>';

// Opposition home kit background colour.
echo '<div class="form-group">
  		<label for="colour_home_background" id="colour_home_background" class="col-sm-2 control-label">Home Kit Background Colour</label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'colour_home_background', 'maxlength' => '7'), $form['colour_home_background'], 'class="form-control colorpicker validate[required]"');
			echo (empty($errors['colour_home_background'])) ? '' : $errors['colour_home_background'];
	echo'</div>
</div>';

// Opposition home kit foreground colour.
echo '<div class="form-group">
  		<label for="colour_home_foreground" id="colour_home_foreground" class="col-sm-2 control-label">Home Kit Foreground Colour</label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'colour_home_foreground', 'maxlength' => '7'), $form['colour_home_foreground'], 'class="form-control colorpicker validate[required]"');
			echo (empty($errors['colour_home_foreground'])) ? '' : $errors['colour_home_foreground'];
	echo'</div>
</div>';

// Opposition away kit background colour.
echo '<div class="form-group">
  		<label for="colour_away_background" id="colour_away_background" class="col-sm-2 control-label">Away Kit Background Colour</label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'colour_away_background', 'maxlength' => '7'), $form['colour_away_background'], 'class="form-control colorpicker validate[required]"');
			echo (empty($errors['colour_away_background'])) ? '' : $errors['colour_away_background'];
	echo'</div>
</div>';

// Opposition away kit foreground colour.
echo '<div class="form-group">
  		<label for="colour_away_foreground" id="colour_away_foreground" class="col-sm-2 control-label">Away Kit Foreground Colour</label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'colour_away_foreground', 'maxlength' => '7'), $form['colour_away_foreground'], 'class="form-control colorpicker validate[required]"');
			echo (empty($errors['colour_away_foreground'])) ? '' : $errors['colour_away_foreground'];
	echo'</div>
</div>';

// Opposition team logo.
echo '<div class="form-group">
  		<label for="logo" id="logo" class="col-sm-2 control-label">Team Logo <small class="gallery">Click to select logo</small></label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'logo', 'maxlength' => '128'), $form['logo'], 'class="form-control validate[required]"');
			echo (empty($errors['logo'])) ? '' : $errors['logo'];
	echo'</div>
</div>';

// Opposition team website.
echo '<div class="form-group">
  		<label for="website" id="website" class="col-sm-2 control-label">Team Website <small>(optional)</small></label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'website', 'maxlength' => '128'), $form['website'], 'class="form-control"');
			echo (empty($errors['website'])) ? '' : $errors['website'];
	echo'</div>
</div>';

// Opposition grounds venue (name of).
echo '<div class="form-group">
  		<label for="grounds_venue" id="grounds_venue" class="col-sm-2 control-label">Grounds Name</label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'grounds_venue', 'maxlength' => '128'), $form['grounds_venue'], 'class="form-control validate[required]"');
			echo (empty($errors['grounds_venue'])) ? '' : $errors['grounds_venue'];
	echo'</div>
</div>';

// Opposition grounds address.
echo '<div class="form-group">
  		<label for="grounds_address" id="grounds_address" class="col-sm-2 control-label">Grounds Address</label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'grounds_address', 'maxlength' => '128'), $form['grounds_address'], 'class="form-control validate[required]"');
			echo (empty($errors['grounds_address'])) ? '' : $errors['grounds_address'];
	echo'</div>
</div>';

// Opposition grounds telephone number.
echo '<div class="form-group">
  		<label for="grounds_telephone" id="grounds_telephone" class="col-sm-2 control-label">Telephone Number</label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'grounds_telephone', 'maxlength' => '11'), $form['grounds_telephone'], 'class="form-control"');
			echo (empty($errors['grounds_telephone'])) ? '' : $errors['grounds_telephone'];
	echo'</div>
</div>';

// Opposition grounds directions.
echo '<div class="form-group">
  		<label for="grounds_directions" id="grounds_directions" class="col-sm-2 control-label">Directions to Ground</label>
  		<div class="col-sm-10">';
	    	echo form::textarea(array('name' => 'grounds_directions'), $form['grounds_directions'], 'class="form-control big validate[required]"');
			echo (empty($errors['grounds_directions'])) ? '' : $errors['grounds_directions'];
	echo'</div>
</div>';

// Opposition grounds google data.
echo '<div class="form-group">
  		<label for="grounds_google" id="grounds_google" class="col-sm-2 control-label">Google Data</label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'grounds_google'), $form['grounds_google'], 'class="form-control validate[required]"');
			echo (empty($errors['grounds_google'])) ? '' : $errors['grounds_google'];
	echo'</div>
</div>';

// Opposition notes.
echo '<div class="form-group">
  		<label for="notes" id="notes" class="col-sm-2 control-label">Notes</label>
  		<div class="col-sm-10">';
	    	echo form::textarea(array('name' => 'notes'), $form['notes'], 'class="form-control"');
			echo (empty($errors['notes'])) ? '' : $errors['notes'];
	echo'</div>
</div>';

// Submit.
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();
