<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Stats Form view
 */

// Generate booking codes
$booking_codes = array
(
	0 => 'No',
	1 => 'Yellow',
	2 => 'Red'
);

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit Statistics</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal'));

// Add table header
?>
<div class="table-responsive" role="form">
  <table class="table stats">
	<thead>
		<tr>
			<th>Shirt</th>
			<th>Name</th>
			<th>Goals</th>
			<th>Booked?</th>
			<th>On / Off for</th>
			<th>Made an appearance?</th>
			<th>Injured?</th>
		</tr>
	</thead>
<tbody>
<?php
// Generate row fields
for ($i = 0; $i < $row_count; $i++)
{
	// Checkbox field fixes (checkbox $_POST hack).
	// Since an unchecked box does not return anything to $_POST
	// (i.e., gets set as null) it will not pass validation.
	echo form::hidden("appearance[$i]", 0);
	echo form::hidden("injured[$i]", 0);
	echo '<tr>';
	// Shirt
	echo '<td>';
	echo form::input("shirt[$i]", empty($form[$i]['shirt']) ? '' : $form[$i]['shirt'], 'class="form-control text-center"');
	echo (empty($errors[$i]['shirt'])) ? '' : $errors[$i]['shirt'];
	echo '</td>';
	// Player
	echo '<td>';
	echo form::dropdown("player_id[$i]", $player_ids, (empty($selected[$i]['player_id'])) ? '' : $selected[$i]['player_id'], 'class="form-control"');
	echo (empty($errors[$i]['player_id'])) ? '' : $errors[$i]['player_id'];
	echo '</td>';
	// Goals
	echo '<td>';
	echo form::input("goals[$i]", empty($form[$i]['goals']) ? '' : $form[$i]['goals'], 'class="form-control text-center"');
	echo (empty($errors[$i]['goals'])) ? '' : $errors[$i]['goals'];
	echo '</td>';
	// Bookings
	echo '<td>';
	echo form::dropdown("bookings[$i]", $booking_codes, (empty($selected[$i]['bookings'])) ? '0' : $selected[$i]['bookings'], 'class="form-control"');
	echo (empty($errors[$i]['bookings'])) ? '' : $errors[$i]['bookings'];
	echo '</td>';
	// Sub Shirt (on/off for)
	echo '<td>';
	echo form::input("sub_shirt[$i]", empty($form[$i]['sub_shirt']) ? '' : $form[$i]['sub_shirt'], 'class="form-control text-center"');
	echo (empty($errors[$i]['sub_shirt'])) ? '' : $errors[$i]['sub_shirt'];
	echo '</td>';
	// Made an appearance?
	echo '<td>';
	echo form::checkbox("appearance[$i]", 1, (empty($selected[$i]['appearance']) ? (($i < 11) ? 1 : 0)  : $selected[$i]['appearance']));
	echo (empty($errors[$i]['appearance'])) ? '' : $errors[$i]['appearance'];
	echo '</td>';
	// Injured?
	echo '<td>';
	echo form::checkbox("injured[$i]", 1, (empty($selected[$i]['injured']) ? '' : $selected[$i]['injured']));
	echo (empty($errors[$i]['injured'])) ? '' : $errors[$i]['injured'];
	echo '</td>';
	echo '</tr>';
} ?>
</tbody>
</table>
</div>
<?php
// Submit.
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();
