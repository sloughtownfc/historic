<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Fixtures Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit Fixture</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal'));
// Checkbox field fixes (checkbox $_POST hack).
// Since an unchecked box does not return anything to $_POST
// (i.e., gets set as null) it will not pass validation.
echo form::hidden('is_home', 0);
echo form::hidden('away_alternate_kit', 0);
// Fixture match date.
echo '<div class="form-group">
  		<label for="match_date" id="match_date" class="col-sm-2 control-label">Match Date <small>(Leave blank if TBC)</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('match_date', $form['match_date'], 'class="form-control datepicker"');
			echo (empty($errors['match_date'])) ? '' : $errors['match_date'];
	echo'</div>
</div>';

// Kickoff time.
echo '<div class="form-group">
  		<label for="kickoff" id="kickoff" class="col-sm-2 control-label">Kick Off <small>(Leave blank if TBC)</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('kickoff', $form['kickoff'], 'class="form-control timepicker"');
			echo (empty($errors['kickoff'])) ? '' : $errors['kickoff'];
	echo'</div>
</div>';

// Fixture competition.
echo '<div class="form-group">
  		<label for="competition_id" id="competition_id" class="col-sm-2 control-label">Competition</label>
  		<div class="col-sm-10">';
	    	echo form::dropdown('competition_id', $form['competition_id'], (empty($selected['competition_id'])) ? '' : $selected['competition_id'], 'class="form-control"');
			echo (empty($errors['competition_id'])) ? '' : $errors['competition_id'];
	echo'</div>
</div>';

// Fixture opposition.
echo '<div class="form-group">
  		<label for="opposition_id" id="opposition_id" class="col-sm-2 control-label">Opposition</label>
  		<div class="col-sm-10">';
	    	echo form::dropdown('opposition_id', $form['opposition_id'], (empty($selected['opposition_id'])) ? '' : $selected['opposition_id'], 'class="form-control"');
			echo (empty($errors['opposition_id'])) ? '' : $errors['opposition_id'];
	echo'</div>
</div>';

// Is game being played at home?
echo '<div class="form-group">
  		<label for="is_home" id="is_home" class="col-sm-2 control-label">Is a home game?</label>
  		<div class="col-sm-10">';
	    	echo form::checkbox('is_home', 1, (empty($selected['is_home'])) ? '' : $selected['is_home']);
			echo (empty($errors['is_home'])) ? '' : $errors['is_home'];
	echo'</div>
</div>';

// Is the away team wearing their alternate kit?
echo '<div class="form-group">
  		<label for="away_alternate_kit" id="away_alternate_kit" class="col-sm-2 control-label">In away kit?</label>
  		<div class="col-sm-10">';
	    	echo form::checkbox('away_alternate_kit', 1, (empty($selected['away_alternate_kit'])) ? '' : $selected['away_alternate_kit']);
			echo (empty($errors['away_alternate_kit'])) ? '' : $errors['away_alternate_kit'];
	echo'</div>
</div>';

// Match sponsor.
echo '<div class="form-group">
  		<label for="match_sponsor" id="match_sponsor" class="col-sm-2 control-label">Match Sponsor <small>(Optional)</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('match_sponsor', $form['match_sponsor'], 'class="form-control"');
			echo (empty($errors['match_sponsor'])) ? '' : $errors['match_sponsor'];
	echo'</div>
</div>';

// Ball sponsor.
echo '<div class="form-group">
  		<label for="ball_sponsor" id="ball_sponsor" class="col-sm-2 control-label">Ball Sponsor <small>(Optional)</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('ball_sponsor', $form['ball_sponsor'], 'class="form-control"');
			echo (empty($errors['ball_sponsor'])) ? '' : $errors['ball_sponsor'];
	echo'</div>
</div>';

// Submit.
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();
