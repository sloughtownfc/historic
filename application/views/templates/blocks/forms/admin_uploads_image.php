<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Image Uploads Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Upload an Image</h2>';

// Open multipart form.
echo form::open_multipart(NULL, array('class'=>'form-horizontal validation'));
// Print errors
foreach ($errors as $error)
{
	echo $error;
}
// Picture type
echo '<div class="form-group">
  		  <label for="radios" class="col-sm-2 control-label">Select image type</label>
  		<div class="col-sm-10"><div class="radio">';
        echo form::radio('type_1', 'front', TRUE);
		    echo form::label('type_1', 'Front Image (960x360)', 'style="float: none;"');
		echo '</div><div class="radio">';
		    echo form::radio('type_1', 'player', FALSE);
		    echo form::label('type_1', 'Player Image (200x200)', 'style="float: none;"');
		echo '</div><div class="radio">';
		    echo form::radio('type_1', 'badge', FALSE);
		    echo form::label('type_1', 'Opposition Badge Image (100x100)', 'style="float: none;"');
echo '</div></div></div>';

// Picture name
echo '<div class="form-group">
  		<label for="file_1" class="col-sm-2 control-label">Upload File</label>
  		<div class="col-sm-10">';
    		echo form::upload('file_1');
  	echo'</div>
</div>';

// Picture TagsInput
echo '<div class="form-group">
  		<label for="tags" id="tags" class="col-sm-2 control-label">Tag Images</label>
  		<div class="col-sm-10">';
    		echo form::input(array('id' => 'tags_1', 'class' => 'tags', 'name' => 'tags_1', 'type' => 'text'));
  	echo'</div>
</div>';

// Submit
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();
