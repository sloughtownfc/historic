<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Academy Reports Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit Report</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));
// Checkbox field fixes (checkbox $_POST hack).
// Since an unchecked box does not return anything to $_POST
// (i.e., gets set as null) it will not pass validation.
echo form::hidden('report_published', 0);
// Home score.
echo '<div class="form-group">
  		<label for="report_home_score" id="report_home_score" class="col-sm-2 control-label">Home Score</label>
  		<div class="col-sm-10">';
	    	echo form::input('report_home_score', $form['report_home_score'], 'class="form-control validate[required,custom[integer]]"');
			echo (empty($errors['report_home_score'])) ? '' : $errors['report_home_score'];
	echo'</div>
</div>';

// Away score.
echo '<div class="form-group">
  		<label for="report_away_score" id="report_away_score" class="col-sm-2 control-label">Away Score</label>
  		<div class="col-sm-10">';
	    	echo form::input('report_away_score', $form['report_away_score'], 'class="form-control validate[required,custom[integer]]"');
			echo (empty($errors['report_away_score'])) ? '' : $errors['report_away_score'];
	echo'</div>
</div>';

// Home scorers.
echo '<div class="form-group">
  		<label for="report_home_scorers" id="report_home_scorers" class="col-sm-2 control-label">Slough Scorers</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('report_home_scorers', $form['report_home_scorers'], 'class="form-control"');
			echo (empty($errors['report_home_scorers'])) ? '' : $errors['report_home_scorers'];
	echo'</div>
</div>';

// Headline.
echo '<div class="form-group">
  		<label for="report_headline" id="report_headline" class="col-sm-2 control-label">Headline</label>
  		<div class="col-sm-10">';
	    	echo form::input('report_headline', $form['report_headline'], 'class="form-control validate[required]"');
			echo (empty($errors['report_headline'])) ? '' : $errors['report_headline'];
	echo'</div>
</div>';

// Excerpt.
echo '<div class="form-group">
  		<label for="report_excerpt" id="report_excerpt" class="col-sm-2 control-label">Excerpt</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('report_excerpt', $form['report_excerpt'], 'class="form-control validate[required]"');
			echo (empty($errors['report_excerpt'])) ? '' : $errors['report_excerpt'];
	echo'</div>
</div>';

// Body.
echo '<div class="form-group">
  		<label for="report_body" id="report_body" class="col-sm-2 control-label">Body</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('report_body', $form['report_body'], 'class="form-control big validate[required]"');
			echo (empty($errors['report_body'])) ? '' : $errors['report_body'];
	echo'</div>
</div>';

// Picture URL.
echo '<div class="form-group">
  		<label for="picture_url" id="picture_url" class="col-sm-2 control-label">Picture URL <small class="gallery">Click to select image</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('picture_url', $form['picture_url'] , 'class="form-control validate[required]"');
			echo (empty($errors['picture_url'])) ? '' : $errors['picture_url'];
	echo'</div>
</div>';

// Man Of The Match.
echo '<div class="form-group">
  		<label for="report_motm" id="report_motm" class="col-sm-2 control-label">Man Of The Match</label>
  		<div class="col-sm-10">';
	    	echo form::input('report_motm', $form['report_motm'], 'class="form-control"');
			echo (empty($errors['report_motm'])) ? '' : $errors['report_motm'];
	echo'</div>
</div>';

// Team.
echo '<div class="form-group">
  		<label for="report_team" id="report_team" class="col-sm-2 control-label">Rebels Team</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('report_team', $form['report_team'], 'class="form-control"');
			echo (empty($errors['report_team'])) ? '' : $errors['report_team'];
	echo'</div>
</div>';

// Is this report published?
echo '<div class="form-group">
  		<label for="report_published" id="report_published" class="col-sm-2 control-label">Publish report?</label>
  		<div class="col-sm-10">';
	    	echo form::checkbox('report_published', 1, (empty($selected['report_published'])) ? '' : $selected['report_published']);
			echo (empty($errors['report_published'])) ? '' : $errors['report_published'];
	echo'</div>
</div>';

// Submit.
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();
