<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Search Oppositions filter view
 */

// Open form.
echo form::open(NULL, array('method' => 'get','class'=>'form-horizontal search', 'role'=>'form'));
echo '<div class="form-group">';
// Player name filter.
echo '<label for="name" class="col-sm-2 control-label">Search</label>';
echo '<div class="col-sm-8"><input type="text" class="form-control" id="name" name="name" ></div>';
// Submit.
echo '<div class="col-sm-2"><input type="submit" class="btn btn-primary" id="submit" name="submit" value="Search"></input></div>';
// Close form.
echo '</div>';
echo form::close();