<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Paper Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit A Media Link</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));

// News Publish Date.
echo '<div class="form-group">
  		<label for="publish_date" id="publish_date" class="col-sm-2 control-label">Publish</label>
  		<div class="col-sm-10">';
	    	echo form::input('publish_date', $form['publish_date'], 'class="form-control datepicker validate[required,custom[date]]"');
			echo (empty($errors['publish_date'])) ? '' : $errors['publish_date'];
	echo'</div>
</div>';

// News headline.
echo '<div class="form-group">
  		<label for="headline" id="headline" class="col-sm-2 control-label">Headline</label>
  		<div class="col-sm-10">';
	    	echo form::input('headline', $form['headline'], 'class="form-control validate[required]"');
			echo (empty($errors['headline'])) ? '' : $errors['headline'];
	echo'</div>
</div>';

// URL
echo '<div class="form-group">
  		<label for="url" id="url" class="col-sm-2 control-label">URL</label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'url', 'maxlength' => '255'), $form['url'], 'class="form-control validate[required]"');
			echo (empty($errors['url'])) ? '' : $errors['url'];
	echo'</div>
</div>';

// Submit
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();
