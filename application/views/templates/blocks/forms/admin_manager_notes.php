<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Manager Note Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit Manager Note</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));
// Checkbox field fixes (checkbox $_POST hack).
// Since an unchecked box does not return anything to $_POST
// (i.e., gets set as null) it will not pass validation.
echo form::hidden('report_published', 0);
// Manager Note story author.
echo '<div class="form-group">
  		<label for="manager_note_author" id="manager_note_author" class="col-sm-2 control-label">Author</label>
  		<div class="col-sm-10">';
	    	echo form::input('manager_note_author', $form['manager_note_author'], 'class="form-control validate[required]"');
			echo (empty($errors['manager_note_author'])) ? '' : $errors['manager_note_author'];
	echo'</div>
</div>';

// Manager Note headline.
echo '<div class="form-group">
  		<label for="manager_note_headline" id="manager_note_headline" class="col-sm-2 control-label">Headline</label>
  		<div class="col-sm-10">';
	    	echo form::input('manager_note_headline', $form['manager_note_headline'], 'class="form-control validate[required]"');
			echo (empty($errors['manager_note_headline'])) ? '' : $errors['manager_note_headline'];
	echo'</div>
</div>';

// Manager Note excerpt.
echo '<div class="form-group">
  		<label for="manager_note_excerpt" id="manager_note_excerpt" class="col-sm-2 control-label">Excerpt</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('manager_note_excerpt', $form['manager_note_excerpt'], 'class="form-control validate[required]"');
			echo (empty($errors['manager_note_excerpt'])) ? '' : $errors['manager_note_excerpt'];
	echo'</div>
</div>';

// Body.
echo '<div class="form-group">
  		<label for="manager_note_body" id="manager_note_body" class="col-sm-2 control-label">Body</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('manager_note_body', $form['manager_note_body'], 'class="form-control big validate[required]"');
			echo (empty($errors['manager_note_body'])) ? '' : $errors['manager_note_body'];
	echo'</div>
</div>';

// Manager Note picture URL.
echo '<div class="form-group">
  		<label for="manager_note_picture_url" id="manager_note_picture_url" class="col-sm-2 control-label">Picture URL <small class="gallery">Click to select image</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('manager_note_picture_url', $form['manager_note_picture_url'], 'class="form-control validate[required]"');
			echo (empty($errors['manager_note_picture_url'])) ? '' : $errors['manager_note_picture_url'];
	echo'</div>
</div>';

// Manager Note picture URL.
echo '<div class="form-group">
  		<label for="manager_note_picture_thumbnail_url" id="manager_note_picture_thumbnail_url" class="col-sm-2 control-label">Small Picture URL <small class="gallery">Click to select image</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('manager_note_picture_thumbnail_url', $form['manager_note_picture_thumbnail_url'], 'class="form-control validate[required]"');
			echo (empty($errors['manager_note_picture_thumbnail_url'])) ? '' : $errors['manager_note_picture_thumbnail_url'];
	echo'</div>
</div>';

// Manager Note caption.
echo '<div class="form-group">
  		<label for="manager_note_caption" id="manager_note_caption" class="col-sm-2 control-label">Caption</label>
  		<div class="col-sm-10">';
	    	echo form::input('manager_note_caption', $form['manager_note_caption'], 'class="form-control validate[required]"');
			echo (empty($errors['manager_note_caption'])) ? '' : $errors['manager_note_caption'];
	echo'</div>
</div>';

// Manager Note quote.
echo '<div class="form-group">
  		<label for="manager_note_quote" id="manager_note_quote" class="col-sm-2 control-label">Quote</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('manager_note_quote', $form['manager_note_quote'], 'class="form-control validate[required]"');
			echo (empty($errors['manager_note_quote'])) ? '' : $errors['manager_note_quote'];
	echo'</div>
</div>';

// Manager Note quote by.
echo '<div class="form-group">
  		<label for="manager_note_quote_credit" id="manager_note_quote_credit" class="col-sm-2 control-label">Quote By</label>
  		<div class="col-sm-10">';
	    	echo form::input('manager_note_quote_credit', $form['manager_note_quote_credit'], 'class="form-control validate[required]"');
			echo (empty($errors['manager_note_quote_credit'])) ? '' : $errors['manager_note_quote_credit'];
	echo'</div>
</div>';

// Is this report published?
echo '<div class="form-group">
  		<label for="manager_note_published" id="manager_note_published" class="col-sm-2 control-label">Publish note?</label>
  		<div class="col-sm-10">';
	    	echo form::checkbox('manager_note_published', 1, (empty($selected['manager_note_published'])) ? '' : $selected['manager_note_published']);
			echo (empty($errors['manager_note_published'])) ? '' : $errors['manager_note_published'];
	echo'</div>
</div>';

// Submit
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';
// Close form.
echo form::close();
