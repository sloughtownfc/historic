<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Reports Form view
 */

// Notification
if(isset($notification)) echo $notification;

// Start block
echo '<h2>Add/Edit Report</h2>';

// Open form.
echo form::open(NULL, array('class'=>'form-horizontal validation'));
// Checkbox field fixes (checkbox $_POST hack).
// Since an unchecked box does not return anything to $_POST
// (i.e., gets set as null) it will not pass validation.
echo form::hidden('report_published', 0);
// Picture URL.
echo '<div class="form-group">
  		<label for="report_picture_url" id="report_picture_url" class="col-sm-2 control-label">Picture URL <small class="gallery">Click to select image</small></label>
  		<div class="col-sm-10">';
	    	echo form::input('report_picture_url', $form['report_picture_url'], 'class="form-control validate[required]"');
			echo (empty($errors['report_picture_url'])) ? '' : $errors['report_picture_url'];
	echo'</div>
</div>';

// Home score.
echo '<div class="form-group">
  		<label for="report_home_score" id="report_home_score" class="col-sm-2 control-label">Home Score</label>
  		<div class="col-sm-10">';
	    	echo form::input('report_home_score', $form['report_home_score'], 'class="form-control validate[required,custom[integer]]"');
			echo (empty($errors['report_home_score'])) ? '' : $errors['report_home_score'];
	echo'</div>
</div>';

// Away score.
echo '<div class="form-group">
  		<label for="report_away_score" id="report_away_score" class="col-sm-2 control-label">Away Score</label>
  		<div class="col-sm-10">';
	    	echo form::input('report_away_score', $form['report_away_score'], 'class="form-control validate[required,custom[integer]]"');
			echo (empty($errors['report_away_score'])) ? '' : $errors['report_away_score'];
	echo'</div>
</div>';

// Home scorers.
echo '<div class="form-group">
  		<label for="report_home_scorers" id="report_home_scorers" class="col-sm-2 control-label">Home Scorers</label>
  		<div class="col-sm-10">';
		 	echo form::textarea('report_home_scorers', $form['report_home_scorers'], 'class="form-control"');
			echo (empty($errors['report_home_scorers'])) ? '' : $errors['report_home_scorers'];
	echo'</div>
</div>';

// Away scorers.
echo '<div class="form-group">
  		<label for="report_away_scorers" id="report_away_scorers" class="col-sm-2 control-label">Away Scorers</label>
  		<div class="col-sm-10">';
		 	echo form::textarea('report_away_scorers', $form['report_away_scorers'], 'class="form-control"');
			echo (empty($errors['report_away_scorers'])) ? '' : $errors['report_away_scorers'];
	echo'</div>
</div>';

// Extra time.
echo '<div class="form-group">
  		<label for="report_extra_time" id="report_extra_time" class="col-sm-2 control-label">Extra Time</label>
  		<div class="col-sm-10">';
	    	echo form::input('report_extra_time', $form['report_extra_time'], 'class="form-control"');
			echo (empty($errors['report_extra_time'])) ? '' : $errors['report_extra_time'];
	echo'</div>
</div>';

// Attendance.
echo '<div class="form-group">
  		<label for="report_attendance" id="report_attendance" class="col-sm-2 control-label">Attendance</label>
  		<div class="col-sm-10">';
	    	echo form::input('report_attendance', $form['report_attendance'], 'class="form-control validate[required]"');
			echo (empty($errors['report_attendance'])) ? '' : $errors['report_attendance'];
	echo'</div>
</div>';

// Headline.
echo '<div class="form-group">
  		<label for="report_headline" id="report_headline" class="col-sm-2 control-label">Headline</label>
  		<div class="col-sm-10">';
	    	echo form::input('report_headline', $form['report_headline'], 'class="form-control validate[required]"');
			echo (empty($errors['report_headline'])) ? '' : $errors['report_headline'];
	echo'</div>
</div>';

// Excerpt.
echo '<div class="form-group">
  		<label for="report_excerpt" id="report_excerpt" class="col-sm-2 control-label">Excerpt</label>
  		<div class="col-sm-10">';
		    echo form::textarea('report_excerpt', $form['report_excerpt'], 'class="form-control validate[required]"');
			echo (empty($errors['report_excerpt'])) ? '' : $errors['report_excerpt'];
	echo'</div>
</div>';

// Body.
echo '<div class="form-group">
  		<label for="report_body" id="report_body" class="col-sm-2 control-label">Body</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('report_body', $form['report_body'], 'class="form-control big validate[required]"');
			echo (empty($errors['report_body'])) ? '' : $errors['report_body'];
	echo'</div>
</div>';

// Man Of The Match.
echo '<div class="form-group">
  		<label for="report_mom" id="report_mom" class="col-sm-2 control-label">Man of the Match</label>
  		<div class="col-sm-10">';
	    	echo form::dropdown("report_mom", $player_ids, (empty($selected['report_mom'])) ? '' : $selected['report_mom'], 'class="form-control"');
			echo (empty($errors['report_mom'])) ? '' : $errors['report_mom'];
	echo'</div>
</div>';

// Friendly Team.
if (isset($friendlies_mode))
{
	echo '<div class="form-group">
  		<label for="report_friendly_team" id="report_friendly_team" class="col-sm-2 control-label">Rebels Team</label>
  		<div class="col-sm-10">';
			echo form::textarea('report_friendly_team', $form['report_friendly_team'], 'class="form-control"');
			echo (empty($errors['report_friendly_team'])) ? '' : $errors['report_friendly_team'];
	echo'</div>
</div>';
}

// Away Team.
echo '<div class="form-group">
  		<label for="report_away_team" id="report_away_team" class="col-sm-2 control-label">Opposition Team</label>
  		<div class="col-sm-10">';
	    	echo form::textarea('report_away_team', $form['report_away_team'], 'class="form-control"');
			echo (empty($errors['report_away_team'])) ? '' : $errors['report_away_team'];
	echo'</div>
</div>';

// Flickr URL
echo '<div class="form-group">
  		<label for="report_flickr_url" id="report_flickr_url" class="col-sm-2 control-label">Flickr URL</label>
  		<div class="col-sm-10">';
	    	echo form::input(array('name' => 'report_flickr_url', 'maxlength' => '128'), $form['report_flickr_url'], 'class="form-control"');
			echo (empty($errors['report_flickr_url'])) ? '' : $errors['report_flickr_url'];
	echo'</div>
</div>';

// Is this report published?
echo '<div class="form-group">
  		<label for="report_published" id="report_published" class="col-sm-2 control-label">Publish report?</label>
  		<div class="col-sm-10">';
	    	echo form::checkbox('report_published', 1, (empty($selected['report_published'])) ? '' : $selected['report_published']);
			echo (empty($errors['report_published'])) ? '' : $errors['report_published'];
	echo'</div>
</div>';

// Submit.
echo '<div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">';
          echo form::submit('submit', 'Save', 'class="btn btn-primary"');
	echo '</div>
</div>';;
// Close form.
echo form::close();
