<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

	<?php if(isset($notification)) echo $notification; ?>

      <h2>First XI</h2>

      <div class="row">
        <div class="col-md-4">

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Fixtures</h3>
            </div>
            <div class="panel-body">
               <a href="/admin/fixtures/add" class="clearfix"><span class="glyphicon glyphicon-plus"></span>Add a fixture</a>
               <a href="/admin/fixtures/search" class="clearfix"><span class="glyphicon glyphicon-search"></span>Search a fixture</a>
            </div>
          </div>

        </div>
        <div class="col-md-4">

            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">News</h3>
              </div>
              <div class="panel-body">
                 <a href="/admin/news/add" class="clearfix"><span class="glyphicon glyphicon-plus"></span>Add a news item</a>
                 <a href="/admin/news/search" class="clearfix"><span class="glyphicon glyphicon-search"></span>Search for news</a>
              </div>
            </div>

        </div>
        <div class="col-md-4">

          <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Uploads</h3>
              </div>
              <div class="panel-body">
                 <a href="/admin/uploads/add" class="clearfix"><span class="glyphicon glyphicon-cloud-upload"></span>Upload a picture</a>
              </div>
            </div>

        </div>
      </div>

<div class="row">
        <div class="col-md-4">

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Players and Staff</h3>
            </div>
            <div class="panel-body">
               <a href="/admin/players/add" class="clearfix"><span class="glyphicon glyphicon-plus"></span>Add a player</a>
               <a href="/admin/players/search" class="clearfix"><span class="glyphicon glyphicon-search"></span>Search for past players</a>
            </div>
          </div>

        </div>
        <div class="col-md-4">

            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">In The Media</h3>
              </div>
              <div class="panel-body">
                 <a href="/admin/papers/add" class="clearfix"><span class="glyphicon glyphicon-plus"></span>Add a new link</a>
                 <a href="/admin/papers/delete" class="clearfix"><span class="glyphicon glyphicon-search"></span>Search for a link</a>
              </div>
            </div>

        </div>
        <div class="col-md-4">

          <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Opposition</h3>
              </div>
              <div class="panel-body">
                 <a href="/admin/oppositions/add" class="clearfix"><span class="glyphicon glyphicon-plus"></span>Add a new opposition</a>
                 <a href="/admin/oppositions/search" class="clearfix"><span class="glyphicon glyphicon-search"></span>Search for opposition</a>
              </div>
            </div>

        </div>
      </div>

      <div class="row">
        <div class="col-md-4">

          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Competitions</h3>
            </div>
            <div class="panel-body">
               <a href="/admin/competitions/add" class="clearfix"><span class="glyphicon glyphicon-plus"></span>Add a new Competition</a>
               <a href="/admin/competitions/search" class="clearfix"><span class="glyphicon glyphicon-search"></span>Search Competitions</a>
            </div>
          </div>

        </div>
        <div class="col-md-4">

            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Seasons</h3>
              </div>
              <div class="panel-body">
                 <a href="/admin/seasons/add" class="clearfix"><span class="glyphicon glyphicon-plus"></span>Add a new season</a>
                 <a href="/admin/seasons/search" class="clearfix"><span class="glyphicon glyphicon-search"></span>Search for seasons</a>
              </div>
            </div>

        </div>
      </div>
     

      <h2>Reserves</h2>

      <div class="row">
        <div class="col-md-4">

         <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Fixtures</h3>
            </div>
            <div class="panel-body">
               <a href="/admin/reserve_fixtures/add" class="clearfix"><span class="glyphicon glyphicon-plus"></span>Add a fixture</a>
               <a href="/admin/reserve_fixtures/search" class="clearfix"><span class="glyphicon glyphicon-search"></span>Search a fixture</a>
            </div>
          </div>

        </div>
      </div>

      <h2>U18s</h2>

     <div class="row">
        <div class="col-md-4">

         <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Fixtures</h3>
            </div>
            <div class="panel-body">
               <a href="/admin/u18s_fixtures/add" class="clearfix"><span class="glyphicon glyphicon-plus"></span>Add a fixture</a>
               <a href="/admin/u18s_fixtures/search" class="clearfix"><span class="glyphicon glyphicon-search"></span>Search a fixture</a>
            </div>
          </div>

        </div>
      </div>

      <h2>Academy</h2>

      <div class="row">
        <div class="col-md-4">

         <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Fixtures</h3>
            </div>
            <div class="panel-body">
               <a href="/admin/academy_fixtures/add" class="clearfix"><span class="glyphicon glyphicon-plus"></span>Add a fixture</a>
               <a href="/admin/academy_fixtures/search" class="clearfix"><span class="glyphicon glyphicon-search"></span>Search a fixture</a>
            </div>
          </div>

        </div>
      </div>

      
      <h2>Trust</h2>

      <div class="row">
        <div class="col-md-4">

         <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">News</h3>
            </div>
            <div class="panel-body">
               <a href="/admin/trust/add" class="clearfix"><span class="glyphicon glyphicon-plus"></span>Add a news item</a>
               <a href="/admin/trust/search" class="clearfix"><span class="glyphicon glyphicon-search"></span>Search for news</a>
            </div>
          </div>

        </div>
      </div>