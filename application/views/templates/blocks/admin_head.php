<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="All | NoIndex | NoFollow | None" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Slough Town FC: Content Management System</title>

	<link rel="shortcut icon" href="/images/favicon.png" />

	<!-- Bootstrap -->
    <link href="/css/admin/bootstrap.min.css" rel="stylesheet">
    <link href="/css/admin/main.css" rel="stylesheet">

    <link rel="stylesheet" href="/css/admin/datepicker.css" />
	<link rel="stylesheet" href="/css/admin/colorpicker.css" />
    <link rel="stylesheet" href="/css/admin/timepicker.css" />
	<link rel="stylesheet" href="/css/admin/tagsinput.css" />
    <link rel="stylesheet" href="/css/admin/validationEngine.jquery.css" />

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->