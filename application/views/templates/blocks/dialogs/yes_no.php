<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Yes / No Dialog view
 */
// Show prompt.
echo "<h2>Delete?</h2> <p>$prompt</p>";
// Open form.
echo form::open(NULL, array('class'=>'form-horizontal'));

echo'
	<div class="form-group">
	  <div class="col-sm-offset-2 col-sm-10">';
	  echo form::submit('yes', 'Yes', 'class="btn btn-danger"');
	  echo form::submit('no', 'No', 'class="btn btn-success"');
echo'</div></div>';

// Close form.
echo form::close();
