<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Seasons List view
 */
// Open list.
echo "<ul>\n";
foreach ($items as $item)
{
	echo "<li>$item</li>";
}
// Close list.
echo '</ul>';
// Show pagination.
echo $pagination;