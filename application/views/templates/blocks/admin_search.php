<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Search view
 */

// Notification
if(isset($notification)) echo $notification;


echo'<div class="row actions">
    <div class="col-sm-5">
      <ul class="list-inline">
        <li><a href="/admin"><span class="glyphicon glyphicon-chevron-left"></span>Go Back</a></li>
        <li><a href="'.$root_controller_url.'/add"><span class="glyphicon glyphicon-plus"></span>Add New</a></li>
      </ul>
    </div>
    <div class="col-sm-7 hidden-xs">
      <ul class="list-inline pull-right">
        <li><span class="glyphicon glyphicon-pencil"></span>Edit</li>
        <li><span class="glyphicon glyphicon-remove"></span>Delete</li>
        <li><span class="glyphicon glyphicon-file"></span>Manager Note</li>
        <li><span class="glyphicon glyphicon-film"></span>Video</li>
        <li><span class="glyphicon glyphicon-map-marker"></span>Coach</li>
        <li><span class="glyphicon glyphicon-stats"></span>Stats</li>
        <li><span class="glyphicon glyphicon-folder-open"></span>Match Report</li>
      </ul>
    </div></div>';

// Show filter form
if (isset($filter_form))
{
	echo $filter_form;
}


// Open table
echo '<div class="table-responsive">
          <table class="table fixture-search">';

// Add column headers
echo '<thead><tr>';
foreach ($column_titles as $title)
{
	echo "<th>$title</th>";
}
echo '<th>Actions</th>';
echo '</tr></thead>';

echo '<tbody>';
// Print rows
foreach ($rows as $key => $row)
{
	// Open row
	echo '<tr>';

	// Print row columns (fields)
	foreach ($row as $column)
	{
		if (is_object($column))
		{
			$field = $column->name;
		}
		else
		{
			$field = $column;
		}
		echo '<td>' . $field . '</td>';
	}

	// Print row item links
	echo '<td>';
	foreach ($links[$key] as $title => $link)
	{
		echo html::anchor("$root_controller_url/$link", $title) . ' ';
	}
	echo '</td>';

	// Close row.
	echo '</tr>';
}
echo '</tbody>';

// Close table.
echo '</table></div>';

// Show pagination.
echo $pagination;
