<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>STFC Picture Picker</title>
<link rel="stylesheet" href="/css/reset-min.css" type="text/css" />
<link rel="stylesheet" href="/css/gallerifficPlus.css" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="/js/gallerifficPlus.js"></script>
<script type="text/javascript">
	document.write("<style type='text/css'>div.navigation{width:300px;float: left;}div.content{display:block;}</style>");
</script>
</head>
<body>

<div id="page">
        <div id="container">
            <h1>STFC Picture Picker</h1>
            <?php

            echo form::open();
				echo form::label('search', 'Search');
				echo form::input('search');
				echo form::submit('submit', 'Search');
            echo form::close();

            ?>

<!-- Start Gallery Html Containers -->

<div id="gallery" class="content">
    <div id="controls" class="controls"></div>
    <div id="slideshow" class="slideshow"></div>
    <div id="details" class="embox">
        <div id="download" class="download">
        	<a class="return_picture" id="picture_url">Select Image (Original)</a>
        	<a class="return_picture" id="m_picture_url">Select Image (Medium)</a>
        </div>
        <div id="image-title" class="image-title"></div>
        <div id="image-desc" class="image-desc"></div>
    </div>
</div>

<div id="navigation" class="navigation">
    <ul class="thumbs noscript">
<?php
foreach ($gallery_images as $image)
{
?>
		<li><a href="<?php echo "/$root_url" . $image->preview; ?>" medium="<?php echo "/$root_url" . $image->preview; ?>" original="<?php echo "/$root_url" . $image->filename; ?>"><img src="<?php echo "/$root_url" . $image->thumbnail; ?>" /></a></li>
<?php
}
?>
	</ul>
    </div>
        <div style="clear: both;"></div>
    </div>
</div>
<!-- End Gallery Html Containers -->

<script type="text/javascript">
$(document).ready(function() {
	var gallery = $('#gallery').galleriffic('#navigation', {
		delay:                2000,
		numThumbs:            16,
		imageContainerSel:    '#slideshow',
		controlsContainerSel: '#controls',
		titleContainerSel:    '#image-title',
		descContainerSel:     '#image-desc',
		downloadLinkSel:      '#picture_url',
		m_downloadLinkSel:     '#m_picture_url',
		fixedNavigation:	   true,
		galleryKeyboardNav:	   true,
		autoPlay:			   false
	});

	gallery.onFadeOut = function() {
		$('#details').fadeOut('fast');
	};

	gallery.onFadeIn = function() {
		$('#details').fadeIn('fast');
	};
});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.return_picture').click( function() {
			retVal = $(this).attr("href");
			$(opener.document).contents().find("<?php echo '#' . $return_el; ?>").val(retVal);
			opener.focus();
			self.close();
		});
	});
</script>
</body>
</html>