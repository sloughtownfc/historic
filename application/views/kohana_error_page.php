<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="All | NoIndex | NoFollow | None" />
	<!--<meta name="viewport" content="width = device-width" />-->
	<meta name="keywords" content="Slough Town FC, Slough Town Football Club, Slough Town, Rebels, Slough FC, Football, Southern League, Southern Central League, Non League football, Holloways Park, STFC, Slough, sloughtownfc.net" />
	<meta name="description" content="The Official Website for Slough Town Football Club" />
	<meta name="google-site-verification" content="NQXbM_LJ1twsLLEnAFsx2vPg-zHyNG5Qy_TW1_c9cRo" />
	<link rel="shortcut icon" href="/images/favicon.png" />

	<link rel="apple-touch-icon" href="apple-touch-icon.png" />
	<meta http-equiv="content-canguage" content="EN" />
	<meta name="copyright" content="Slough Town FC" />
	<meta name="revisit-after" content="14 days" />
	<meta name="distribution" content="global" />
	<meta name="resource-type" content="document" />

	<title>SloughTownFC.net - The Official Website of Slough Town FC </title>

	<link rel="stylesheet" type="text/css" href="/css/reset-min.css" />
	<link rel="stylesheet" type="text/css" href="/css/main.css" />
	<!--<link rel="stylesheet" type="text/css" media="only screen and (max-device-width: 480px)" href="css/mobile.css" />-->
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAA3uBevvhZk4yrjORqZxvDjBRubxwQCYD3BNJhsPro5KqUKWn0LhTKRuLlzu6C4gXcfusj6NQR0q6kKg"type="text/javascript"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
	<script type='text/javascript' src='/js/jquery.aviaSlider.js'></script>
	<script type="text/javascript" src="/js/ui_main.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		$('#frontpage-slider').aviaSlider( { autorotationSpeed:5 } );
	});
	</script>

	<!-- Tabs -->
	<script type="text/javascript">
	$(document).ready(function(){
		$('#tabs > ul').tabs({ fx: { height: 'toggle', opacity: 'toggle' } });
	});
	</script>

	<!-- Toggle -->
	<script type="text/javascript">
	$(document).ready(function(){

		$(".toggle_container").hide();

		$("a.trigger").toggle(function(){
			$(this).addClass("active");
			}, function () {
			$(this).removeClass("active");
		});

		$("a.trigger").click(function(){
			$(this).next(".toggle_container").slideToggle("slow");
		});

	});
	</script>

    <!-- Clear/Focus -->
	<script type="text/javascript">
    $(document).ready(function(){
        var clearMePrevious = '';

        // clear input on focus
        $('.clearMeFocus').focus(function()
        {
            if($(this).val()==$(this).attr('title'))
            {
                clearMePrevious = $(this).val();
                $(this).val('');
            }
        });
        // if field is empty afterward, add text again
        $('.clearMeFocus').blur(function()
        {
            if($(this).val()=='')
            {
                $(this).val(clearMePrevious);
            }
        });

        $("textarea").focus(function() {

            if( $(this).text() == "Your enquiry" ) {
                $(this).text("");
            }

        });

        $("textarea").blur(function() {

            if( $(this).text() == "" ) {
                $(this).text("Your enquiry");
            }

        });

    });
    </script>
    
    
    <!-- Analytics -->
    <script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-2349973-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
</head>
<body onload="initialize()" onunload="GUnload()">
	<div id="container">
		<div id="header_container">
	    	<div id="header">

			<div id="menu_container">
		    	<div class="menu">
				<ul>
                	<li><a href="/">Home</a></li>
			    	<li><a href="/">Latest</a>
					<ul>
					    <li><a href="/news">News</a></li>

						<li><a href="/fixtures">Fixtures &amp; Reports</a></li>
						<li><a href="/league-table">League Table</a></li>
						<li><a href="/trust">Trust News</a></li>
						<li><a href="/manager-notes">Manager Notes</a></li>
						<li><a href="http://www.flickr.com/photos/horshamrebel/collections/" onclick="window.open(this.href); return false;">Match Galleries</a></li>

						<li><a href="/videos">Video Clips</a></li>
				    </ul>
				</li>
				<li><a href="/">First Team</a>
					<ul>
						<li><a href="/current-players">Squad Profiles</a></li>
						<li><a href="/player-statistics">Player Statistics</a></li>

						<li><a href="/player-database">Player Database</a></li>
                        <li><a href="/opposition-database">Opposition Database</a></li>
				    </ul>
				</li>
				<li><a href="/">The Club</a>
					<ul>
					    <li><a href="/contacts">STFC Contacts</a></li>

						<li><a href="/how-to-find-us">How To Find Us?</a></li>
					    <li><a href="/honours">Club Honours</a></li>
						<li><a href="/links">External Links</a></li>
						<li><a href="/messageboards">Messageboards</a></li>
						<li><a href="/away-directions">Away Directions</a></li>
						<li><a href="/supporters-coach">Supporters Coach</a></li>

						<li><a href="/shop">Online Clubshop</a></li>
				    </ul>
				</li>
				<li><a href="/">Sponsorship</a>
					<ul>
				    	<li><a href="/sponsorship-opportunities">Matchday Packages</a></li>
						<li><a href="/partners">Become a Partner</a></li>

						<li><a href="/500_club">500 Club</a></li>
				    </ul>
				</li>
				<li><a href="/">Under 18s</a>
					<ul>
				    	<li><a href="/u18-fixtures">Fixtures &amp; Reports</a></li>

						<li><a href="http://full-time.thefa.com/Table.do?divisionseason=4212881" onclick="window.open(this.href); return false;">League Table</a></li>
				    </ul>
				</li>
                <!--<li><a href="/stfc-youth">STFC Youth</a></li>-->
			    </ul>
			</div>

			<!--<div class="rss">
				<a href="#">News</a>
			    <a href="#">Reports</a>
			    <a href="/">Home</a>
			</div>-->
		    </div>

		</div>
	    </div>
	    <div id="main">
			<div id="content">

                <div id="content_area">
                
                   	<h1><?php echo html::specialchars($error) ?></h1>
                    
                    <p><?php echo html::specialchars($description) ?></p>
                    
                    <p><a href="/">Click here</a> to return to the home page.</p>
                
                </div><!--End Content Area -->

     		<br class="clear" />

            </div>
        </div>
	</div>
	<div id="footer_container">
	<div id="footer">
		<div id="credit">
	    	<p>+ Quick Links: <a href="/news">Latest News</a> | <a href="/fixtures">Latest Reports</a> | <a href="/messageboards">Forums</a> | <a href="/contacts">Contact Us</a> | <a href="/away-directions">Directions</a> | <a href="/partners">Sponsorship</a> | <a href="/shop">Shop</a></p>

	       	<p>+ Copyright Slough Town FC 2011 | Pictures: <a href="http://www.horshamrebel.co.uk/" onclick="window.open(this.href); return false;">Gary House</a> | Developed by
: <a href="http://www.oliverhuish.com" onclick="window.open(this.href); return false;">Oliver Huish</a> &amp; <a href="http://www.tomgathern.me" onclick="window.open(this.href); return false;">Tom Gathern</a></p>
	    </div>
		<div id="scarf"></div>
	</div>
    </div>

</body>
</html>