<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Workflow controller
 */
abstract class Admin_Workflow_Controller extends Admin_Controller {

	// Set form field default values.
	protected $_form_defaults = array();

	// Set errors for form fields.
	protected $_form_errors = array();

	// Set selected form field values.
	protected $_form_selected = array();

	// Set form field values.
	protected $_form_values = array();

	// Set the workflow name (plural).
	protected $_workflow = '';

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin';

	// Set the value to display for missing columns.
	protected $_search_missing_column = '--';

	// Set the column and direction to order by in a search.
	protected $_search_orderby = array
	(
		// 'Column Name' => 'Order Direction'
	);

	// Set the row item links.
	protected $_search_row_links = array
	(
		'Edit'   => 'edit/%id%',
		'Delete' => 'delete/%id%'
	);

	// Set the columns to return in a search, in the order to display them.
	protected $_search_titles = array
	(
		// 'Column Name' => 'Column Title'
	);

	// Abstract method definitions.
	abstract public function view($id);

	public function __construct()
	{
		parent::__construct();

		// Duplicate form to errors and selected for DRY
		$this->_form_errors = $this->_form_selected = $this->_form_values;
	}

	/**
	 * Add a workflow dataset.
	 */
	public function add()
	{
		// Set form and selected defaults
		$this->_form_selected = $this->_form_values = arr::overwrite($this->_form_values, $this->_form_defaults);

		// Check for $_POST data
		if ($this->input->post())
		{
			// Fetch $_POST data into array
			$data = $this->input->post();
			// Create new ORM model
			$model = ORM::factory($this->_singular());
			// Process the input form data
			if ($this->_process_form_data($data, $model))
			{
				// Flash success notification
				$this->_set_notification
				(
					$this->_singular(TRUE) . ' added.',
					'success',
					TRUE
				);
				// Redirect to root location
				$this->_redirect_to_root();
			}
			else
			{
				// Set error notification
				$this->_set_notification
				(
					'Unable to add ' . $this->_singular(TRUE),
					'error'
				);
			}
		}

		// Load form body block, replaces default
		$this->_load_block('body', 'forms/admin_' . $this->_workflow);
		// Set the title
		$this->layout->blocks['head']->title = 'Add ' . $this->_singular(TRUE);

		// Send any form data or errors to the view
		$this->layout->blocks['body']->form = $this->_form_values;
		$this->layout->blocks['body']->selected = $this->_form_selected;
		$this->layout->blocks['body']->errors = $this->_form_errors;
	}

	/**
	 * Delete a workflow dataset.
	 */
	public function delete($id = NULL)
	{
		// Redirect to root if passed an invalid Id
		if (!valid::digit($id) || $id < 1)
			$this->_redirect_to_root();

		// Load ORM model from database
		$model = ORM::factory($this->_singular(), $id);

		// Check the model successfully loaded
		if ($model->loaded === TRUE)
		{
			if ($this->input->post('yes'))
			{
				// Delete model
				$model->delete();
				// Flash success notification
				$this->_set_notification
				(
					$this->_singular(TRUE) . ' deleted.',
					'success',
					TRUE
				);
				// Redirect to root location
				$this->_redirect_to_root();
			}
			elseif($this->input->post('no'))
			{
				// Redirect to root location
				$this->_redirect_to_root();
			}
			else
			{
				// Load dialog block, replaces default
				$this->_load_block('body', 'dialogs/yes_no');
				// Set dialog prompt
				$this->layout->blocks['body']->prompt = 'Are you sure you want to delete this ' . $this->_singular(TRUE) . ' ?';
				// Set the title
				$this->layout->blocks['head']->title = 'Delete ' . $this->_singular(TRUE);
			}
		}
		else
		{
			// Flash error notification
			$this->_set_notification
			(
				'Unable to find ' . $this->_singular(TRUE) . '.',
				'error',
				TRUE
			);
			// Redirect to root location
			$this->_redirect_to_root();
		}
	}

	/**
	 * Edit a workflow dataset.
	 *
	 * @param mixed integer or string value of object ID to load
	 */
	public function edit($id = NULL)
	{
		// Redirect to root if passed an invalid Id
		if (!valid::digit($id) || $id < 1)
			$this->_redirect_to_root();

		// Load ORM model from database
		$model = ORM::factory($this->_singular(), $id);

		// Check the model successfully loaded
		if ($model->loaded === TRUE)
		{
			// Check for $_POST data
			if ($this->input->post())
			{
				// Fetch $_POST data into array
				$data = $this->input->post();
				// Process the input form data
				if ($this->_process_form_data($data, $model))
				{
					// Flash success notification
					$this->_set_notification
					(
						'Updated ' . $this->_singular(TRUE) . '.',
						'success',
						TRUE
					);
					// Redirect to root location
					$this->_redirect_to_root();
				}
				else
				{
					// Set error notification
					$this->_set_notification
					(
						'Unable to update ' . $this->_singular(TRUE) . '.',
						'error'
					);
				}
			}
			else
			{
				// Return the model information as form
				$this->_form_values = arr::overwrite($this->_form_values, $model->as_array());
				// Return the model selected form elements
				$this->_form_selected = arr::overwrite($this->_form_selected, $model->as_array());
			}

			// Load form body block, replaces default
			$this->_load_block('body', 'forms/admin_' . $this->_workflow);
			// Set the title
			$this->layout->blocks['head']->title = 'Edit ' . $this->_singular(TRUE);

			// Send any form data or errors to the view
			$this->layout->blocks['body']->form = $this->_form_values;
			$this->layout->blocks['body']->selected = $this->_form_selected;
			$this->layout->blocks['body']->errors = $this->_form_errors;
		}
		else
		{
			// Flash error notification
			$this->_set_notification
			(
				'Unable to find ' . $this->_singular(TRUE) . '.',
				'error',
				TRUE
			);
			// Redirect to root location
			$this->_redirect_to_root();
		}
	}

	public function index()
	{
		// Redirect to search
		url::redirect($this->_root_controller_url . '/search/');
	}

	public function search($page = NULL, $model = NULL, $filter_form = NULL)
	{
		// Check page number is a valid positive digit
		if (!valid::digit($page) || $page < 1)
		{
			$page = 1;
		}

		// Load model and fetch column names
		if (!isset($model))
		{
			$model = ORM::factory($this->_singular());
		}
		$column_titles = $this->_search_titles;

		// Fetch pagination configuration into array
		$config = Kohana::Config('pagination.default', TRUE);

		// Calculate limit and offset for pagination
		$limit = $config['items_per_page'];
		$offset = $config['items_per_page'] * ($page - 1);

		// Fetch ORM iterator
		if (empty($this->_search_orderby))
		{
			// Fetch all items with default ordering
			$iterator = $model->find_all($limit, $offset);
		}
		else
		{
			// Fetch all items using the defined controller context order
			$iterator = $model->orderby($this->_search_orderby)->find_all($limit, $offset);
		}

		// Set pagination total_items using count_last_query (ignores limit and offset)
		$config['total_items'] = $model->count_last_query();

		$links = array();
		$rows = array();
		foreach ($iterator as $key => $item)
		{
			$row = array();
			// Generate row data columns
			foreach ($this->_search_titles as $search_column => $search_title)
			{
				// Check whether the column exists
				if ($item->__isset($search_column))
				{
					if (substr($search_column, -3) === '_id')
					{
						$related_object_name = substr($search_column, 0, -3);
						// Attempt to load a related object using the reference id
						$related_object = ORM::factory($related_object_name, $item->$search_column);
						// Check whether a valid related object was loaded
						if ($related_object->loaded)
						{
							$related_object->find($item->$search_column);
							$row[$search_column] = $related_object;
						}
						else
						{
							// Invalid object reference
							$row[$search_column] = 'Invalid Reference (' . $item->$search_column . ')';
						}
					}
					else
					{
						$row[$search_column] = $item->$search_column;
					}
				}
				else
				{
					// Search column isn't set or doesn't exist
					$row[$search_column] = $this->_search_missing_column;
				}
			}
			// Append new row
			$rows[$key] = $row;
			// Generate row links
			$links[$key] = array();
			foreach ($this->_search_row_links as $title => $link)
			{
				// Replace all occurrences of id with the row id
				$links[$key][$title] = str_replace('%id%', $item->id, $link);
			}
		}

		if (count($rows) === 0)
		{
			if ($page > 1)
			{
				url::redirect($this->_root_controller_url . '/search/1/?' . http_build_query($this->input->get()));
			}
			else
			{
				// Set error notification
				$this->_set_notification
				(
					'There are currently no ' . ucfirst($this->_workflow) . ' to display.',
					'info'
				);
			}
		}

		// Create new pagination object
		$pag = new Pagination;
		$pagination = $pag->Factory($config);

		// Load body block, replaces default
		$this->_load_block('body', 'admin_search');
		// Set the title
		$this->layout->blocks['head']->title = $this->_singular(TRUE) . ' Search';
		// Set the columns
		$this->layout->blocks['body']->column_titles = $column_titles;
		// Set the rows
		$this->layout->blocks['body']->rows = $rows;
		// Set the root controller URL
		$this->layout->blocks['body']->root_controller_url = $this->_root_controller_url;
		// Set the actions
		$this->layout->blocks['body']->links = $links;
		// Set the pagination
		$this->layout->blocks['body']->pagination = $pagination;
		// Set the filter form
		if (isset($filter_form))
		{
			$this->layout->blocks['body']->filter_form = View::factory($this->blocks_dir . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . $filter_form);
		}
	}

	/**
	 * Sanitize and validate the passed data using a given ORM model.
	 * The model saves successfully validated data. Form fields and
	 * their selected states are populated, in addition to any
	 * validation errors.
	 *
	 * @param array input data, typically from $_POST
	 * @param mixed instance of an ORM model
	 * @param string the i18n file to translate errors
	 * @return boolean validation state
	 */
	protected function _process_form_data(array &$data, $model, $errors_i18n = NULL)
	{
		// Pull out only the expected form fields, this is to prevent
		// malicious data injection (e.g., crafted 'id') and fill
		// non-validated field keys
		$data = arr::extract
		(
			$data,
			array_keys($this->_form_values)
		);
		// Perform validation, converts $data to Validation object
		if ($model->validate($data))
		{
			// Successful validation, save!
			$model->save();
			// Populate form fields from saved model
			$this->_form_values = arr::overwrite($this->_form_values, $model->as_array());
			// Populate selected fields from saved model
			$this->_form_selected = arr::overwrite($this->_form_selected, $model->as_array());
			// Return TRUE (validation success)
			return TRUE;
		}
		else
		{
			// Failed validation
			// Repopulate form fields with passed $data (last entered values)
			$this->_form_values = arr::overwrite($this->_form_values, $data->as_array());
			// Populate selected fields with last selected values
			$this->_form_selected = arr::overwrite($this->_form_selected, $data->as_array());
			// Populate the error fields
			$this->_form_errors = arr::overwrite($this->_form_errors, $data->errors($errors_i18n));
			// Return FALSE (validation failure)
			return FALSE;
		}
	}

	/**
	 * Redirect to the root controller location.
	 */
	protected function _redirect_to_root()
	{
		// Redirect to root location
		url::redirect($this->_root_controller_url . '/search/');
	}

	/**
	 * Returns the singular workflow name from the workflow name.
	 *
	 * @param boolean switch the first letter as capital
	 * @param boolean make string lowercase (before applying ucFirst)
	 * @return string model name
	 */
	protected function _singular($uc_first = FALSE, $to_lower = TRUE)
	{
		$singular = inflector::singular($this->_workflow);
		$singular = ($to_lower) ? strtolower($singular) : $singular;
		$singular = ($uc_first) ? ucfirst($singular) : $singular;
		return $singular;
	}

} // End Admin Workflow Controller
