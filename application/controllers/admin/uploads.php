<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Uploads Controller
 */
class Uploads_Controller extends Admin_Workflow_Controller {

	// Set the workflow name (plural).
	protected $_workflow = 'uploads';

	public function add()
	{
		// Proxy to add_image()
		self::add_image();
	}

	public function add_image()
	{
		// Create an array to store errors
		$errors = array();

		// Have any files been submitted for uploading?
		if (!empty($_FILES))
		{
			// Build an upload validation object
			$file = Validation::factory($_FILES)
					->add_rules
					(
						'file_1',
						'upload::required',
						'upload::valid',
						'upload::type[gif,jpg,png]'
					);

			// Check the upload is valid
			if ($file->validate())
			{
				// Check for an image type
				if ($type = $this->input->post('type_1'))
				{
					// Check for tags
					if ($tags = $this->input->post('tags_1'))
					{
						// Split string
						$tags = explode(',', $tags);
						// Check there is at least one tag
						if (!empty($tags))
						{
							// Try to save uploaded file to the uploads directory
							$filename = upload::save('file_1');

							// Get the file md5 vale
							$md5 = md5_file($filename);

							// Check to see whether this image has already been uploaded
							if (ORM::factory('gallery_image')->where('original_md5', $md5)->count_all() === 0)
							{
								// Get dimensions and directory data for this image type
								$size_dimensions = Kohana::config('gallery.size_dimensions');
								if (($dir = Kohana::config("gallery.$type" . '_dir', TRUE)) &&
									isset($size_dimensions[$type]))
								{
									// Get height and width dimensions
									$height = $size_dimensions[$type]['height'];
									$width = $size_dimensions[$type]['width'];
									// Get preview height and width dimensions
									$preview_height = $size_dimensions['preview']['height'];
									$preview_width = $size_dimensions['preview']['width'];
									// Get thumbnail height and width dimensions
									$thumbnail_height = $size_dimensions['thumbnail']['height'];
									$thumbnail_width = $size_dimensions['thumbnail']['width'];
									// Fix any image dimensions that are smaller than preview/thumbnail
									if (($height < $preview_height) || ($width < $preview_width))
									{
										$preview_height = $height;
										$preview_width = $width;
									}
									elseif(($height < $thumbnail_height) || ($width < $thumbnail_width))
									{
										$thumbnail_height = $height;
										$thumbnail_width = $width;
									}

									// Get root gallery directory
									if ($gallery_root = Kohana::config('gallery.root_dir', TRUE))
									{
										// Get file extension
										$ext = strtolower(substr(strrchr($filename, '.'), 1));
										// Generate image save paths
										$resized_image = $dir . "$md5.$ext";
										$preview_image = $dir . $md5 ."_m.$ext";
										$thumbnail_image = $dir . $md5 ."_s.$ext";
										try
										{
											// Save a resized copy to the gallery type directory
											Image::factory($filename)
												->resize($width, $height, Image::WIDTH)
												->save($gallery_root . $resized_image);
											// Save a preview copy
											Image::factory($filename)
												->resize($preview_width, $preview_height, Image::WIDTH)
												->save($gallery_root . $preview_image);
											// Save a thumbnail copy
											Image::factory($filename)
												->resize($thumbnail_width, $thumbnail_height, Image::WIDTH)
												->save($gallery_root . $thumbnail_image);
										}
										catch (Exception $e)
										{
											$errors[] = $e->getMessage();
										}
										// Delete the uploaded file
										unlink($filename);

										// Create a gallery image model
										$gallery_image = ORM::factory('gallery_image');
										// Build model data
										$model_data = array
										(
											'filename' => $resized_image,
											'preview' => $preview_image,
											'thumbnail' => $thumbnail_image,
											'type' => $type,
											'original_md5' => $md5,
										);
										// Attempt model validation
										if ($gallery_image->validate($model_data))
										{
											// Add tags to the gallery image
											foreach ($tags as $tag_name)
											{
												$tag = ORM::factory('tag')->where('name', $tag_name)->find();
												// Check tag exists
												if (!$tag->loaded)
												{
													$tag->name = $tag_name;
													// Save tag
													$tag->save();
												}
												// Add saved tag to the gallery image
												$gallery_image->add($tag);
											}
											// Save gallery image
											$gallery_image->save();

											// Flash success notification
											$this->_set_notification
											(
												$this->_singular(TRUE) . ' added.',
												'success'
											);
										}
										else
										{
											// Model validation failed
											$errors = $model_data->errors();
										}
									}
									else
									{
										// No configured gallery root
										$errors[] = 'no_gallery_root';
									}
								}
								else
								{
									// Invalid type / type config
									$errors[] = 'invalid_type';
								}
							}
							else
							{
								// File with that md5 exists already
								$errors[] = 'file_exists';
							}
						}
						else
						{
							// Tags was a non-array / empty array
							$errors[] = 'tag_invalid';
						}

					}
					else
					{
						// No tags
						$errors[] = 'tag_missing';
					}
				}
				else
				{
					// No type
					$errors[] = 'no_type';
				}
			}
			else
			{
				// File validation failed
				$errors = $file->errors();
			}
		}

		// Load form body block, replaces default
		$this->_load_block('body', 'forms/admin_uploads_image');
		// Set the title
		$this->layout->blocks['head']->title = 'Add ' . $this->_singular(TRUE);
		// Set errors
		$this->layout->blocks['body']->errors = $errors;
	}

	public function index()
	{
		// Redirect to fixtures search location
		url::redirect("admin/uploads/add");
	}

	public function view($id)
	{
		// TODO: Load a given model for viewing
	}

} // End Uploads Controller
