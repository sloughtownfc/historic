<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Academy Reports controller
 */
class Academy_Reports_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(
		'report_home_score' => 0,
		'report_away_score' => 0,
	);

	// Set form field values.
	protected $_form_values = array
	(
		'report_home_score' => '',
		'report_away_score' => '',
		'report_home_scorers' => '',
		'report_headline' => '',
		'report_excerpt' => '',
		'report_body' => '',
		'picture_url' => '',
		'report_team' => '',
		'report_motm' => '',
		'report_published' => ''
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/academy_reports';

	// Set the workflow name. This should be plural.
	// e.g., 'competitions'
	protected $_workflow = 'academy_reports';

	/**
	 * Redirect to the root controller location.
	 */
	protected function _redirect_to_root()
	{
		// Redirect to root location
		url::redirect('/admin/academy_fixtures/search/');
	}

	public function add()
	{
		// Redirect to fixtures search location
		url::redirect('admin/academy_fixtures/search/');
	}
	
	public function delete($id = NULL)
	{
		// Prevent deletion
		$this->_set_notification
		(
			'Sorry, you can\'t delete this ' . $this->_singular(TRUE) . '.',
			'error',
			TRUE
		);
		// Redirect to fixtures search location
		url::redirect('admin/academy_fixtures/search/');
	}

	public function search($page = NULL)
	{
		// Redirect to fixtures search location
		url::redirect("admin/academy_fixtures/search/$page/");
	}

	public function view($id)
	{
		// TODO: Load a given model for viewing
	}

} // End Academy Reports Controller
