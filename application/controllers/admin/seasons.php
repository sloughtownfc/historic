<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Seasons controller
 */
class Seasons_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(
	);

	// Set form field values.
	protected $_form_values = array
	(
		'name' => '',
		'start_date' => '',
		'end_date' => ''
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/seasons';

	// Set the value to display for missing columns.
	protected $_search_missing_column = 'TBA';

	// Set the column and direction to order by in a search.
	protected $_search_orderby = array
	(
		'start_date' => 'DESC'
	);

	// Set the row item links.
	protected $_search_row_links = array
	(
		'<span class="glyphicon glyphicon-pencil"></span>' => 'edit/%id%',
		'<span class="glyphicon glyphicon-remove"></span>' => 'delete/%id%'
	);

	// Set the columns to return in a search.
	protected $_search_titles = array
	(
		'name' => 'Name',
		'start_date' => 'Start Date',
		'end_date' => 'End Date'
	);

	// Set the workflow name (plural).
	protected $_workflow = 'seasons';

	public function view($id)
	{
		// TODO: Load a given model for viewing
	}

} // End Seasons Controller
