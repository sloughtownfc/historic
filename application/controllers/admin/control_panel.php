<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin Control Panel controller
 */
class Control_Panel_Controller extends Admin_Controller {

	public function index()
	{
		// Set the title.
		$this->layout->blocks['head']->title = 'STFC Admin Area';
	}

} // End Admin Control Panel Controller