<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Reports controller
 */
class Reports_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(
		'report_home_score' => 0,
		'report_away_score' => 0,
		'report_extra_time' => 0,
		'report_picture_url' => '/gallery/front/c2c3aca45dbafe58281cece5694a91ac.jpg',
	);

	// Set form field values.
	protected $_form_values = array
	(
		'report_picture_url' => '',
		'report_home_score' => '',
		'report_away_score' => '',
		'report_home_scorers' => '',
		'report_away_scorers' => '',
		'report_extra_time' => '',
		'report_attendance' => '',
		'report_headline' => '',
		'report_excerpt' => '',
		'report_body' => '',
		'report_friendly_team' => '',
		'report_mom' => '',
		'report_away_team' => '',
		'report_flickr_url' => '',
		'report_published' => ''
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/reports';

	// Set the workflow name. This should be plural.
	// e.g., 'competitions'
	protected $_workflow = 'reports';

	/**
	 * Redirect to the root controller location.
	 */
	protected function _redirect_to_root()
	{
		// Redirect to root location
		url::redirect('/admin/fixtures/search/');
	}

	public function add()
	{
		// Redirect to fixtures search location
		url::redirect('admin/fixtures/search/');
	}

	public function edit($id = NULL)
	{
		parent::edit($id);
		// Attempt to load fixture and check it belongs to a valid competition
		$fixture = ORM::factory('fixture', $id);
		if ($fixture->loaded && $fixture->__isset('competition_id'))
		{
			// Check whether stats can be recorded for this fixture
			if ($fixture->competition->stats_recorded !== 1)
			{
				// Switch to "friendlies" mode
				$this->layout->blocks['body']->friendlies_mode = TRUE;
			}
		}
		// Fetch all player ids and names, grouped by active state for form select
		$player_ids = array();
		$player_ids['Active Players'] = ORM::factory('player')->where('active', 1)->orderby('last_name', 'ASC')->select_list_fullname('id');
		$player_ids['Other Players'] = ORM::factory('player')->where('active', 0)->orderby('last_name', 'ASC')->select_list_fullname('id');
		$this->layout->blocks['body']->player_ids = $player_ids;
	}

	public function delete($id = NULL)
	{
		// Prevent deletion
		$this->_set_notification
		(
			'Sorry, you can\'t delete this ' . $this->_singular(TRUE) . '.',
			'error',
			TRUE
		);
		// Redirect to fixtures search location
		url::redirect('admin/fixtures/search/');
	}

	public function search($page = NULL)
	{
		// Redirect to fixtures search location
		url::redirect("admin/fixtures/search/$page/");
	}

	public function view($id)
	{
		// TODO: Load a given model for viewing
	}

} // End Reports Controller
