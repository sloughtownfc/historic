<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Trust controller
 */
class Trust_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(
	);

	// Set form field values.
	protected $_form_values = array
	(
		'publish_date' => '',
	 	'author' => '',
		'headline' => '',
		'body' => '',
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/trust';

	// Set the row item links.
	protected $_search_row_links = array
	(
		'<span class="glyphicon glyphicon-pencil"></span>' => 'edit/%id%',
		'<span class="glyphicon glyphicon-remove"></span>' => 'delete/%id%'
	);

	// Set the columns to return in a search.
	protected $_search_titles = array
	(
		'publish_date' => 'Date Published',
		'headline' => 'Headline',
		'author' => 'Author'
	);

	// Set the workflow name (plural).
	protected $_workflow = 'trust';

	public function view($id)
	{
		// TODO: Load a given model for viewing
	}

} // End Trust Controller
