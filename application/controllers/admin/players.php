<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Players controller
 */
class Players_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(
	 	'picture_url' => '/images/main/no_profile.jpg'
	);

	// Set form field values.
	protected $_form_values = array
	(
		'position_id' => '',
		'first_name' => '',
		'last_name' => '',
		'dob' => '',
		'description' => '',
		'achievements' => '',
		'picture_url' => '',
		'active' => ''
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/players';

	// Set the row item links.
	protected $_search_row_links = array
	(
		'<span class="glyphicon glyphicon-pencil"></span>' => 'edit/%id%',
		'<span class="glyphicon glyphicon-remove"></span>' => 'delete/%id%'
	);

	// Set the columns to return in a search.
	protected $_search_titles = array
	(
		'last_name' => 'Last Name',
		'first_name' => 'First Name',
		'position_id' => 'Position',
		'dob' => 'Date of Birth',
		'active' => 'Still active?'
	);

	// Set the workflow name (plural).
	protected $_workflow = 'players';

	public function add()
	{
		// Call parent add
		parent::add();
		// Fetch all Competition Ids and names for form select
		$this->_form_values['position_id'] = ORM::factory('position')->select_list('id', 'name');
		// Send late additions to the view
		$this->layout->blocks['body']->form = $this->_form_values;
	}

	public function edit($id)
	{
		// Call parent edit
		parent::edit($id);
		// Fetch all Competition Ids and names for form select
		$this->_form_values['position_id'] = ORM::factory('position')->select_list('id', 'name');
		// Send late additions to the view
		$this->layout->blocks['body']->form = $this->_form_values;
	}

	public function search($page = NULL)
	{
		// Create model
		$model = ORM::factory($this->_singular());
		// Check for $_GET data
		if ($data = $this->input->get())
		{
			$model->search_fullname($data['name']);
		}
		// Perform search
		parent::search($page, $model, 'admin_search_players');
		foreach ($this->layout->blocks['body']->rows as &$row)
		{
			$row['active'] = ($row['active'] === 1 ? 'Y' : 'N');
		}
	}

	// Set the column and direction to order by in a search.
	protected $_search_orderby = array
	(
		'last_name' => 'ASC'
	);

	public function view($id)
	{
		// TODO: Load a given model for viewing
	}

} // End Players Controller
