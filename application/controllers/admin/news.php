<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * News controller
 */
class News_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(
	);

	// Set form field values.
	protected $_form_values = array
	(
		'headline' => '',
		'author' => '',
		'publish_date' => '',
		'publish_time' => '',
		'excerpt' => '',
		'body' => '',
		'video_url' => '',
		'audio_url' => '',
		'picture_url' => '',
		'small_picture_url' => '',
		'caption' => '',
		'quote' => '',
		'quote_by' => ''
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/news';

	// Set the row item links.
	protected $_search_row_links = array
	(
		'<span class="glyphicon glyphicon-pencil"></span>' => 'edit/%id%',
		'<span class="glyphicon glyphicon-remove"></span>' => 'delete/%id%'
	);

	// Set the columns to return in a search.
	protected $_search_titles = array
	(
		'publish_date' => 'Date Published',
		'headline' => 'Headline',
	);
	
	// Set the column and direction to order by in a search.
	protected $_search_orderby = array
	(
		'publish_date' => 'DESC', 
		'publish_time' => 'DESC',
	);

	// Set the workflow name (plural).
	protected $_workflow = 'news';

	public function view($id)
	{
		// TODO: Load a given model for viewing
	}

} // End News Controller
