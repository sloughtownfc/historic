<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Stats controller
 */
class Stats_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(

	);

	// Set form field values.
	protected $_form_values = array
	(
		'fixture_id' => '',
		'player_id' => '',
		'shirt' => '',
		'sub_shirt' => '',
		'appearance' => '',
		'goals' => '',
		'bookings' => '',
		'injured' => ''
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/stats';

	// Set the row item links.
	protected $_search_row_links = array
	(
		'<span class="glyphicon glyphicon-pencil"></span>' => 'edit/%id%',
		'<span class="glyphicon glyphicon-remove"></span>' => 'delete/%id%'
	);

	// Set the columns to return in a search.
	protected $_search_titles = array
	(

	);

	// Set the workflow name (plural).
	protected $_workflow = 'stats';


	public function edit($fixture_id = NULL)
	{
		// Attempt to load fixture and check it belongs to a valid competition
		$fixture = ORM::factory('fixture', $fixture_id);
		if ($fixture->loaded && $fixture->__isset('competition_id'))
		{
			// Check whether stats can be recorded for this fixture
			if ($fixture->competition->stats_recorded === 1)
			{
				// Fetch row count (expected rows) for this fixture based on its competition
				$row_count = 11 + $fixture->competition->bench_count;
				// Check whether data has been submitted
				if ($this->input->post())
				{
					// Fetch $_POST data into array
					$data = $this->input->post();
					// Extract keys, strings can't be rotated - e.g., 'submit'
					// Helps protect against injection
					$data = arr::extract
					(
						$data,
						array_keys($this->_form_values)
					);
					// Unset fixture_id as this is set via the controller parameters
					unset($data['fixture_id']);
					// Rotate array to match up form values
					$data = arr::rotate($data, TRUE);
					// Destroy all existing stats for a fixture.
					ORM::factory($this->_singular())
						->where('fixture_id', $fixture_id)
						->delete_all();
					// Store default form values
					$default_form_values = $this->_form_values;
					$error_occurred = FALSE;
					$form_values = $form_selected = $form_errors = array();
					foreach ($data as $key => $stat)
					{
						if (empty($stat['shirt']))
						{
							// Reset rows where shirt hasn't been set
							$form_values[$key] = $default_form_values;
							continue;
						}
						else
						{
							$stat['fixture_id'] = $fixture_id;
							$model = ORM::factory($this->_singular())
								->where('fixture_id', $stat['fixture_id'])
								->where('player_id', $stat['player_id'])
								->find();
							if (!$this->_process_form_data($stat, $model))
							{
								$error_occurred = TRUE;
								$form_errors[$key] = $this->_form_errors;
							}
							$form_selected[$key] = $this->_form_selected;
							$form_values[$key] = $this->_form_values;
						}
					}
					if ($error_occurred)
					{
						// Set error notification
						$this->_set_notification
						(
							'An error occurred saving some elements.',
							'error'
						);
						// Load form body block, replaces default
						$this->_load_block('body', 'forms/admin_' . $this->_workflow);

						// Set the bench count
						$this->layout->blocks['body']->row_count = $row_count;

						// Send any form data or errors to the view
						$this->layout->blocks['body']->form = $form_values;
						$this->layout->blocks['body']->selected = $form_selected;
						$this->layout->blocks['body']->errors = $form_errors;

						// Fetch all player ids and names, grouped by active state for form select
						$player_ids = array();
						$player_ids['Active Players'] = ORM::factory('player')->where('active', 1)->select_list_fullname('id');
						$player_ids['Other Players'] = ORM::factory('player')->where('active', 0)->select_list_fullname('id');
						$this->layout->blocks['body']->player_ids = $player_ids;
					}
					else
					{
						// Flash success notification
						$this->_set_notification
						(
							'All saved successfully.',
							'success',
							TRUE
						);
						// Redirect to fixtures search location
						url::redirect('admin/fixtures/search/');
					}
				}
				else
				{
					// Clear out controller context form values
					$this->_form_values = array();
					// Attempt to find existing statistics for this fixture
					$stats = ORM::factory('stat')->where('fixture_id', $fixture_id)->find_all();
					foreach ($stats as $stat)
					{
						$this->_form_values[] = $this->_form_selected[] = $stat->as_array();
					}
					// Load form body block, replaces default
					$this->_load_block('body', 'forms/admin_' . $this->_workflow);

					// Set the bench count
					$this->layout->blocks['body']->row_count = $row_count;

					// Send any form data or errors to the view
					$this->layout->blocks['body']->form = $this->_form_values;
					$this->layout->blocks['body']->selected = $this->_form_selected;
					$this->layout->blocks['body']->errors = $this->_form_errors;

					// Fetch all player ids and names, grouped by active state for form select
					$player_ids = array();
					$player_ids['Active Players'] = ORM::factory('player')->where('active', 1)->orderby('last_name', 'ASC')->select_list_fullname('id');
					$player_ids['Other Players'] = ORM::factory('player')->where('active', 0)->orderby('last_name', 'ASC')->select_list_fullname('id');
					$this->layout->blocks['body']->player_ids = $player_ids;
				}
			}
			else
			{
				// Cannot record stats for this fixture
				// Flash error notification
				$this->_set_notification
				(
					'Statistics are not recorded for fixtures in this competition.',
					'error',
					TRUE
				);
				// Redirect to fixtures search location
				url::redirect('admin/fixtures/search/');
			}
		}
		else
		{
			// Invalid fixture or fixture has no competition...
			// Flash error notification
			$this->_set_notification
			(
				'Invalid or corrupt fixture.',
				'error',
				TRUE
			);
			// Redirect to fixtures search location
			url::redirect('admin/fixtures/search/');
		}
	}

	public function add()
	{
		// Redirect to fixtures search location
		url::redirect('admin/fixtures/search/');
	}

	public function delete($id = NULL)
	{
		// Prevent deletion
		$this->_set_notification
		(
			'Sorry, you can\'t delete this ' . $this->_singular(TRUE) . '.',
			'error',
			TRUE
		);
		// Redirect to fixtures search location
		url::redirect('admin/fixtures/search/');
	}

	public function search($page = NULL)
	{
		// Redirect to fixtures search location
		url::redirect("admin/fixtures/search/$page/");
	}

	public function view($id)
	{
		// TODO: Load a given model for viewing.
	}

} // End Stats Controller
