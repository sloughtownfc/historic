<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Fixtures controller
 */
class Fixtures_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(

	);

	// Set form field values.
	protected $_form_values = array
	(
		'match_date' => '',
		'competition_id' => '',
		'opposition_id' => '',
		'is_home' => '',
		'away_alternate_kit' => '',
		'kickoff' => '',
		'match_sponsor' => '',
		'ball_sponsor' => ''
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/fixtures';

	// Set the value to display for missing columns.
	protected $_search_missing_column = 'TBA';

	// Set the column and direction to order by in a search.
	protected $_search_orderby = array
	(
		'match_date' => 'DESC'
	);

	// Set the row item links.
	protected $_search_row_links = array
	(
		'<span class="glyphicon glyphicon-pencil"></span>' => 'edit/%id%',
		'<span class="glyphicon glyphicon-remove"></span>' => 'delete/%id%',
		'<span class="glyphicon glyphicon-folder-open"></span>' => '../reports/edit/%id%',
		'<span class="glyphicon glyphicon-film"></span>' => '../videos/edit/%id%',
		'<span class="glyphicon glyphicon-map-marker"></span>' => '../coaches/edit/%id%',
		'<span class="glyphicon glyphicon-file"></span>' => '../manager_notes/edit/%id%',
		'<span class="glyphicon glyphicon-stats"></span>' => '../stats/edit/%id%'
	);

	// Set the columns to return in a search.
	protected $_search_titles = array
	(
		'match_date' => 'Match Date',
		'competition_id' => 'Competition',
		'opposition_id' => 'Opposition',
		'is_home' => 'Home / Away',
		'kickoff' => 'Kickoff Time'
	);

	// Set the workflow name (plural).
	protected $_workflow = 'fixtures';

	public function add()
	{
		// Call parent add
		parent::add();
		// Fetch all Competition Ids and names for form select
		$this->_form_values['competition_id'] = ORM::factory('competition')->select_list('id', 'name');
		// Fetch all Opposition Ids and names for form select
		$this->_form_values['opposition_id'] = ORM::factory('opposition')->orderby('name', 'ASC')->select_list('id', 'name');
		// Send late additions to the view
		$this->layout->blocks['body']->form = $this->_form_values;
	}

	public function delete($id)
	{
		parent::delete($id);
		// Set dialog prompt
		$this->layout->blocks['body']->prompt = "Are you sure you want to delete this fixture? This will delete all associated reports, coach details, etc.";
	}

	public function edit($id)
	{
		// Call parent edit
		parent::edit($id);
		// Fetch all Competition Ids and names for form select
		$this->_form_values['competition_id'] = ORM::factory('competition')->select_list('id', 'name');
		// Fetch all Opposition Ids and names for form select
		$this->_form_values['opposition_id'] = ORM::factory('opposition')->orderby('name', 'ASC')->select_list('id', 'name');
		// Send late additions to the view
		$this->layout->blocks['body']->form = $this->_form_values;
	}

	public function search($page = NULL)
	{
		// Create model
		$model = ORM::factory($this->_singular());
		// Check for $_GET data
		if ($data = $this->input->get())
		{
			$oppositions = ORM::factory('opposition')
				->like('name', $data['name'])
				->find_all()
				->as_array();
			// Only search if an opposition is returned, otherwise an error
			// will occur
			if (count($oppositions) > 0)
			{
				$model->in('opposition_id', $oppositions);
			}
		}
		// Perform search
		parent::search($page, $model, 'admin_search_fixtures');
		foreach ($this->layout->blocks['body']->rows as $key => &$row)
		{
			// Perform home / away tweaks
			if ($row['is_home'] === 1)
			{
				// Remove coach details for home games
				unset($this->layout->blocks['body']->links[$key]['<span class="glyphicon glyphicon-map-marker"></span>']);
				// Add grey copy
				$this->layout->blocks['body']->links[$key]['<span class="glyphicon glyphicon-map-marker unset"></span>'] = '#';
				// Display home / away in the is_home column
				$row['is_home'] = 'H';
			}
			else
			{
				// Remove manager notes for away games
				unset($this->layout->blocks['body']->links[$key]['<span class="glyphicon glyphicon-file"></span>']);
				// Add grey copy
				$this->layout->blocks['body']->links[$key]['<span class="glyphicon glyphicon-file unset"></span>'] = '#';
				// Display home / away in the is_home column
				$row['is_home'] = 'A';
			}

			// Remove the "Edit Statistics" link / icon for matches that don't record statistics
			if (is_object($row['competition_id']))
			{
				if ($row['competition_id']->stats_recorded !== 1)
				{
					// Remove the statstics link
					unset($this->layout->blocks['body']->links[$key]['<span class="glyphicon glyphicon-stats"></span>']);
					// Add grey copy
					$this->layout->blocks['body']->links[$key]['<span class="glyphicon glyphicon-stat unset"></span>'] = '#';
				}
			}
		}
	}

	public function view($id)
	{
		// TODO: Load a given model for viewing.
	}

} // End Fixtures Controller
