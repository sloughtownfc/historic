<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Oppositions controller
 */
class Oppositions_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(
		'logo' => '/images/main/no_image.jpg',
	);

	// Set form field values.
	protected $_form_values = array
	(
		'name' => '',
		'colour_away_background' => '',
		'colour_away_foreground' => '',
		'colour_home_background' => '',
		'colour_home_foreground' => '',
		'logo' => '',
		'website' => '',
		'grounds_venue' => '',
		'grounds_address' => '',
		'grounds_telephone' => '',
		'grounds_directions' => '',
		'grounds_google' => '',
		'notes' => ''
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/oppositions';

	// Set the row item links.
	protected $_search_row_links = array
	(
		'<span class="glyphicon glyphicon-pencil"></span>' => 'edit/%id%',
		'<span class="glyphicon glyphicon-remove"></span>' => 'delete/%id%'
	);

	// Set the columns to return in a search.
	protected $_search_titles = array
	(
		'name' => 'Name',
		'grounds_venue' => 'Ground Name'
	);

	// Set the column and direction to order by in a search.
	protected $_search_orderby = array
	(
		'name' => 'ASC',
	);

	// Set the workflow name (plural).
	protected $_workflow = 'oppositions';

	public function search($page = NULL)
	{
		// Create model
		$model = ORM::factory($this->_singular());
		// Check for $_GET data
		if ($data = $this->input->get())
		{
			$model->like('name', $data['name']);
		}
		// Perform search
		parent::search($page, $model, 'admin_search_oppositions');
	}

	public function view($id)
	{
		// TODO: Load a given model for viewing
	}

} // End Oppositions Controller
