<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * U18s Fixtures controller
 */
class U18s_Fixtures_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(

	);

	// Set form field values.
	protected $_form_values = array
	(
		'match_date' => '',
		'competition' => '',
		'opposition' => '',
		'is_home' => '',
		'kickoff' => ''
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/u18s_fixtures';

	// Set the value to display for missing columns.
	protected $_search_missing_column = 'TBA';

	// Set the column and direction to order by in a search.
	protected $_search_orderby = array
	(
		'match_date' => 'DESC'
	);

	// Set the row item links.
	protected $_search_row_links = array
	(
		'<span class="glyphicon glyphicon-pencil"></span>' => 'edit/%id%',
		'<span class="glyphicon glyphicon-remove"></span>' => 'delete/%id%',
		'<span class="glyphicon glyphicon-folder-open"></span>' => '../u18s_reports/edit/%id%'
	);

	// Set the columns to return in a search.
	protected $_search_titles = array
	(
		'match_date' => 'Match Date',
		'competition' => 'Competition',
		'opposition' => 'Opposition',
		'is_home' => 'Home / Away',
		'kickoff' => 'Kickoff Time'
	);

	// Set the workflow name (plural).
	protected $_workflow = 'u18s_fixtures';

	public function add()
	{
		// Call parent add
		parent::add();
		// Send late additions to the view
		$this->layout->blocks['body']->form = $this->_form_values;
	}

	public function delete($id)
	{
		parent::delete($id);
		// Set dialog prompt
		$this->layout->blocks['body']->prompt = "Are you sure you want to this fixture?<br />\nThis will delete all associated reports.";
	}

	public function edit($id)
	{
		// Call parent edit
		parent::edit($id);
		// Send late additions to the view
		$this->layout->blocks['body']->form = $this->_form_values;
	}

	public function search($page = NULL)
	{
		// Create model
		$model = ORM::factory($this->_singular());
		// Check for $_GET data
		if ($data = $this->input->get())
		{
			$model->like('opposition', $data['name']);
		}
		// Perform search
		parent::search($page, $model, 'admin_search_fixtures');
		foreach ($this->layout->blocks['body']->rows as $key => &$row)
		{
			// Perform home / away tweaks
			if ($row['is_home'] === 1)
			{
				// Display home / away in the is_home column
				$row['is_home'] = 'H';
			}
			else
			{
				// Display home / away in the is_home column
				$row['is_home'] = 'A';
			}
		}
	}

	public function view($id)
	{
		// TODO: Load a given model for viewing.
	}

} // End U18s Fixtures Controller
