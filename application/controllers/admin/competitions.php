<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Competitions controller
 */
class Competitions_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(
		'bench_count' => 5,
		'stats_recorded' => 1
	);

	// Set form field values.
	protected $_form_values = array
	(
		'name' => '',
		'description' => '',
		'bench_count' => '',
		'stats_recorded' => ''
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/competitions';

	// Set the row item links.
	protected $_search_row_links = array
	(
		'<span class="glyphicon glyphicon-pencil"></span>' => 'edit/%id%',
		'<span class="glyphicon glyphicon-remove"></span>' => 'delete/%id%'
	);

	// Set the columns to return in a search.
	protected $_search_titles = array
	(
		'name' => 'Name',
		'description' => 'Description',
		'bench_count' => 'Bench Count',
		'stats_recorded' => 'Statistics recorded?'
	);

	// Set the workflow name (plural).
	protected $_workflow = 'competitions';

	public function search($page = NULL)
	{
		parent::search($page);
		foreach ($this->layout->blocks['body']->rows as &$row)
		{
			$row['stats_recorded'] = ($row['stats_recorded'] === 1 ? 'Y' : 'N');
		}
	}

	public function view($id)
	{
		// TODO: Load a given Competition for viewing
	}

} // End Competitions Controller
