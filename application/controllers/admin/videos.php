<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Videos controller
 */
class Videos_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(

	);

	// Set form field values.
	protected $_form_values = array
	(
		'video_author' => '',
		'video_picture_url' => '',
		'video_picture_thumbnail_url' => '',
		'video_url' => '',
		'video_body' => '',
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/videos';

	// Set the row item links.
	protected $_search_row_links = array
	(
		'<span class="glyphicon glyphicon-pencil"></span>' => 'edit/%id%',
		'<span class="glyphicon glyphicon-remove"></span>' => 'delete/%id%'
	);

	// Set the columns to return in a search.
	protected $_search_titles = array
	(
		'video_url' => 'Video URL',
	);

	// Set the workflow name (plural).
	protected $_workflow = 'videos';

	public function add()
	{
		// Redirect to fixtures search location
		url::redirect('admin/fixtures/search/');
	}

	public function delete($id = NULL)
	{
		// Prevent deletion
		$this->_set_notification
		(
			'Sorry, you can\'t delete this ' . $this->_singular(TRUE) . '.',
			'error',
			TRUE
		);
		// Redirect to fixtures search location
		url::redirect('admin/fixtures/search/');
	}

	public function search($page = NULL)
	{
		// Redirect to fixtures search location
		url::redirect("admin/fixtures/search/$page/");
	}

	public function view($id)
	{
		// TODO: Load a given model for viewing
	}

} // End Videos Controller
