<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Manager_Notes controller
 */
class Manager_Notes_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array

	(
	);

	// Set form field values.
	protected $_form_values = array
	(
		'manager_note_author' => '',
		'manager_note_headline' => '',
		'manager_note_excerpt' => '',
		'manager_note_body' => '',
		'manager_note_picture_url' => '',
		'manager_note_picture_thumbnail_url' => '',
		'manager_note_caption' => '',
		'manager_note_quote' => '',
		'manager_note_quote_credit' => '',
		'manager_note_published' => ''
	);

	// Set the workflow name (plural).
	protected $_workflow = 'manager_notes';

	// Set the Controller root URL.
	protected $_root_controller_url = 'admin/manager_notes';

	public function add()
	{
		// Redirect to fixtures search location
		url::redirect('admin/fixtures/search/');
	}

	public function delete($id = NULL)
	{
		// Prevent deletion
		$this->_set_notification
		(
			'Sorry, you can\'t delete this ' . $this->_singular(TRUE) . '.',
			'error',
			TRUE
		);
		// Redirect to fixtures search location
		url::redirect('admin/fixtures/search/');
	}

	public function edit ($id = NULL)
	{
		// Attempt to load fixture
		$fixture = ORM::factory('fixture', $id);
		if ($fixture->loaded)
		{
			// Check whether this is an away game
			if ($fixture->is_home !== 1)
			{
				// Prevent adding a manager note for an away game
				$this->_set_notification
				(
					'You can\'t edit manager notes for an away game.',
					'error',
					TRUE
				);
				// Redirect to fixtures search location
				url::redirect('admin/fixtures/search/');
			}
		}
		// Call parent method
		parent::edit($id);
	}

	public function search($page = NULL)
	{
		// Redirect to fixtures search location
		url::redirect("admin/fixtures/search/$page/");
	}

	public function view($id)
	{
		// TODO: Load a given model for viewing
	}

} // End Manager_Notes Controller
