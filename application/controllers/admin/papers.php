<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Paper controller
 */
class Papers_Controller extends Admin_Workflow_Controller {

	// Set form field default values.
	protected $_form_defaults = array
	(
	);

	// Set form field values.
	protected $_form_values = array
	(
		'publish_date' => '',
		'headline' => '',
		'url' => '',
	);

	// Set the Controller root URL.
	protected $_root_controller_url = '/admin/papers';

	// Set the row item links.
	protected $_search_row_links = array
	(
		'<span class="glyphicon glyphicon-pencil"></span>' => 'edit/%id%',
		'<span class="glyphicon glyphicon-remove"></span>' => 'delete/%id%'
	);

	// Set the columns to return in a search.
	protected $_search_titles = array
	(
		'publish_date' => 'Date Published',
		'headline' => 'Headline',
	);
	
	// Set the column and direction to order by in a search.
	protected $_search_orderby = array
	(
		'publish_date' => 'DESC'
	);

	// Set the workflow name (plural).
	protected $_workflow = 'papers';

	public function view($id)
	{
		// TODO: Load a given model for viewing
	}

} // End Paper Controller
