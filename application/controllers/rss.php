<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * RSS controller
 */
class Rss_Controller extends Controller {

	public function reports()
	{
		$db = new Database;
		$report_data = $db->query
		(
			'
			SELECT
				fixtures.id,
				fixtures.report_excerpt as description,
				fixtures.report_headline as title,
				fixtures.match_date as pubDate
			FROM
				fixtures
			WHERE
				fixtures.report_published IS NOT NULL
			ORDER BY
				fixtures.match_date DESC
			LIMIT
				1;
			'
		);
		foreach ($report_data as $key => $data)
		{
			$items[$key]['description'] = $data->description;
			$items[$key]['title'] = $data->title;
			$items[$key]['pubDate'] = $data->pubDate;
			$items[$key]['link'] = "/reports/$data->id/";
		}
		$info = array
		(
			'title' => 'Slough Town FC Reports Feed',
			'link' => '/reports/',
			'description' => 'The latest reports from SloughTownFC.net - The Official Website of Slough Town FC',
			'language' => 'en-uk',
			'copyright' => 'Copyright ' . date('Y', time()) . ' Slough Town FC',
			'generator' => 'STFC'
		);
		echo feed::create($info, $items);
	}
	
	public function reserve_reports()
	{
		$db = new Database;
		$report_data = $db->query
		(
			'
			SELECT
				reserve_fixtures.id,
				reserve_fixtures.report_excerpt as description,
				reserve_fixtures.report_headline as title,
				reserve_fixtures.match_date as pubDate
			FROM
				reserve_fixtures
			WHERE
				reserve_fixtures.report_published IS NOT NULL
			ORDER BY
				reserve_fixtures.match_date DESC
			LIMIT
				1;
			'
		);
		foreach ($report_data as $key => $data)
		{
			$items[$key]['description'] = $data->description;
			$items[$key]['title'] = $data->title;
			$items[$key]['pubDate'] = $data->pubDate;
			$items[$key]['link'] = "/reserve-report/$data->id/";
		}
		$info = array
		(
			'title' => 'Slough Town FC Reserve Reports Feed',
			'link' => '/reserve-report/',
			'description' => 'The latest reserve reports from SloughTownFC.net - The Official Website of Slough Town FC',
			'language' => 'en-uk',
			'copyright' => 'Copyright ' . date('Y', time()) . ' Slough Town FC',
			'generator' => 'STFC'
		);
		echo feed::create($info, $items);
	}
	
	public function u18_reports()
	{
		$db = new Database;
		$report_data = $db->query
		(
			'
			SELECT
				u18s_fixtures.id,
				u18s_fixtures.report_excerpt as description,
				u18s_fixtures.report_headline as title,
				u18s_fixtures.match_date as pubDate
			FROM
				u18s_fixtures
			WHERE
				u18s_fixtures.report_published IS NOT NULL
			ORDER BY
				u18s_fixtures.match_date DESC
			LIMIT
				1;
			'
		);
		foreach ($report_data as $key => $data)
		{
			$items[$key]['description'] = $data->description;
			$items[$key]['title'] = $data->title;
			$items[$key]['pubDate'] = $data->pubDate;
			$items[$key]['link'] = "/u18-report/$data->id/";
		}
		$info = array
		(
			'title' => 'Slough Town FC Under 18 Reports Feed',
			'link' => '/u18-report/',
			'description' => 'The latest Under 18 reports from SloughTownFC.net - The Official Website of Slough Town FC',
			'language' => 'en-uk',
			'copyright' => 'Copyright ' . date('Y', time()) . ' Slough Town FC',
			'generator' => 'STFC'
		);
		echo feed::create($info, $items);
	}
	
	public function academy_reports()
	{
		$db = new Database;
		$report_data = $db->query
		(
			'
			SELECT
				academy_fixtures.id,
				academy_fixtures.report_excerpt as description,
				academy_fixtures.report_headline as title,
				academy_fixtures.match_date as pubDate
			FROM
				academy_fixtures
			WHERE
				academy_fixtures.report_published IS NOT NULL
			ORDER BY
				academy_fixtures.match_date DESC
			LIMIT
				1;
			'
		);
		foreach ($report_data as $key => $data)
		{
			$items[$key]['description'] = $data->description;
			$items[$key]['title'] = $data->title;
			$items[$key]['pubDate'] = $data->pubDate;
			$items[$key]['link'] = "/academy-report/$data->id/";
		}
		$info = array
		(
			'title' => 'Slough Town FC Academy Reports Feed',
			'link' => '/u18-report/',
			'description' => 'The latest Academy reports from SloughTownFC.net - The Official Website of Slough Town FC',
			'language' => 'en-uk',
			'copyright' => 'Copyright ' . date('Y', time()) . ' Slough Town FC',
			'generator' => 'STFC'
		);
		echo feed::create($info, $items);
	}
	

	public function news()
	{
		//Current date and time
		$date = date('Y-m-d');
		$time = date('H:i:s');

		$db = new Database;
		$report_data = $db->query
		(
			'
			SELECT
				news.id,
				news.excerpt as description,
				news.headline as title,
				news.publish_date as pubDate
			FROM
				news
			WHERE
				news.publish_date <= "' . $date . '" AND
				news.publish_time <= "' . $time . '"
			ORDER BY
				news.publish_date DESC
			LIMIT
				1;
			'
		);
		foreach ($report_data as $key => $data)
		{
			$items[$key]['description'] = $data->description;
			$items[$key]['title'] = $data->title;
			$items[$key]['pubDate'] = $data->pubDate;
			$items[$key]['link'] = "/article/$data->id";
		}
		$info = array
		(
			'title' => 'Slough Town FC Reports Feed',
			'link' => '/news/',
			'description' => 'The latest news from SloughTownFC.net - The Official Website of Slough Town FC',
			'language' => 'en-uk',
			'copyright' => 'Copyright ' . date('Y', time()) . ' Slough Town FC',
			'generator' => 'STFC'
		);
		echo feed::create($info, $items);
	}

} // End RSS controller