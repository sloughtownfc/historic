<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Admin controller
 *
 * This is extended by all controllers under /admin. The Admin
 * controller cannot be loaded directly through URI mapping as it is
 * abstract.
 *
 * Please check config/routes.php and edit the referenced /admin
 * controller to change the appearance of the default admin page.
 */
abstract class Admin_Controller extends Layout_Controller {

	// Set the default blocks array.
	public $blocks = array
	(
		'head' => 'admin_head',
		'body' => 'admin_body'
	);

	// Set the layout name.
	public $layout = 'admin';

	public function __construct()
	{
		parent::__construct();
		// Load in temporary auth (method scope only)
		$auth = Auth::instance();

		if (!$auth->logged_in('admin'))
		{
			// If not logged in, redirect to login controller.
			// The current URI is saved for redirection after the
			// login process
			$this->session->set('requested_uri', url::current());
			url::redirect('login');
		}
		else
		{
			// User is logged in with admin privileges, load their
			// information
			$this->user = $auth->get_user();
		}
	}

} // End Admin Controller
