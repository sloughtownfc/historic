<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Default controller
 */
class Default_Controller extends Layout_Controller {

	// Set the blocks array.
	public $blocks = array
	(
		'head' => 'default_head',
		'body' => 'default_body'
	);

	// Set the layout name.
	public $layout = 'default';

	public function index()
	{
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Fetch the most recent (current) season
		$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();

		// Fetch all fixtures for the given season
		$start_date = $season->start_date;
		$end_date = $season->end_date;

		// Push publish date / start date checking onto where
		$where = array('match_date >=' => $start_date);
		if ($end_date !== NULL)
		{
			// Push publish / end date checking onto where
			$where['match_date <='] = $end_date;
		}

		// Create database instance
		$db = new Database();

		// Get cache instance
		$this->cache = Cache::instance();

		// Fetch slider from cache, if set
		$slider = $this->cache->get('slider');

		if ( ! $slider)
		{
			// Current date and time
			$date = type_valid::to_date(date('Y-m-d'));
			$time = type_valid::to_time(date('H:i:s'));

			// Fetch all matches played from database
			$slider = $db->query
			(
				'(SELECT
					fixtures.id as fix_id,
					fixtures.match_date as match_date,
					fixtures.report_headline as headline,
					fixtures.report_excerpt as excerpt,
					fixtures.report_picture_url as picture_url,
					"" as publish_time,
					"fixture"
				AS
				tbl
				FROM
					fixtures
				WHERE
					fixtures.report_published = 1
				)
				UNION ALL
				(
					SELECT
						news.id as fix_id,
						news.publish_date as match_date,
						news.headline as headline,
						news.excerpt as excerpt,
						news.picture_url as picture_url,
						news.publish_time,
						"news"
				AS
				tbl
					FROM
						news
					WHERE
						news.publish_date < "' . $date . '" OR
						(news.publish_date = "' . $date . '" AND news.publish_time <= "' . $time . '")
				)
				UNION ALL
				(
					SELECT
						u18s_fixtures.id as fix_id,
						u18s_fixtures.match_date as match_date,
						u18s_fixtures.report_headline as headline,
						u18s_fixtures.report_excerpt as excerpt,
						u18s_fixtures.picture_url as picture_url,
						"" as publish_time,
					"u18"
				AS
				tbl
					FROM
						u18s_fixtures
					WHERE
						u18s_fixtures.report_published = 1
				)
				UNION ALL
				(
					SELECT
						reserve_fixtures.id as fix_id,
						reserve_fixtures.match_date as match_date,
						reserve_fixtures.report_headline as headline,
						reserve_fixtures.report_excerpt as excerpt,
						reserve_fixtures.picture_url as picture_url,
						"" as publish_time,
					"reserve"
				AS
				tbl
					FROM
						reserve_fixtures
					WHERE
						reserve_fixtures.report_published = 1
				)
				UNION ALL
				(
					SELECT
						academy_fixtures.id as fix_id,
						academy_fixtures.match_date as match_date,
						academy_fixtures.report_headline as headline,
						academy_fixtures.report_excerpt as excerpt,
						academy_fixtures.picture_url as picture_url,
						"" as publish_time,
					"academy"
				AS
				tbl
					FROM
						academy_fixtures
					WHERE
						academy_fixtures.report_published = 1
				)
				ORDER BY
					match_date DESC, publish_time DESC
				LIMIT 7;'
			)->result_array(NULL);

			// Set in cache
			$this->cache->set('slider', $slider);
		}

		// Splice out recent news
		$recent_news = array_splice($slider, -3);

		$this->layout->blocks['body']->slider = $slider;
		$this->layout->blocks['body']->recent_news = $recent_news;

		$this->layout->blocks['body']->papers = ORM::factory('paper')->orderby('publish_date', 'DESC')->limit('4')->find_all();

		// get current day
		$currentday = date('d');
		//$currentday = 13;

		// get current month
		$currentmonth = date('n');
		//$currentmonth = 8;

		$currentyear = date('Y');

		// Fetch today from cache, if set
		$today = $this->cache->get('today');

		if ( ! $today)
		{
			// Fetch all matches played
			$today = $db->query
			(
				'SELECT fixtures.id, fixtures.match_date, fixtures.opposition_id, fixtures.competition_id, fixtures.is_home, fixtures.report_home_score, fixtures.report_away_score, oppositions.name FROM fixtures, oppositions WHERE DAY (fixtures.match_date) = "' . $currentday . '" AND MONTH (fixtures.match_date) = "' . $currentmonth . '" AND YEAR (fixtures.match_date) <> "' . $currentyear . '" AND fixtures.opposition_id = oppositions.id ORDER BY fixtures.match_date DESC;'
			)->result_array(NULL);

			// Set in cache
			$this->cache->set('today', $today);
		}

		$this->layout->blocks['body']->today = $today;
	}

	/**
	 * The 500 club.
	 */
	public function page_500_club()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'The 500 club';
		// Set the page body
		$this->_load_block('body', 'pages/500_club');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * STFC Academy.
	 */
	public function page_academy()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Slough Town Football Club Academy';
		// Set the page body
		$this->_load_block('body', 'pages/academy');

		// Create an error array
		$errors = array();

		// Create the captcha object
		$captcha = new Captcha;

		// Check whether the user has exceeded the CAPTCHA threshold
		if ($captcha->invalid_count() > 20)
		{
			// Redirect to root
			url::redirect('/');
		}

		// Check whether a message has been submitted
		$message_data = array
		(
			'name' => NULL,
			'return_email' => NULL,
			'dob' => NULL,
			'phone' => NULL,
			'trial_date' => NULL,
			'school' => NULL,
			'position' => NULL,
			'captcha_response' => NULL
		);
		if ($this->input->post('submit'))
		{
			// Message has been submitted
			// Fetch $_POST data
			$post = $this->input->post();
			$message_data = arr::extract
			(
				$post,
				array_keys($message_data)
			);

			$message = ORM::factory('academy');
			if ($message->validate($message_data))
			{
				// Check the captcha is valid
				if (Captcha::valid($post['captcha_response']))
				{
					// Successful validation, save!
					$message->save();
					// Send e-mail
					$headers = 	'From:' . 'postmaster@sloughtownfc.net';

					mail
					(
						$message->return_email,
						"SloughTownFC.net Online Message - Academy Trial Registration Confirmation",
						$message->name . ",\n\nThank for registering for a trial with Slough Town FC Academy for the 2016/17 season. We look forward to seeing you on your selected trial date of " . $message->trial_date . ". Trials will begin at 10.00am and last 2 and half hours, ending at 12.30pm. \n\nMark Betts\n\nSTFC Academy Manager ",
						$headers
					);

					mail
					(
						"academy@sloughtownfc.net",
						"SloughTownFC.net Online Message - Academy Trial Registration Confirmation",
						"There has been an new Academy Trial Registration, defails below:\n\n Name: " . $message->name . "\n\n Trial date: " . $message->trial_date . "\n\n Playing Position: " . $message->position . "\n\n DOB: " . $message->dob . "\n\n School Attending: " . $message->school . "\n\n Phone: " . $message->phone ."\n\n Email: " . $message->return_email,
						$headers
					);

					// Flash success notification
					$this->_set_notification
					(
						'Thank you, your Academy trial registration has been received.',
						'success',
						TRUE
					);
					// Redirect to root
					url::redirect('/academy');
				}
				else
				{
					// Set validation errors and CAPTCHA error message
					$errors = $message_data->errors();
					$errors['captcha'] = 'CAPTCHA check failed!';
					// Flash error notification
					$this->_set_notification
					(
						'There was an error processing your registration.',
						'error'
					);
				}
			}
			else
			{
				// Set validation errors
				$errors = $message_data->errors();
				// Flash error notification
				$this->_set_notification
				(
					'There was an error processing your registration.',
					'error'
				);
			}
			// Set message data to an array
			$message_data = $message_data->as_array();
		}

		// Send to view
		$this->layout->blocks['body']->captcha = $captcha->render();
		$this->layout->blocks['body']->errors = $errors;
		$this->layout->blocks['body']->message_data = $message_data;

	}
	
	/**
	 * Academy Fixtures page.
	 */
	public function page_academy_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Academy Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/academy_fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Fetch all seasons as select list
		$this->layout->blocks['body']->seasons = ORM::factory('season')->where('start_date>=','2013-07-01')->orderby('start_date', 'DESC')->select_list('id', 'name');
		if (($season = $this->input->post('season')) && valid::digit($season) && ($season > 0))
		{
			// Find season matching the given id
			$season = ORM::factory('season')->where('id', $season)->find();
		}
		else
		{
			// Fetch the most recent (current) season
			$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();
		}
		// Send through season name to the view
		$this->layout->blocks['body']->season_name = $season->name;
		// Send through selected season to the view
		$this->layout->blocks['body']->season_selected = $season->id;

		// Fetch all fixtures for the given season
		$start_date = $season->start_date;
		$end_date = $season->end_date;

		// Push match date / start date checking onto where
		$where = array('match_date >=' => $start_date);
		if ($end_date !== NULL)
		{
			// Push match date / end date checking onto where
			$where['match_date <='] = $end_date;
		}
		// Create orwhere for TBC matches
		$orwhere = array('match_date' => NULL);
		// Fetch all fixtures between the start and end dates
		$fixtures = ORM::factory('academy_fixture')
					->where($where)
					->orwhere($orwhere)
					->orderby('match_date', 'ASC')
					->find_all();

		// Group fixtures by month
		$grouped_fixtures = array();
		$tbc_fixtures = array();
		foreach ($fixtures as $fixture)
		{
			$match_date = $fixture->match_date;
			// Assumes date delimiter to be '-'
			if (($match_date !== NULL) && $match_date = explode('-', $match_date))
			{
					// Get month number
					$month = $match_date[1];
					// Get month name
					$month = date('F', mktime(0, 0, 0, $month));
					$grouped_fixtures[$month][] = $fixture;
			}
			else
			{
				// Add to TBC fixture list
				$tbc_fixtures[] = $fixture;
			}
		}
		// Apend the TBC list to the sorted fixtures list
		$grouped_fixtures['TBC'] = $tbc_fixtures;

		// Fetch the most recent (current) season
		$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();

		// Pass to the view
		$this->layout->blocks['body']->season = $season;
		$this->layout->blocks['body']->grouped_fixtures = $grouped_fixtures;
	}

	/**
	 * Academy Report page.
	 */
	public function page_academy_report($id = NULL)
	{
		// Check article id is a valid positive digit
		if (!valid::digit($id) || $id < 1)
		{
			// Redirect to current news page
			url::redirect('/academy-fixtures');
		}

		// Fetch the article
		$academy = ORM::factory('academy_fixture', $id);

		// Check news loaded
		if (!$academy->loaded)
		{
			// Redirect to current news page
			url::redirect('/academy-fixtures');
		}

		// Generate variables
		$headline = $academy->report_headline;
		$is_home = $academy->is_home;
		$match_date = $academy->match_date;
		$opposition = $academy->opposition;
		$home_score = $academy->report_home_score;
		$away_score = $academy->report_away_score;
		$home_scorers = $academy->report_home_scorers;
		$away_scorers = $academy->report_home_scorers;
		$competition = $academy->competition;
		$body = explode("\n\n", $academy->report_body);
		$report_team = $academy->report_team;

		// Set the title
		$this->layout->blocks['head']->title = $headline;
		// Set the page body
		$this->_load_block('body', 'pages/other_report');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Pass to view
		$this->layout->blocks['body']->headline = $headline;
		$this->layout->blocks['body']->is_home = $is_home;
		$this->layout->blocks['body']->match_date = $match_date;
		$this->layout->blocks['body']->opposition = $opposition;
		$this->layout->blocks['body']->home_score = $home_score;
		$this->layout->blocks['body']->away_score = $away_score;
		$this->layout->blocks['body']->home_scorers = $home_scorers;
		$this->layout->blocks['body']->away_scorers = $away_scorers;
		$this->layout->blocks['body']->opposition = $opposition;
		$this->layout->blocks['body']->competition = $competition;
		$this->layout->blocks['body']->body = $body;
		$this->layout->blocks['body']->report_team = $report_team;
	}

	/**
	 * All-time records
	 */
	public function page_all_time()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'All Time Records';
		// Set the page body
		$this->_load_block('body', 'pages/all_time');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Create database instance
		$db = new Database();
		// Fetch stats for all players who have made an appearance this season
		$goals = $db->query
		(
			'SELECT
				sum( stats.appearance ) AS appearances,
				sum( stats.goals ) AS goals,
				stats.player_id,
				players.first_name,
				players.last_name
			FROM
				players
			INNER JOIN stats ON players.id = stats.player_id
			WHERE
				stats.appearance =1
			GROUP BY
				stats.player_id,
				players.first_name,
				players.last_name
			ORDER BY sum( stats.goals ) DESC
			LIMIT 20 ;'
		);

		// Fetch stats for all players who have made an appearance this season
		$apps = $db->query
		(
			'SELECT
				sum( stats.appearance ) AS appearances,
				sum( stats.goals ) AS goals,
				stats.player_id,
				players.first_name,
				players.last_name
			FROM
				players
			INNER JOIN stats ON players.id = stats.player_id
			WHERE
				stats.appearance =1
			GROUP BY
				stats.player_id,
				players.first_name,
				players.last_name
			ORDER BY sum( stats.appearance ) DESC
			LIMIT 20 ;'
		);

		// Pass to view
		$this->layout->blocks['body']->apps = $apps;
		$this->layout->blocks['body']->goals = $goals;

	}

	/**
	 * Away Directions page.
	 */
	public function page_away_directions()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Away Directions';
		// Set the page body
		$this->_load_block('body', 'pages/away_directions');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Fetch the most recent (current) season
		$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();

		// Fetch all fixtures for the given season
		$start_date = $season->start_date;
		$end_date = $season->end_date;

		// Create database instance
		$db = new Database();

		// Fetch all matches played
		$oppo = $db->query
		(
			'SELECT DISTINCT
				oppositions.id,
				oppositions.name,
				oppositions.grounds_venue,
				fixtures.opposition_id,
				fixtures.is_home
			FROM
				oppositions,fixtures
			WHERE
				fixtures.match_date >= "' . $start_date . '" AND
				fixtures.is_home IS NULL  AND
				fixtures.opposition_id = oppositions.id
			ORDER BY
			oppositions.name;'
		);

		$this->layout->blocks['body']->oppo = $oppo;
	}

	/**
	 * Article page.
	 */
	public function page_article($id = NULL)
	{
		// Check article id is a valid positive digit
		if (!valid::digit($id) || $id < 1)
		{
			// Redirect to current news page
			url::redirect('/news');
		}

		// Fetch the article
		$news = ORM::factory('news', $id);

		// Check news loaded
		if (!$news->loaded)
		{
			// Redirect to current news page
			url::redirect('/news');
		}

		// Generate variables
		$headline = $news->headline;
		$excerpt = $news->excerpt;
		$author = $news->author;
		$publish_date = $news->publish_date;
		$caption = $news->caption;
		$body = explode("\n\n", $news->body);
		$video_url = $news->video_url;
		$audio_url = $news->audio_url;
		$small_picture_url = $news->small_picture_url;
		$quote = $news->quote;
		$quote_by = $news->quote_by;

		// Set the title
		$this->layout->blocks['head']->title = $headline;
		// Set the page body
		$this->_load_block('body', 'pages/article');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Pass to view
		$this->layout->blocks['body']->headline = $headline;
		$this->layout->blocks['body']->excerpt = $excerpt;
		$this->layout->blocks['body']->author = $author;
		$this->layout->blocks['body']->publish_date = $publish_date;
		$this->layout->blocks['body']->caption = $caption;
		$this->layout->blocks['body']->body = $body;
		$this->layout->blocks['body']->video_url = $video_url;
		$this->layout->blocks['body']->audio_url = $audio_url;
		$this->layout->blocks['body']->small_picture_url = $small_picture_url;
		$this->layout->blocks['body']->quote = $quote;
		$this->layout->blocks['body']->quote_by = $quote_by;
	}

	/**
	 * Attendance table.
	 */
	public function page_attendance_table()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Attendance Table';
		// Set the page body
		$this->_load_block('body', 'pages/attendance_table');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * Away league table page.
	 */
	public function page_away_league_table()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Away League Table';
		// Set the page body
		$this->_load_block('body', 'pages/away_league_table');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * Contacts page.
	 */
	public function page_contacts()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Contacts';
		// Set the page body
		$this->_load_block('body', 'pages/contacts');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * Current Players page.
	 */
	public function page_current_players()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Squad';
		// Set the page body
		$this->_load_block('body', 'pages/current_players');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Fetch positions
		$positions = ORM::factory('position')->find_all();
		// Fetch players, ordered by last name
		$players = 	ORM::factory('player')
					->where('active', 1)
					->orderby('last_name', 'ASC')
					->find_all();
		// Group players by position
		$grouped_players = array();
		foreach ($players as $player)
		{
			$grouped_players[$player->position_id][] = $player;
		}
		// Pass to the view
		$this->layout->blocks['body']->grouped_players = $grouped_players;
		$this->layout->blocks['body']->positions = $positions;
	}

	/**
	 * Directions page.
	 */
	public function page_directions($id = NULL)
	{
		// Check article id is a valid positive digit
		if (!valid::digit($id) || $id < 1)
		{
			// Redirect to current news page
			url::redirect('/away-directions');
		}

		// Fetch the opposition
		$directions = ORM::factory('opposition', $id);

		// Check news loaded
		if (!$directions->loaded)
		{
			// Redirect to current news page
			url::redirect('/away-directions');
		}

		// Generate variables
		$opposition = $directions->name;
		$website = $directions->website;
		$logo = $directions->logo;
		$grounds_venue = $directions->grounds_venue;
		$grounds_address = $directions->grounds_address;
		$grounds_telephone = $directions->grounds_telephone;
		$grounds_directions = $directions->grounds_directions;
		$grounds_google = $directions->grounds_google;

		// Set the title
		$headline = "Directions to $opposition";
		$this->layout->blocks['head']->title = $headline;
		// Set the page body
		$this->_load_block('body', 'pages/directions');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Pass to view
		$this->layout->blocks['body']->opposition = $opposition;
		$this->layout->blocks['body']->website = $website;
		$this->layout->blocks['body']->logo = $logo;
		$this->layout->blocks['body']->grounds_venue = $grounds_venue;
		$this->layout->blocks['body']->grounds_address = $grounds_address;
		$this->layout->blocks['body']->grounds_telephone = $grounds_telephone;
		$this->layout->blocks['body']->grounds_directions = $grounds_directions;
		$this->layout->blocks['body']->grounds_google = $grounds_google;
	}

	/**
	* Fixtures page.
	*/
	public function page_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
		// Fetch all seasons as select list
		$this->layout->blocks['body']->seasons = ORM::factory('season')->orderby('start_date', 'DESC')->select_list('id', 'name');
		if (($season = $this->input->post('season')) && valid::digit($season) && ($season > 0))
		{
			// Find season matching the given id
			$season = ORM::factory('season')->where('id', $season)->find();
		}
		else
		{
			// Fetch the most recent (current) season
			$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();
		}
		// Send through season name to the view
		$this->layout->blocks['body']->season_name = $season->name;
		// Send through selected season to the view
		$this->layout->blocks['body']->season_selected = $season->id;

		// Fetch all fixtures for the given season
		$start_date = $season->start_date;
		$end_date = $season->end_date;
		// Push match date / start date checking onto where
		$where = array('match_date >=' => $start_date);
		if ($end_date !== NULL)
		{
			// Push match date / end date checking onto where
			$where['match_date <='] = $end_date;
		}
		// Create orwhere for TBC matches
		$orwhere = array('match_date' => NULL);
		// Fetch all fixtures between the start and end dates
		$fixtures = ORM::factory('fixture')
					->where($where)
					->orwhere($orwhere)
					->orderby('match_date', 'ASC')
					->find_all();

		// Group fixtures by month
		$grouped_fixtures = array();
		$tbc_fixtures = array();
		foreach ($fixtures as $fixture)
		{
			$match_date = $fixture->match_date;
			// Assumes date delimiter to be '-'
			if (($match_date !== NULL) && $match_date = explode('-', $match_date))
			{
					// Get month number
					$month = $match_date[1];
					// Get month name
					$month = date('F', mktime(0, 0, 0, $month));
					$grouped_fixtures[$month][] = $fixture;
			}
			else
			{
				// Add to TBC fixture list
				$tbc_fixtures[] = $fixture;
			}
		}
		// Apend the TBC list to the sorted fixtures list
		$grouped_fixtures['TBC'] = $tbc_fixtures;

		// Fetch the most recent (current) season
		$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();

		// Pass to the view
		$this->layout->blocks['body']->season = $season;
		$this->layout->blocks['body']->grouped_fixtures = $grouped_fixtures;
	}

	/**
	 * Gallerific page.
	 */
	public function page_gallery($return_el = '')
	{
		// Get the root gallery URL
		$root_url = Kohana::config('gallery.root_url', TRUE);
		if ($this->input->post('submit') && $this->input->post('search'))
		{
			$search = $this->input->post('search');
			// Fetch all tags that match the search
			$tag = ORM::factory('tag')
			       ->like('name', '%' . $search . '%', FALSE)
			       ->find();
			// Fetch all gallery images with the matching tag
			$gallery_images = $tag->gallery_images;
			// Check some gallery images were found
			if ($gallery_images->count() == 0)
			{
				// Fetch all gallery images
				$gallery_images = ORM::factory('gallery_image')->find_all();
			}
		}
		else
		{
			// Fetch all gallery images
			$gallery_images = ORM::factory('gallery_image')->find_all();
		}
		// Overwrite the view
		$this->layout = View::factory('galleriffic');
		$this->layout->root_url = $root_url;
		$this->layout->gallery_images = $gallery_images;
		$this->layout->return_el = $return_el;
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * Get In Touch Page.
	 */
	public function page_getintouch()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Contact Us at Slough Town FC';
		// Set the page body
		$this->_load_block('body', 'pages/getintouch');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Create an error array
		$errors = array();

		// Create the captcha object
		$captcha = new Captcha;

		// Check whether the user has exceeded the CAPTCHA threshold
		if ($captcha->invalid_count() > 20)
		{
			// Redirect to root
			url::redirect('/');
		}

		// Check whether a message has been submitted
		$message_data = array
		(
			'name' => NULL,
			'return_email' => NULL,
			'contact_id' => NULL,
			'subject' => NULL,
			'body' => NULL,
			'captcha_response' => NULL
		);
		if ($this->input->post('submit'))
		{
			// Message has been submitted
			// Fetch $_POST data
			$post = $this->input->post();
			$message_data = arr::extract
			(
				$post,
				array_keys($message_data)
			);

			$message = ORM::factory('message');
			if ($message->validate($message_data))
			{
				// Check the captcha is valid
				if (Captcha::valid($post['captcha_response']))
				{
					// Successful validation, save!
					$message->save();
					// Send e-mail
					$headers = 	'From:' . 'postmaster@sloughtownfc.net' . "\n" .
								'Reply-To:' . $message->return_email . "\n";
					mail
					(
						$message->contact->email,
						"SloughTownFC.net Online Message: " . $message->subject,
						"From: " . $message->name . "\n\n" . $message->body . "\n\n Reply-To: " . $message->return_email . "\n IP: " . $_SERVER['REMOTE_ADDR'],
						$headers
					);
					// Flash success notification
					$this->_set_notification
					(
						'Thank you, your message has been sent.',
						'success',
						TRUE
					);
					// Redirect to root
					url::redirect('/getintouch');
				}
				else
				{
					// Set validation errors and CAPTCHA error message
					$errors = $message_data->errors();
					$errors['captcha'] = 'CAPTCHA check failed!';
					// Flash error notification
					$this->_set_notification
					(
						'There was an error processing your message.',
						'error'
					);
				}
			}
			else
			{
				// Set validation errors
				$errors = $message_data->errors();
				// Flash error notification
				$this->_set_notification
				(
					'There was an error processing your message.',
					'error'
				);
			}
			// Set message data to an array
			$message_data = $message_data->as_array();
		}

		// Fetch contact list
		$contacts = ORM::factory('contact')
						->select_list('id', 'name');

		// Send to view
		$this->layout->blocks['body']->contacts = $contacts;
		$this->layout->blocks['body']->captcha = $captcha->render();
		$this->layout->blocks['body']->errors = $errors;
		$this->layout->blocks['body']->message_data = $message_data;
	}

	/**
	 * Goals scored table page.
	 */
	public function page_goals_scored_table()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Goals Scored Table';
		// Set the page body
		$this->_load_block('body', 'pages/goals_scored_table');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * Ground Regs page.
	 */
	public function page_groundregs()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Arbour Park Ground Regulations';
		// Set the page body
		$this->_load_block('body', 'pages/ground_regs');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * Home league table page.
	 */
	public function page_home_league_table()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Home League Table';
		// Set the page body
		$this->_load_block('body', 'pages/home_league_table');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * Honours page.
	 */
	public function page_honours()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Club Honours';
		// Set the page body
		$this->_load_block('body', 'pages/honours');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * How To Find Us page.
	 */
	public function page_how_to_find_us()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Directions to Slough Town FC';
		// Set the page body
		$this->_load_block('body', 'pages/how_to_find_us');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * League table page.
	 */
	public function page_league_table()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'League Table';
		// Set the page body
		$this->_load_block('body', 'pages/league_table');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * Links page.
	 */
	public function page_links()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'External Links';
		// Set the page body
		$this->_load_block('body', 'pages/links');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * Manager Notes page.
	 */
	public function page_manager_notes()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Manager Notes';
		// Set the page body
		$this->_load_block('body', 'pages/manager_notes');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
		// Fetch all seasons as select list
		$this->layout->blocks['body']->seasons = ORM::factory('season')->where('start_date>=','2009-07-01')->orderby('start_date', 'DESC')->select_list('id', 'name');
		if (($season = $this->input->post('season')) && valid::digit($season) && ($season > 0))
		{
			// Find season matching the given id
			$season = ORM::factory('season')->where('id', $season)->find();
		}
		else
		{
			// Fetch the most recent (current) season
			$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();
		}
		// Send through season name to the view
		$this->layout->blocks['body']->season_name = $season->name;
		// Send through selected season to the view
		$this->layout->blocks['body']->season_selected = $season->id;

		// Fetch all fixtures for the given season
		$start_date = $season->start_date;
		$end_date = $season->end_date;
		// Push match date / start date checking onto where
		$where = array('match_date >=' => $start_date);
		if ($end_date !== NULL)
		{
			// Push match date / end date checking onto where
			$where['match_date <='] = $end_date;
		}
		// Fetch all Fixtures between the start and end dates
		$this->layout->blocks['body']->fixtures = ORM::factory('fixture')
													->where($where)
													->where('is_home', '1')
													->where('competition_id !=', '1')
													->where('manager_note_published', '1')
													->orderby('match_date', 'DESC')
													->find_all();
	}

	/**
	 * Messageboards page.
	 */
	public function page_messageboards()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Messageboards';
		// Set the page body
		$this->_load_block('body', 'pages/messageboards');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * News page.
	 */
	public function page_news()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'News';
		// Set the page body
		$this->_load_block('body', 'pages/news');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Fetch all seasons as select list
		$this->layout->blocks['body']->seasons = ORM::factory('season')->where('start_date>=','2007-07-01')->orderby('start_date', 'DESC')->select_list('id', 'name');
		if (($season = $this->input->post('season')) && valid::digit($season) && ($season > 0))
		{
			// Find season matching the given id
			$season = ORM::factory('season')->where('id', $season)->find();
		}
		else
		{
			// Fetch the most recent (current) season
			$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();
		}
		// Send through season name to the view
		$this->layout->blocks['body']->season_name = $season->name;
		// Send through selected season to the view
		$this->layout->blocks['body']->season_selected = $season->id;

		// Fetch all fixtures for the given season
		$start_date = $season->start_date;
		$end_date = $season->end_date;

		// Push publish date / start date checking onto where
		$where = array('publish_date >=' => $start_date);
		if ($end_date !== NULL)
		{
			// Push publish / end date checking onto where
			$where['publish_date <='] = $end_date;
		}

		//Current date and time
		$date = type_valid::to_date(date('Y-m-d'));
		$time = type_valid::to_time(date('H:i:s'));

		// Fetch all News items between
		$this->layout->blocks['body']->news = ORM::factory('news')
													->where($where)
													->where('publish_date < "' . $date . '" OR (publish_date = "' . $date . '" AND publish_time <= "' . $time . '")')
													->orderby('publish_date', 'DESC')
													->find_all();
	}

	/**
	 * Note page.
	 */
	public function page_note($id = NULL)
	{
		// Check article id is a valid positive digit
		if (!valid::digit($id) || $id < 1)
		{
			// Redirect to current news page
			url::redirect('/manager-notes');
		}

		// Fetch the article
		$note = ORM::factory('fixture', $id);

		// Check fixture loaded
		if (!$note->loaded)
		{
			// Redirect to current news page
			url::redirect('/manager-notes');
		}

		// Generate headline
		$headline = $note->manager_note_headline;
		$author = $note->manager_note_author;
		$match_date = $note->match_date;
		$manager_note_picture_thumbnail_url = $note->manager_note_picture_thumbnail_url;
		$body = explode("\n\n", $note->manager_note_body);
		$caption = $note->manager_note_caption;
		$quote = $note->manager_note_quote;
		$quote_by = $note->manager_note_quote_credit;

		// Set the title
		$this->layout->blocks['head']->title = $headline;
		// Set the page body
		$this->_load_block('body', 'pages/note');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Pass to view
		$this->layout->blocks['body']->headline = $headline;
		$this->layout->blocks['body']->author = $author;
		$this->layout->blocks['body']->match_date = $match_date;
		$this->layout->blocks['body']->manager_note_picture_thumbnail_url = $manager_note_picture_thumbnail_url;
		$this->layout->blocks['body']->body = $body;
		$this->layout->blocks['body']->caption = $caption;
		$this->layout->blocks['body']->quote = $quote;
		$this->layout->blocks['body']->quote_by = $quote_by;

	}

	/**
	 * On This Day.
	 */
	public function page_on_this_day()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'On This Day';
		// Set the page body
		$this->_load_block('body', 'pages/on_this_day');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Create database instance
		$db = new Database();

		// get current day
		$currentday = date('d');
		//$currentday = 13;

		// get current month
		$currentmonth = date('n');
		//$currentmonth = 8;

		$currentyear = date('Y');

		// Fetch all matches played
		$today = $db->query
		(
			'SELECT fixtures.id, fixtures.match_date, fixtures.opposition_id, fixtures.competition_id, fixtures.is_home, fixtures.report_home_score, fixtures.report_away_score, oppositions.name, competitions.name as league FROM fixtures, oppositions, competitions WHERE DAY (fixtures.match_date) = "' . $currentday . '" AND MONTH (fixtures.match_date) = "' . $currentmonth . '" AND YEAR (fixtures.match_date) <> "' . $currentyear . '" AND fixtures.opposition_id = oppositions.id AND fixtures.competition_id = competitions.id ORDER BY fixtures.match_date DESC;'
		)->result_array(NULL);

		$this->layout->blocks['body']->today = $today;
	}

	/**
	 * Opposition Database page.
	 */
	public function page_opposition_database($page = NULL)
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Opposition Database';
		// Set the page body
		$this->_load_block('body', 'pages/opposition_database');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		if ($this->input->post('submit') && $this->input->post('search'))
		{
			// Fetch pagination configuration into array
			$config = Kohana::Config('pagination.opposition_database', TRUE);
			// Set pagination total_items using count
			$config['total_items'] = 1;
			// Calculate limit and offset for pagination
			$limit = $config['items_per_page'];
			$offset = $config['items_per_page'];
			// Create new pagination object
			$pag = new Pagination;
			$pagination = $pag->Factory($config);

			$search = $this->input->post('search');
			// Fetch all opposition that match the search
			$opposition = ORM::factory('opposition')->like('name', '%' . $search . '%', FALSE)->find_all();

			// Check some gallery images were found
			if (count($opposition) === 0)
			{
				// Redirect if an empty set is returned
				url::redirect('/opposition-database/1');
			}
		}
		else
		{
			// Check page number is a valid positive digit
			if (!valid::digit($page) || $page < 1)
			{
				$page = 1;
			}
			// Create a new opposition model
			$model = ORM::factory('opposition');
			// Fetch pagination configuration into array
			$config = Kohana::Config('pagination.opposition_database', TRUE);
			// Set pagination total_items using count
			$config['total_items'] = $model->count_all();
			// Calculate limit and offset for pagination
			$limit = $config['items_per_page'];
			$offset = $config['items_per_page'] * ($page - 1);
			// Create new pagination object
			$pag = new Pagination;
			$pagination = $pag->Factory($config);

			// Fetch all section of items with last_name ordering
			$opposition = $model->orderby('name', 'ASC')->find_all($limit, $offset);

			if (count($opposition) === 0 && ($page > 1))
			{
				// Redirect if an empty set is returned
				url::redirect('/opposition-database/1');
			}
		}

		// Pass to view
		$this->layout->blocks['body']->pagination = $pagination;
		$this->layout->blocks['body']->opposition = $opposition;
	}

	/**
	 * Opposition page.
	 */
	public function page_opposition($id = NULL)
	{
		// Check article id is a valid positive digit
		if (!valid::digit($id) || $id < 1)
		{
			// Redirect to current news page
			url::redirect('/opposition_database');
		}

		// Fetch the opposition
		$oppo = ORM::factory('opposition', $id);

		// Check opposition loaded
		if (!$oppo->loaded)
		{
			// Redirect to current news page
			url::redirect('/opposition_database');
		}

		// Generate variables
		$id = $oppo->id;
		$opposition = $oppo->name;
		$logo = $oppo->logo;

		// Set the title
		$headline = "$opposition";
		$this->layout->blocks['head']->title = $headline;
		// Set the page body
		$this->_load_block('body', 'pages/opposition');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Fetch all fixtures between the start and end dates
		$fixtures = ORM::factory('fixture')
					->where('opposition_id', $id)
					->orderby('match_date', 'DESC')
					->find_all();

		// Count games
		$total_games = count($fixtures);

		$win = 0;
		$draw = 0;
		$lose = 0;
		$goals_for = 0;
		$goals_against = 0;

		foreach ($fixtures as $fixture)
		{
			if (($fixture->is_home == 1 && $fixture->report_home_score > $fixture->report_away_score) || ($fixture->is_home != 1 && $fixture->report_home_score < $fixture->report_away_score))
			{
				$win++;
			}

			if ($fixture->report_home_score == $fixture->report_away_score)
			{
				$draw++;
			}

			if (($fixture->is_home == 1 && $fixture->report_home_score < $fixture->report_away_score) || ($fixture->is_home != 1 && $fixture->report_home_score > $fixture->report_away_score))
			{
				$lose++;
			}

			if ($fixture->is_home == 1 && $fixture->report_home_score)
			{
				$goals_for = $goals_for + $fixture->report_home_score;
			}
			elseif ($fixture->is_home != 1 && $fixture->report_away_score)
			{
				$goals_for = $goals_for + $fixture->report_away_score;
			}

			if ($fixture->is_home == 1 && $fixture->report_away_score)
			{
				$goals_against = $goals_against + $fixture->report_away_score;
			}
			elseif ($fixture->is_home != 1 && $fixture->report_home_score)
			{
				$goals_against = $goals_against + $fixture->report_home_score;
			}

		}

		// Pass to view
		$this->layout->blocks['body']->opposition = $opposition;
		$this->layout->blocks['body']->logo = $logo;
		$this->layout->blocks['body']->total_games = $total_games;

		$this->layout->blocks['body']->win = $win;
		$this->layout->blocks['body']->draw = $draw;
		$this->layout->blocks['body']->lose = $lose;
		$this->layout->blocks['body']->goals_for = $goals_for;
		$this->layout->blocks['body']->goals_against = $goals_against;
		$this->layout->blocks['body']->fixtures = $fixtures;

	}

	/**
	 * Player Database page.
	 */
	public function page_player_database($page = NULL)
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Player Database';
		// Set the page body
		$this->_load_block('body', 'pages/player_database');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		if ($this->input->post('submit') && $this->input->post('search'))
		{
			// Fetch pagination configuration into array
			$config = Kohana::Config('pagination.player_database', TRUE);
			// Set pagination total_items using count
			$config['total_items'] = 1;
			// Calculate limit and offset for pagination
			$limit = $config['items_per_page'];
			$offset = $config['items_per_page'];
			// Create new pagination object
			$pag = new Pagination;
			$pagination = $pag->Factory($config);

			$search = $this->input->post('search');

			$query = str_word_count($search);

			if($query == 1)
			{
				$players = ORM::factory('player')->like('first_name', $search . '%' , FALSE)->orlike('last_name', '%' . $search , FALSE)->find_all();
			}
			else
			{
				$name = explode(" ", $search);

				// Fetch all opposition that match the search
				$players = ORM::factory('player')->where(array('first_name' => $name[0], 'last_name' => $name[1]))->find_all();
			}

			// Check some gallery images were found
			if (count($players) === 0)
			{
				// Redirect if an empty set is returned
				url::redirect('/player-database/1');
			}
		}
		else
		{
			// Check page number is a valid positive digit
            if (!valid::digit($page) || $page < 1)
            {
                $page = 1;
            }
            // Create a new player model
            $model = ORM::factory('player');
            // Fetch pagination configuration into array
            $config = Kohana::Config('pagination.player_database', TRUE);
            // Set pagination total_items using count
            $config['total_items'] = $model->count_all();
            // Calculate limit and offset for pagination
            $limit = $config['items_per_page'];
            $offset = $config['items_per_page'] * ($page - 1);
            // Create new pagination object
			$pag = new Pagination;
			$pagination = $pag->Factory($config);

            // Fetch all section of items with last_name ordering
            $players = $model->orderby('last_name', 'ASC')->find_all($limit, $offset);

            if (count($players) === 0 && ($page > 1))
            {
                // Redirect if an empty set is returned
                url::redirect('/player-database/1');
            }
		}
		// Pass to view
		$this->layout->blocks['body']->pagination = $pagination;
		$this->layout->blocks['body']->players = $players;
	}
	
	/**
	 * Social Media Guidelines
	 */
	public function page_social_guidelines()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Slough Town FC and Social Media Guidelines';
		// Set the page body
		$this->_load_block('body', 'pages/social_guidelines');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * Statistics page.
	 */
	public function page_player_statistics()
	{
		// Fetch current (chronologically most recent by start date) season
		$current_season = ORM::factory('season')
							->orderby('start_date', 'DESC')
							->find();
		// Create database instance
		$db = new Database();
		// Fetch stats for all players who have made an appearance this season
		$stats = $db->query
		(
			'SELECT
				stats.appearance,
				stats.bookings,
				stats.goals,
				stats.player_id,
				players.active,
				players.first_name,
				players.last_name
			FROM
				fixtures, players, stats
			WHERE
				stats.appearance = 1 AND
				stats.fixture_id = fixtures.id AND
				stats.player_id = players.id AND
				fixtures.match_date >= "' . $current_season->start_date . '"
			ORDER BY
				players.last_name ASC;'
		);
		// Create player statistics array
		$player_stats = array();

		foreach ($stats as $stat)
		{
			$player_id = $stat->player_id;
			if (isset($player_stats[$player_id]))
			{
				// Increment player stats
				$player_stats[$player_id]['appearances']++;
				$player_stats[$player_id]['goals'] += $stat->goals;
				if ($stat->bookings == 2)
				{
					$player_stats[$player_id]['reds']++;
				}
				elseif ($stat->bookings == 1)
				{
					$player_stats[$player_id]['yellows']++;
				}
			}
			else
			{
				// Register player for the first time
				$player_stats[$player_id] = array
				(
					'name' => $stat->first_name . ' ' . $stat->last_name,
					'appearances' => $stat->appearance,
					'goals' => $stat->goals,
					'active' => $stat->active,
					'yellows' => 0,
					'reds' => 0
				);
				if ($stat->bookings == 2)
				{
					$player_stats[$player_id]['reds'] = 1;
				}
				elseif ($stat->bookings == 1)
				{
					$player_stats[$player_id]['yellows'] = 1;
				}
			}
		}



		// Set the title
		$this->layout->blocks['head']->title = 'Statistics';
		// Set the page body
		$this->_load_block('body', 'pages/player_statistics');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Pass to view
		$this->layout->blocks['body']->player_stats = $player_stats;
	}

	/**
	 * Player profile page.
	 */
	public function page_profile($id = NULL)
	{
		// Check profile id is a valid positive digit
		if (!valid::digit($id) || $id < 1)
		{
			// Redirect to current players page
			url::redirect('/current-players');
		}

		// Fetch the player
		$player = ORM::factory('player', $id);
		// Check player loaded
		if (!$player->loaded)
		{
			// Redirect to current players page
			url::redirect('/current-players');
		}

		// Create database instance
		$db = new Database();

		// Fetch all matches played
		$matches_played = $db->query
		(
			'SELECT
				competitions.name as competition_name,
				fixtures.id,
				fixtures.match_date,
				fixtures.is_home,
				fixtures.report_away_score,
				fixtures.report_home_score,
				fixtures.report_published,
				oppositions.name as opposition_name,
				stats.goals,
				stats.bookings
			FROM
				competitions, fixtures, oppositions, stats
			WHERE
				stats.player_id = ' . $player->id . ' AND
				fixtures.id = stats.fixture_id AND
				stats.appearance = 1 AND
				fixtures.competition_id = competitions.id AND
				fixtures.opposition_id = oppositions.id
			ORDER BY
				fixtures.match_date DESC;'
		)->result_array(NULL);
		// Count appearances
		$total_appearances = count($matches_played);
		// Set total goals to zero
		$total_goals = 0;
		// Check at least one appearance was returned
		if (count($matches_played) > 0)
		{
			$newDate = date("d-m-Y", strtotime($matches_played[count($matches_played)-1]->match_date));
			// Get first appearance
			$first_appearance = $newDate . ' vs ' . $matches_played[count($matches_played)-1]->opposition_name;
			// Get first goal and tally total goals
			$total_goals = 0;
			$totalcount123 = count($matches_played) - 1;
			for($i = $totalcount123; $i >= 0; $i--)
			{
				$match = $matches_played[$i];
				// Check the player scored in the match and they haven't
				// got a recorded first goal
				if (($match->goals > 0) && !isset($first_goal))
				{
					$newDate = date("d-m-Y", strtotime($match->match_date));
					$first_goal = $newDate . ' vs ' . $match->opposition_name;
				}
				$total_goals += $match->goals;
			}
			if (!isset($first_goal))
			{
				// Never scored a goal
				$first_goal = 'Never';
			}
		}
		else
		{
			// Never made an apperance, also means they never scored a goal
			$first_appearance = 'Never';
			$first_goal = 'Never';
		}

		// Generate player full name
		$player_name = $player->first_name . ' ' . $player->last_name;
		// Get player position
		$position = $player->position->name;

		// Set the title
		$this->layout->blocks['head']->title = $player_name;
		// Set the page body
		$this->_load_block('body', 'pages/profile');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Pass to view
		$this->layout->blocks['body']->player = $player;
		$this->layout->blocks['body']->player_name = $player_name;
		$this->layout->blocks['body']->position = $position;
		$this->layout->blocks['body']->matches_played = $matches_played;
		$this->layout->blocks['body']->first_appearance = $first_appearance;
		$this->layout->blocks['body']->first_goal = $first_goal;
		$this->layout->blocks['body']->total_appearances = $total_appearances;
		$this->layout->blocks['body']->total_goals = $total_goals;
	}

	/**
	 * Privacy Policy.
	 */
	public function page_privacy_policy()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Privacy Policy';
		// Set the page body
		$this->_load_block('body', 'pages/privacy_policy');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}

	/**
	 * Report page.
	 */
	public function page_report($id = NULL)
	{
		// Check article id is a valid positive digit
		if (!valid::digit($id) || $id < 1)
		{
			// Redirect to current fixtures page
			url::redirect('/fixtures');
		}

		// Fetch the article
		$report = ORM::factory('report', $id);

		// Check report loaded
		if (!$report->loaded)
		{
			// Redirect to current fixtures page
			url::redirect('/fixtures');
		}

		// Check report is published
		if (!$report->report_published)
		{
			// Redirect to current fixtures page
			url::redirect('/fixtures');
		}

		// Fetch fixture, no relation so must create new model (ugly)
		$fixture = ORM::factory('fixture', $report->id);

		// Check fixture loaded
		if (!$fixture->loaded)
		{
			url::redirect('/fixtures');
		}

		// Fetch friend competition id
		$friendly = ORM::factory('competition')->where('name', 'Friendly')->find();

		// Default to 1 if the Friendly competition cannot be found
		if ($friendly->loaded)
		{
			$friendly_id = $friendly->id;
		}
		else
		{
			$friendly_id = 1;
		}

		// Check for friendly competition
		if ($fixture->competition_id != $friendly_id)
		{
		// Fetch fixture stats bound with players
		$stats = ORM::factory('stat')
		         ->where('fixture_id', $fixture->id)
		         ->with('player')
		         ->find_all()
		         ->as_array();

		// Separate lineup and stats players
		$lineup = array_slice($stats, 0, 11);
		$subs = array_slice($stats, 11);

		// Generate variables
		$attendance = $report->report_attendance;
		$headline = $report->report_headline;
		$is_home = $fixture->is_home;
		$away_alternate_kit = $fixture->away_alternate_kit;
		$match_date = $fixture->match_date;
		$opposition = $fixture->opposition;
		$home_score = $report->report_home_score;
		$away_score = $report->report_away_score;
		$home_scorers = $report->report_home_scorers;
		$away_scorers = $report->report_away_scorers;
		$competition = $fixture->competition;
		$body = explode("\n\n", $report->report_body);
		$match_sponsor = $fixture->match_sponsor;
		$ball_sponsor = $fixture->ball_sponsor;
		$video_url = $report->video_url;
		$flickr_url = $report->report_flickr_url;
		// TODO: Rename the report_away_team column to report_opposition_team
		$opposition_team = $report->report_away_team;

		if ($is_home)
		{
			// Slough is home
			$home_background = '#EFAF12';
			$home_badge = 'TBA';
			$home_foreground = '#000C34';
			$home_team = 'Slough Town';
			$away_background = $opposition->colour_away_background;
			$away_badge = $opposition->logo;
			$away_foreground = $opposition->colour_away_foreground;
			$away_team = $opposition->name;
		}
		elseif($away_alternate_kit == 0)
		{
			// Slough is home
			$home_background = $opposition->colour_home_background;
			$home_badge = $opposition->logo;
			$home_foreground = $opposition->colour_home_foreground;
			$home_team = $opposition->name;
			$away_background = '#c9e6ff';
			$away_badge = 'TBA';
			$away_foreground = '#FFFFFF';
			$away_team = 'Slough Town';
		}
		else
		{
			// Slough is away
			$home_background = $opposition->colour_home_background;
			$home_badge = $opposition->logo;
			$home_foreground = $opposition->colour_home_foreground;
			$home_team = $opposition->name;
			$away_background = '#c9e6ff';
			$away_badge = 'TBA';
			$away_foreground = '#FFFFFF';
			$away_team = 'Slough Town';
		}

		// Fetch Man of the Match
		$mom = ORM::factory('player', $report->report_mom);
		$mom = ($mom->loaded) ? $mom->first_name . ' ' . $mom->last_name : 'Unknown';

		// Set the title
		$this->layout->blocks['head']->title = $headline;
		// Set the page body
		$this->_load_block('body', 'pages/report');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Pass to view
		$this->layout->blocks['body']->attendance = $attendance;
		$this->layout->blocks['body']->headline = $headline;
		$this->layout->blocks['body']->is_home = $is_home;
		$this->layout->blocks['body']->match_date = $match_date;
		$this->layout->blocks['body']->opposition = $opposition->name;
		$this->layout->blocks['body']->home_score = $home_score;
		$this->layout->blocks['body']->away_score = $away_score;
		$this->layout->blocks['body']->home_scorers = $home_scorers;
		$this->layout->blocks['body']->away_scorers = $away_scorers;
		$this->layout->blocks['body']->opposition = $opposition->name;
		$this->layout->blocks['body']->mom = $mom;
		$this->layout->blocks['body']->competition = $competition->name;
		$this->layout->blocks['body']->body = $body;
		$this->layout->blocks['body']->home_background = $home_background;
		$this->layout->blocks['body']->home_badge = $home_badge;
		$this->layout->blocks['body']->home_foreground = $home_foreground;
		$this->layout->blocks['body']->home_team = $home_team;
		$this->layout->blocks['body']->away_background = $away_background;
		$this->layout->blocks['body']->away_badge = $away_badge;
		$this->layout->blocks['body']->away_foreground = $away_foreground;
		$this->layout->blocks['body']->away_team = $away_team;
		$this->layout->blocks['body']->match_sponsor = $match_sponsor;
		$this->layout->blocks['body']->ball_sponsor = $ball_sponsor;
		$this->layout->blocks['body']->opposition_team = $opposition_team;
		$this->layout->blocks['body']->video_url = $video_url;
		$this->layout->blocks['body']->flickr_url = $flickr_url;
		$this->layout->blocks['body']->lineup = $lineup;
		$this->layout->blocks['body']->subs = $subs;
		}
		else
		{

		// Generate variables
		$headline = $report->report_headline;
		$is_home = $fixture->is_home;
		$match_date = $fixture->match_date;
		$opposition = $fixture->opposition;
		$home_score = $report->report_home_score;
		$away_score = $report->report_away_score;
		$home_scorers = $report->report_home_scorers;
		$away_scorers = $report->report_away_scorers;
		$competition = $fixture->competition;
		$body = explode("\n\n", $report->report_body);
		$report_team = $report->report_friendly_team;

		// Set the title
		$this->layout->blocks['head']->title = $headline;
		// Set the page body
		$this->_load_block('body', 'pages/other_report');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Pass to view
		$this->layout->blocks['body']->headline = $headline;
		$this->layout->blocks['body']->is_home = $is_home;
		$this->layout->blocks['body']->match_date = $match_date;
		$this->layout->blocks['body']->opposition = $opposition->name;
		$this->layout->blocks['body']->home_score = $home_score;
		$this->layout->blocks['body']->away_score = $away_score;
		$this->layout->blocks['body']->home_scorers = $home_scorers;
		$this->layout->blocks['body']->away_scorers = $away_scorers;
		$this->layout->blocks['body']->competition = $competition->name;
		$this->layout->blocks['body']->body = $body;
		$this->layout->blocks['body']->report_team = $report_team;

		}
	}

	/**
	 * Reserve Fixtures page.
	 */
	public function page_reserve_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Reserve Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/reserve_fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Fetch all seasons as select list
		$this->layout->blocks['body']->seasons = ORM::factory('season')->where('start_date>=','2012-07-01')->orderby('start_date', 'DESC')->select_list('id', 'name');
		if (($season = $this->input->post('season')) && valid::digit($season) && ($season > 0))
		{
			// Find season matching the given id
			$season = ORM::factory('season')->where('id', $season)->find();
		}
		else
		{
			// Fetch the most recent (current) season
			$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();
		}
		// Send through season name to the view
		$this->layout->blocks['body']->season_name = $season->name;
		// Send through selected season to the view
		$this->layout->blocks['body']->season_selected = $season->id;

		// Fetch all fixtures for the given season
		$start_date = $season->start_date;
		$end_date = $season->end_date;

		// Push match date / start date checking onto where
		$where = array('match_date >=' => $start_date);
		if ($end_date !== NULL)
		{
			// Push match date / end date checking onto where
			$where['match_date <='] = $end_date;
		}
		// Create orwhere for TBC matches
		$orwhere = array('match_date' => NULL);
		// Fetch all fixtures between the start and end dates
		$fixtures = ORM::factory('reserve_fixture')
					->where($where)
					->orwhere($orwhere)
					->orderby('match_date', 'ASC')
					->find_all();

		// Group fixtures by month
		$grouped_fixtures = array();
		$tbc_fixtures = array();
		foreach ($fixtures as $fixture)
		{
			$match_date = $fixture->match_date;
			// Assumes date delimiter to be '-'
			if (($match_date !== NULL) && $match_date = explode('-', $match_date))
			{
					// Get month number
					$month = $match_date[1];
					// Get month name
					$month = date('F', mktime(0, 0, 0, $month));
					$grouped_fixtures[$month][] = $fixture;
			}
			else
			{
				// Add to TBC fixture list
				$tbc_fixtures[] = $fixture;
			}
		}
		// Apend the TBC list to the sorted fixtures list
		$grouped_fixtures['TBC'] = $tbc_fixtures;

		// Fetch the most recent (current) season
		$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();

		// Pass to the view
		$this->layout->blocks['body']->season = $season;
		$this->layout->blocks['body']->grouped_fixtures = $grouped_fixtures;
	}

	/**
	 * Reserve Report page.
	 */
	public function page_reserve_report($id = NULL)
	{
		// Check article id is a valid positive digit
		if (!valid::digit($id) || $id < 1)
		{
			// Redirect to current news page
			url::redirect('/reserve-fixtures');
		}

		// Fetch the article
		$reserve = ORM::factory('reserve_fixture', $id);

		// Check news loaded
		if (!$reserve->loaded)
		{
			// Redirect to current news page
			url::redirect('/reserve-fixtures');
		}

		// Generate variables
		$headline = $reserve->report_headline;
		$is_home = $reserve->is_home;
		$match_date = $reserve->match_date;
		$opposition = $reserve->opposition;
		$home_score = $reserve->report_home_score;
		$away_score = $reserve->report_away_score;
		$home_scorers = $reserve->report_home_scorers;
		$away_scorers = $reserve->report_home_scorers;
		$competition = $reserve->competition;
		$body = explode("\n\n", $reserve->report_body);
		$report_team = $reserve->report_team;

		// Set the title
		$this->layout->blocks['head']->title = $headline;
		// Set the page body
		$this->_load_block('body', 'pages/other_report');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Pass to view
		$this->layout->blocks['body']->headline = $headline;
		$this->layout->blocks['body']->is_home = $is_home;
		$this->layout->blocks['body']->match_date = $match_date;
		$this->layout->blocks['body']->opposition = $opposition;
		$this->layout->blocks['body']->home_score = $home_score;
		$this->layout->blocks['body']->away_score = $away_score;
		$this->layout->blocks['body']->home_scorers = $home_scorers;
		$this->layout->blocks['body']->away_scorers = $away_scorers;
		$this->layout->blocks['body']->opposition = $opposition;
		$this->layout->blocks['body']->competition = $competition;
		$this->layout->blocks['body']->body = $body;
		$this->layout->blocks['body']->report_team = $report_team;
	}

	/**
	 * Sponsorship opportunities page.
	 */
	public function page_sponsorship_opportunities()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Sponsorship Opportunities';
		// Set the page body
		$this->_load_block('body', 'pages/sponsorship_opportunities');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
		
		// Create an error array
		$errors = array();

		// Create the captcha object
		$captcha = new Captcha;

		// Check whether the user has exceeded the CAPTCHA threshold
		if ($captcha->invalid_count() > 20)
		{
			// Redirect to root
			url::redirect('/');
		}

		// Check whether a message has been submitted
		$message_data = array
		(
			'name' => NULL,
			'return_email' => NULL,
			'contact_id' => NULL,
			'subject' => NULL,
			'body' => NULL,
			'captcha_response' => NULL
		);
		if ($this->input->post('submit'))
		{
			// Message has been submitted
			// Fetch $_POST data
			$post = $this->input->post();
			$message_data = arr::extract
			(
				$post,
				array_keys($message_data)
			);

			//Hardcode Contact ID
			$message_data['contact_id'] = 10;

			$message = ORM::factory('message');
			if ($message->validate($message_data))
			{
				// Check the captcha is valid
				if (Captcha::valid($post['captcha_response']))
				{
					// Successful validation, save!
					$message->save();
					// Send e-mail
					$headers = 	'From:' . 'paul.lillywhite@sloughtownfc.net' . "\n" .
								'Reply-To:' . $message->return_email . "\n";
					mail
					(
						$message->contact->email,
						"SloughTownFC.net Online Message - Commercial Opportunities: " . $message->subject,
						"From: " . $message->name . "\n\n" . $message->body . "\n\n Reply-To: " . $message->return_email . "\n IP: " . $_SERVER['REMOTE_ADDR'],
						$headers
					);
					// Flash success notification
					$this->_set_notification
					(
						'Thank you, your message has been sent.',
						'success',
						TRUE
					);
					// Redirect to root
					url::redirect('/sponsorship-opportunities');
				}
				else
				{
					// Set validation errors and CAPTCHA error message
					$errors = $message_data->errors();
					$errors['captcha'] = 'CAPTCHA check failed!';
					// Flash error notification
					$this->_set_notification
					(
						'There was an error processing your message.',
						'error'
					);
				}
			}
			else
			{
				// Set validation errors
				$errors = $message_data->errors();
				// Flash error notification
				$this->_set_notification
				(
					'There was an error processing your message.',
					'error'
				);
			}
			// Set message data to an array
			$message_data = $message_data->as_array();
		}

		// Fetch contact list
		$contacts = ORM::factory('contact')
						->select_list('id', 'name');

		// Send to view
		$this->layout->blocks['body']->contacts = $contacts;
		$this->layout->blocks['body']->captcha = $captcha->render();
		$this->layout->blocks['body']->errors = $errors;
		$this->layout->blocks['body']->message_data = $message_data;
	}

	/**
	 * Supporters Coach page.
	 */
	public function page_supporters_coach()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Supporters Coach';
		// Set the page body
		$this->_load_block('body', 'pages/supporters_coach');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Fetch the most recent (current) season
		$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();

		// Fetch all fixtures for the given season
		$start_date = $season->start_date;
		$end_date = $season->end_date;

		// Push publish date / start date checking onto where
		$where = array('match_date >=' => $start_date);
		if ($end_date !== NULL)
		{
			// Push publish / end date checking onto where
			$where['match_date <='] = $end_date;
		}

		$date = date('Y-m-d');

		// Fetch all Fixtures between the start and end dates
		$coach = ORM::factory('fixture')->where($where)->where('match_date >=', $date)->where('is_home', NULL)->orderby('match_date', 'ASC')->find();

		//Generate variables
		$match_date = $coach->match_date;
		$departs = $coach->coach_departs;
		$adult = $coach->coach_adult_price;
		$reduced = $coach->coach_reduced_price;

		//Pass variables
		$this->layout->blocks['body']->match_date = $match_date;
		$this->layout->blocks['body']->departs = $departs;
		$this->layout->blocks['body']->adult = $adult;
		$this->layout->blocks['body']->reduced = $reduced;

		$opposition = $coach->opposition_id;

		// Fetch opposition
		$opposition = ORM::factory('opposition')->where('id', $opposition)->find();

		//Generate variables
		$name = $opposition->name;
		$logo = $opposition->logo;

		//Pass variables
		$this->layout->blocks['body']->name = $name;
		$this->layout->blocks['body']->logo = $logo;
	}

	/**
	 * Ticks page.
	 */
	public function page_tickets()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Tickets & Admission';
		// Set the page body
		$this->_load_block('body', 'pages/tickets');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');
	}


	/**
	 * Trust page.
	 */
	public function page_trust()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Slough Town Supporters Trust News';
		// Set the page body
		$this->_load_block('body', 'pages/trust');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Fetch all Trust items between
		$this->layout->blocks['body']->trust = ORM::factory('trust')
													->orderby('publish_date', 'ASC')
													->find_all();
	}

	/**
	 * U8s Fixtures page.
	 */
	public function page_u8_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'U8s Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/u8_fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar_youth');
	}

	/**
	 * U9s Fixtures page.
	 */
	public function page_u9_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'U9s Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/u9_fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar_youth');
	}

	/**
	 * U10s Fixtures page.
	 */
	public function page_u10_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'U10s Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/u10_fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar_youth');
	}

	/**
	 * U11s Fixtures page.
	 */
	public function page_u11_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'U11s Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/u11_fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar_youth');
	}

	/**
	 * U12s Fixtures page.
	 */
	public function page_u12_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'U12s Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/u12_fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar_youth');
	}
	
	/**
	 * U13s Fixtures page.
	 */
	public function page_u13_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'U13s Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/u13_fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar_youth');
	}

	/**
	 * U14s Fixtures page.
	 */
	public function page_u14_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'U14s Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/u14_fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar_youth');
	}
	
	/**
	 * U15s Fixtures page.
	 */
	public function page_u15_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'U15s Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/u15_fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar_youth');
	}

	/**
	 * U16s Fixtures page.
	 */
	public function page_u16_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'U16s Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/u16_fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar_youth');
	}

	/**
	 * U18s Fixtures page.
	 */
	public function page_u18_fixtures()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'U18s Fixtures and Reports';
		// Set the page body
		$this->_load_block('body', 'pages/u18_fixtures');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Fetch all seasons as select list
		$this->layout->blocks['body']->seasons = ORM::factory('season')->where('start_date>=','2008-07-01')->orderby('start_date', 'DESC')->select_list('id', 'name');
		if (($season = $this->input->post('season')) && valid::digit($season) && ($season > 0))
		{
			// Find season matching the given id
			$season = ORM::factory('season')->where('id', $season)->find();
		}
		else
		{
			// Fetch the most recent (current) season
			$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();
		}
		// Send through season name to the view
		$this->layout->blocks['body']->season_name = $season->name;
		// Send through selected season to the view
		$this->layout->blocks['body']->season_selected = $season->id;

		// Fetch all fixtures for the given season
		$start_date = $season->start_date;
		$end_date = $season->end_date;

		// Push match date / start date checking onto where
		$where = array('match_date >=' => $start_date);
		if ($end_date !== NULL)
		{
			// Push match date / end date checking onto where
			$where['match_date <='] = $end_date;
		}
		// Create orwhere for TBC matches
		$orwhere = array('match_date' => NULL);
		// Fetch all fixtures between the start and end dates
		$fixtures = ORM::factory('u18s_fixture')
					->where($where)
					->orwhere($orwhere)
					->orderby('match_date', 'ASC')
					->find_all();

		// Group fixtures by month
		$grouped_fixtures = array();
		$tbc_fixtures = array();
		foreach ($fixtures as $fixture)
		{
			$match_date = $fixture->match_date;
			// Assumes date delimiter to be '-'
			if (($match_date !== NULL) && $match_date = explode('-', $match_date))
			{
					// Get month number
					$month = $match_date[1];
					// Get month name
					$month = date('F', mktime(0, 0, 0, $month));
					$grouped_fixtures[$month][] = $fixture;
			}
			else
			{
				// Add to TBC fixture list
				$tbc_fixtures[] = $fixture;
			}
		}
		// Apend the TBC list to the sorted fixtures list
		$grouped_fixtures['TBC'] = $tbc_fixtures;

		// Fetch the most recent (current) season
		$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();

		// Pass to the view
		$this->layout->blocks['body']->season = $season;
		$this->layout->blocks['body']->grouped_fixtures = $grouped_fixtures;
	}

	/**
	 * U18 Report page.
	 */
	public function page_u18_report($id = NULL)
	{
		// Check article id is a valid positive digit
		if (!valid::digit($id) || $id < 1)
		{
			// Redirect to current news page
			url::redirect('/u18-fixtures');
		}

		// Fetch the article
		$u18 = ORM::factory('u18s_fixture', $id);

		// Check news loaded
		if (!$u18->loaded)
		{
			// Redirect to current news page
			url::redirect('/u18-fixtures');
		}

		// Generate variables
		$headline = $u18->report_headline;
		$is_home = $u18->is_home;
		$match_date = $u18->match_date;
		$opposition = $u18->opposition;
		$home_score = $u18->report_home_score;
		$away_score = $u18->report_away_score;
		$home_scorers = $u18->report_home_scorers;
		$away_scorers = $u18->report_home_scorers;
		$competition = $u18->competition;
		$body = explode("\n\n", $u18->report_body);
		$report_team = $u18->report_team;

		// Set the title
		$this->layout->blocks['head']->title = $headline;
		// Set the page body
		$this->_load_block('body', 'pages/other_report');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Pass to view
		$this->layout->blocks['body']->headline = $headline;
		$this->layout->blocks['body']->is_home = $is_home;
		$this->layout->blocks['body']->match_date = $match_date;
		$this->layout->blocks['body']->opposition = $opposition;
		$this->layout->blocks['body']->home_score = $home_score;
		$this->layout->blocks['body']->away_score = $away_score;
		$this->layout->blocks['body']->home_scorers = $home_scorers;
		$this->layout->blocks['body']->away_scorers = $away_scorers;
		$this->layout->blocks['body']->opposition = $opposition;
		$this->layout->blocks['body']->competition = $competition;
		$this->layout->blocks['body']->body = $body;
		$this->layout->blocks['body']->report_team = $report_team;
	}

	/**
	 * Videos page.
	 */
	public function page_videos()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'Club Videos - STFC TV';
		// Set the page body
		$this->_load_block('body', 'pages/videos');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Fetch all seasons as select list
		$this->layout->blocks['body']->seasons = ORM::factory('season')->where('start_date>=','2003-07-01')->orderby('start_date', 'DESC')->select_list('id', 'name');
		if (($season = $this->input->post('season')) && valid::digit($season) && ($season > 0))
		{
			// Find season matching the given id
			$season = ORM::factory('season')->where('id', $season)->find();
		}
		else
		{
			// Fetch the most recent (current) season
			$season = ORM::factory('season')->orderby('start_date', 'DESC')->find();
			//$season = ORM::factory('season')->where('start_date <=','2010-07-01')->orderby('start_date', 'DESC')->find();
		}
		// Send through season name to the view
		$this->layout->blocks['body']->season_name = $season->name;
		// Send through selected season to the view
		$this->layout->blocks['body']->season_selected = $season->id;

		// Fetch all fixtures for the given season
		$start_date = $season->start_date;
		$end_date = $season->end_date;
		// Push match date / start date checking onto where
		$where = array('match_date >=' => $start_date);
		if ($end_date !== NULL)
		{
			// Push match date / end date checking onto where
			$where['match_date <='] = $end_date;
		}
		// Fetch all Fixtures between the start and end dates
		$this->layout->blocks['body']->fixtures = ORM::factory('fixture')
													->where($where)
													->orderby('match_date', 'DESC')
													->find_all();
	}

	/**
	 * Video page
	 */
	public function page_video($id = NULL)
	{
		// Check article id is a valid positive digit
		if (!valid::digit($id) || $id < 1)
		{
			// Redirect to current news page
			url::redirect('/videos');
		}

		// Fetch the article
		$video = ORM::factory('fixture', $id);

		// Check news loaded
		if (!$video->loaded)
		{
			// Redirect to current news page
			url::redirect('/videos');
		}

		// Generate variables
		$id = $video->id;
		$match_date = $video->match_date;
		$is_home = $video->is_home;
		$home_score = $video->report_home_score;
		$away_score = $video->report_away_score;
		$author = $video->video_author;
		$video_url = $video->video_url;
		$video_body = explode("\n\n", $video->video_body);
		$report_flickr_url = $video->report_flickr_url;
		$opposition = $video->opposition->name;

		$headline = "Video footage against $opposition";

		// Set the title
		$this->layout->blocks['head']->title = $headline;
		// Set the page body
		$this->_load_block('body', 'pages/video');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar');

		// Pass to view
		$this->layout->blocks['body']->id = $id;
		$this->layout->blocks['body']->match_date = $match_date;
		$this->layout->blocks['body']->is_home = $is_home;
		$this->layout->blocks['body']->home_score = $home_score;
		$this->layout->blocks['body']->away_score = $away_score;
		$this->layout->blocks['body']->opposition = $opposition;
		$this->layout->blocks['body']->author = $author;
		$this->layout->blocks['body']->video_url = $video_url;
		$this->layout->blocks['body']->video_body = $video_body;
		$this->layout->blocks['body']->report_flickr_url = $report_flickr_url;
	}

	/**
	 * STFC Youth.
	 */
	public function page_youth()
	{
		// Set the title
		$this->layout->blocks['head']->title = 'STFC Youth';
		// Set the page body
		$this->_load_block('body', 'pages/stfc_youth');
		// Set the sidebar
		$this->_load_block('sidebar', 'sidebar_youth');

		// Create an error array
		$errors = array();

		// Create the captcha object
		$captcha = new Captcha;

		// Check whether the user has exceeded the CAPTCHA threshold
		if ($captcha->invalid_count() > 20)
		{
			// Redirect to root
			url::redirect('/');
		}

		// Check whether a message has been submitted
		$message_data = array
		(
			'name' => NULL,
			'return_email' => NULL,
			'contact_id' => NULL,
			'subject' => NULL,
			'body' => NULL,
			'captcha_response' => NULL
		);
		if ($this->input->post('submit'))
		{
			// Message has been submitted
			// Fetch $_POST data
			$post = $this->input->post();
			$message_data = arr::extract
			(
				$post,
				array_keys($message_data)
			);

			//Hardcode Contact ID
			$message_data['contact_id'] = 11;

			$message = ORM::factory('message');
			if ($message->validate($message_data))
			{
				// Check the captcha is valid
				if (Captcha::valid($post['captcha_response']))
				{
					// Successful validation, save!
					$message->save();
					// Send e-mail
					$headers = 	'From:' . 'postmaster@sloughtownfc.net' . "\n" .
								'Reply-To:' . $message->return_email . "\n";
					mail
					(
						$message->contact->email,
						"SloughTownFC.net Online Message - STFC Youth: " . $message->subject,
						"From: " . $message->name . "\n\n" . $message->body . "\n\n Reply-To: " . $message->return_email . "\n IP: " . $_SERVER['REMOTE_ADDR'],
						$headers
					);
					// Flash success notification
					$this->_set_notification
					(
						'Thank you, your message has been sent.',
						'success',
						TRUE
					);
					// Redirect to root
					url::redirect('/stfc-youth');
				}
				else
				{
					// Set validation errors and CAPTCHA error message
					$errors = $message_data->errors();
					$errors['captcha'] = 'CAPTCHA check failed!';
					// Flash error notification
					$this->_set_notification
					(
						'There was an error processing your message.',
						'error'
					);
				}
			}
			else
			{
				// Set validation errors
				$errors = $message_data->errors();
				// Flash error notification
				$this->_set_notification
				(
					'There was an error processing your message.',
					'error'
				);
			}
			// Set message data to an array
			$message_data = $message_data->as_array();
		}

		// Fetch contact list
		$contacts = ORM::factory('contact')
						->select_list('id', 'name');

		// Send to view
		$this->layout->blocks['body']->contacts = $contacts;
		$this->layout->blocks['body']->captcha = $captcha->render();
		$this->layout->blocks['body']->errors = $errors;
		$this->layout->blocks['body']->message_data = $message_data;

	}

} // End Default Controller
