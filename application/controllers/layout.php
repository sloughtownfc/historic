<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Layout controller
 */
abstract class Layout_Controller extends Controller {

	// Default auto-rendering on.
	public $auto_render = TRUE;

	// Set the blocks array.
	public $blocks = array
	(
		// 'Name' => 'View'
	);

	// Set the block views directory.
	public $blocks_dir = 'templates/blocks';

	// Set the layout name.
	public $layout = 'default';

	// Set the layout views directory.
	public $layouts_dir = 'templates/layouts';

	public function __construct()
	{
		parent::__construct();
		// Load the layout
		$this->_load_layout($this->layout);
		// Load the blocks
		if (!empty($this->blocks))
		{
			$this->_load_blocks($this->blocks);
		}
		// Set auto-rendering
		if ($this->auto_render === TRUE)
		{
			// Render the layout immediately after the controller method
			Event::add('system.post_controller', array($this, '_auto_render_layout'));
		}
		// Load in session
		$this->session = Session::instance();
		// Load notification
		$this->_check_flash_notifications();
	}

	/**
	 * Automatically render the layout if $this->auto_render is TRUE.
	 */
	public function _auto_render_layout()
	{
		if ($this->auto_render === TRUE)
		{
			// Render the layout when the class is destroyed
			$this->layout->render(TRUE);
		}
	}

	/**
	 * Check Session for notification and set into current request.
	 *
	 * Session must include the notification type (block template)
	 * and notification message.
	 */
	protected function _check_flash_notifications()
	{
		// Fetch notification data (if it exists)
		$message = $this->session->get('notification_message');
		$type = $this->session->get('notification_type');

		// Check both notification message and type are set
		if ($message !== false && $type !== false)
		{
			$this->_set_notification($message, $type);
		}
	}

	/**
	 * Load a single block into the layout.
	 *
	 * @param string The block name (e.g., 'body').
	 * @param string The block template.
	 */
	protected function _load_block($name, $view)
	{
		// Create the layout blocks array, if it does not already
		// exist, or it is not an array
		// IMPORTANT: Must first test with isset() as using is_array()
		// on an unset View variable directly causes a byRef error
		if (!isset($this->layout->blocks) || !is_array($this->layout->blocks))
		{
			$this->layout->blocks = array();
		}
		// Load block under layout as view
		$this->layout->blocks[$name] = new View
		(
			$this->blocks_dir . DIRECTORY_SEPARATOR . $view
		);
	}

	/**
	 * Load a set of blocks into the layout.
	 *
	 * When $preserve is FALSE, the existing blocks array is cleared.
	 *
	 * @param array The blocks to load.
	 * @param bool Preserve existing blocks.
	 */
	protected function _load_blocks(array $blocks, $preserve = TRUE)
	{
		// Clear the blocks out if not preserving them
		if (!$preserve)	$this->layout->blocks = array();
		// Load each passed array item as a new block in the layout
		foreach ($blocks as $name => $view)
		{
			$this->_load_block($name, $view);
		}
	}

	/**
	 * Load the layout as a new view.
	 *
	 * @param string The layout template to use.
	 */
	protected function _load_layout($layout)
	{
		// Load the layout as a new view
		$this->layout = new View
		(
			$this->layouts_dir . DIRECTORY_SEPARATOR . $layout
		);
	}

	/**
	 * Set a notification for the current request or the next request
	 * using Session.
	 *
	 * @param string notification message
	 * @param string notification block type
	 * @param boolean switch to flash notification to next request
	 */
	protected function _set_notification($message, $type = 'info', $flash = FALSE)
	{
		if ($flash === false)
		{
			// Load notification block for this request
			$this->_load_block('notification', 'notifications' . DIRECTORY_SEPARATOR . $type);
			$this->layout->blocks['notification']->message = $message;
		}
		else
		{
			// Flash notification to next request
			$this->session->set_flash
			(
				array
				(
					'notification_message' => $message,
					'notification_type' => $type
				)
			);
		}
	}

} // End Layout Controller
