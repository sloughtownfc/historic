<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Auth controller
 */
class Auth_Controller extends Layout_Controller {

	// Switch off auto-rendering
	public $auto_render = FALSE;

	// Set the login URI.
	public $login_uri = 'login';

	// Set the default post-login redirect location.
	public $post_login_location = '/admin';

	// Set a post-logout redirect location.
	public $post_logout_location = '/';

	// Set the blocks array.
	public $blocks = array
	(
		'head' => 'admin_head'
	);

	// Set the layout name.
	public $layout = 'admin';

	public function __construct()
	{
		parent::__construct();
		// Load in session
		$this->session = Session::instance();
	}

	public function index()
	{
		url::redirect($this->loginUri);
	}

	public function login()
	{
		if (Auth::instance()->logged_in())
		{
			// Already logged in
			// Redirect to default post-login
			url::redirect($this->post_login_location);
		}
		else
		{
			// Get any POST input
			$post_in = $this->input->post();

			if (empty($post_in))
			{
				// No POST data, a login is required
				// Load the body block
				$this->_load_block('body', 'login_body');
				// Set the title
				$this->layout->blocks['head']->title = 'Please login';
				// Set the message
				$this->layout->blocks['body']->message = 'Please login to continue';
				// Set the login URI
				$this->layout->blocks['body']->login_uri = $this->login_uri;
				// Render!
				$this->layout->render(TRUE);
			}
			else
			{
				// Try logging in with POST input
				// Use ORM to allow input validation
				if (ORM::factory('user')->login($post_in))
				{
					// Login successful
					// Redirect to requested URI (or default post-login URI)
					$redirect_uri = $this->session->get('requested_uri');
					if ($redirect_uri)
					{
						url::redirect($redirect_uri);
					}
					else
					{
						url::redirect($this->post_login_location);
					}
				}
				else
				{
					// Login failed
					// Load the body block
					$this->_load_block('body', 'login_body');
					// Set the title
					$this->layout->blocks['head']->title = 'Please login';
					// Set the message
					$this->layout->blocks['body']->message = 'Login failed! Please try again.';
					// Set the login URI
					$this->layout->blocks['body']->login_uri = $this->login_uri;
					// Render!
					$this->layout->render(TRUE);
				}
			}
		}
	}

	public function logout()
	{
		Auth::instance()->logout(TRUE);
		url::redirect($this->post_logout_location);
	}

	public function testRegister()
	{
		$username = 'oliver';
		$password = 'password';
		$user = ORM::factory('user');
		$user->username = $username;
		$user->password = $password; // The model does the hashing automatically

		if
		(
			$user->add(ORM::factory('role', 'login')) &&
			$user->add(ORM::factory('role', 'admin')) &&
			$user->save()
		)
		{
			echo 'Registered test user successfully!';
		}
	}

} // End Auth Controller
