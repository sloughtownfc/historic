<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Array helper class.
 */
class arr extends arr_Core {

	/**
	 * Extract one or more keys from an array. Each string key given after the
	 * first argument (the array) will be extracted. Keys can also be passed as
	 * an array. Keys that do not exist in the search array will be NULL in the
	 * extracted data.
	 *
	 * @param   array   array to search
	 * @param   mixed   string key name or array of keys
	 * @return  array
	 */
	public static function extract(array $search, $keys)
	{
		if (!is_array($keys))
		{
			// Get the string keys, removing the $search array
			$keys = array_slice(func_get_args(), 1);
		}

		$found = array();
		foreach ($keys as $key)
		{
			if (isset($search[$key]))
			{
				$found[$key] = $search[$key];
			}
			else
			{
				$found[$key] = NULL;
			}
		}

		return $found;
	}
} // End arr
